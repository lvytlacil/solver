#ifndef BISECTORFWD_H
#define BISECTORFWD_H
#include <memory>

namespace vytlalib {
namespace bisector {

class BisectionResult;
class IBisector;
class Bisector;

/**
 * @typedef Unique pointer to IBisector.
 */
typedef std::unique_ptr<IBisector> UPtrIBisector;

}
}
#endif // BISECTORFWD_H
