#ifndef BISECTOR_H
#define BISECTOR_H

#include <utility>
#include <memory>
#include "bisectorfwd.h"
#include "../box/box.h"

namespace vytlalib {
namespace bisector {

/**
 * @brief Encapsulates a result of an evaluation of a box with a bisector.
 */
struct BisectionResult {
    /**
     * @brief Index of a component to be bisected. If a bisection is not possible,
     * then -1.
     */
    int index;
    /**
     * @brief The point c at which component [a, b] should be bisected. If index != -1,
     * then a < c < b.
     */
    double step;

    /**
     * @brief Construct a BisectionResult instance.
     * @param index
     * @param step
     */
    BisectionResult(int index, double step):
        index(index), step(step) { }

    /**
     * @brief Compares two BisectionResult instances.
     * @param other The instance to compare this to.
     * @return True, if either both instances have the same negative index, or
     * they both have the same non-negative index and their steps are equal. Otherwise false.
     */
    bool operator ==(const BisectionResult &other) {
        return index == other.index && (index > -1 ? step == other.step : true);
    }

    /**
     * @brief Sends the given instance of BisectionResult to the given output stream.
     * @param out
     * @param br
     * @return The reference to the same output stream as @p out.
     */
    friend std::ostream& operator<<(std::ostream& out, const BisectionResult &br);
};

/**
 * @brief Abstract class representing bisector, that is capable of analysing boxes
 * for possible bisection.
 */
class IBisector
{
private:
    // Sends this instance to the given stream.
    virtual void toStream(std::ostream &out) const = 0;
public:
    IBisector() { }

    /**
     * @brief Examines the given box for possible bisection.
     * @param parentBox Box to examine
     * @return BisectionResult instance containing information, about what component and
     * at which point should be bisected according to the semantics of this bisector.
     */
    virtual BisectionResult bisect(const boxes::Box &parentBox) const = 0;

    /**
     * @brief Sends the given instance of IBisector to the given output stream.
     * @param out
     * @param br
     * @return The reference to the same output stream as @p out.
     */
    friend std::ostream& operator<<(std::ostream &out, const IBisector &bisector);
};

/**
 * @brief Bisector that analyses boxes for possible bisection
 * along their longest non-degenerate component.
 */
class Bisector : public IBisector {
private:
    // Minimal width to bisect
    double threshold_;

    // Stored ratio
    double ratio_;    

    // The bisection analysis algorithm itself
    BisectionResult bisect_(const boxes::Box &parentBox) const;

    // toStream override
    void toStream(std::ostream &out) const override;

public:
    /**
     * @brief Creates a bisector that will analyse boxes for splitting along their
     * longest component.
     * @param threshold Minimal width a component must have to be bisected.
     * @param ratio The ratio the component will be split in.
     * @pre 0.0 < ratio < 1.0
     * @throw ProgramException if preconditions are not satisfied.
     */
    Bisector(double threshold, double ratio = 0.5);

    /**
     * @brief Returns the underlying threshold.
     * @return The underlying threshold
     */
    double getThreshold() const {
        return threshold_;
    }

    /**
     * @brief Sets a new value of the underlying threshold.
     * @param threshold Value to be set.
     */
    void setThredhold(double threshold) {
        threshold_ = threshold;
    }

    /**
     * @brief Examines the given box, computes index of its longest non degenerate component [a, b]
     * and step c = a + ratio * (b - a).
     * @param Box to examine for bisection.
     * @return BisectionResult(index, c) if bisection can be performed. Otherwise BisectionResult
     * with index -1.
     */
    virtual BisectionResult bisect(const boxes::Box &parentBox) const final override;



};

}
}
#endif // BISECTOR_H
