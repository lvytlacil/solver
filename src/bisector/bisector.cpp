#include <iostream>
#include "bisector.h"
#include "../box/box.h"
#include "../message/messageprocessor.h"

namespace vytlalib {

using boxes::Box;
using boxes::OrientedBox;
using boxes::Component;
using message::ProgramException;

namespace bisector {

/*------------------BisectionResult------------------------*/

std::ostream& operator<<(std::ostream &out, const BisectionResult &br) {
    out << "BisResult[index = " << br.index << ", step = " << br.step;
    return out;
}

/*-------IBisector-----------------------*/

std::ostream& operator<<(std::ostream &out, const IBisector &bisector) {
    bisector.toStream(out);
    return out;
}

/*------------------Bisector------------------------*/
Bisector::Bisector(double threshold, double ratio):
    threshold_(threshold), ratio_(ratio) {
    if (ratio_ <= 0.0 || ratio >= 1.0) {
        throw ProgramException("Ratio must be strictly between 0.0 and 1.0");
    }
}

void Bisector::toStream(std::ostream &out) const {
    out << "Bisector[threshold = " << threshold_ << ", ratio = " << ratio_ << "]";
}

BisectionResult Bisector::bisect_(const boxes::Box &parentBox) const {
    int index = parentBox.getLongestDim();
    if (index == -1)
        return BisectionResult(-1, 0);

    const Component& boundary = parentBox.getComp(index);
    if (boundary.width() < threshold_) {
        return BisectionResult(-1, 0);
    }

    double step = boundary.step(ratio_);
    if (step == boundary.lower() || step == boundary.upper()) {
        return BisectionResult(-1, 0);
    }
    return BisectionResult(index, step);
}

BisectionResult Bisector::bisect(const Box &parentBox) const {
    return bisect_(parentBox);
}

}
}

