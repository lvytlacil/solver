#include <ostream>
#include "signvector.h"
#include "../message/messageprocessor.h"

namespace vytlalib {
using message::ProgramException;
namespace boxes {

/*------- Sign -------*/

Sign pow(Sign base, int exp) {
    if (base == Sign::MINUS)
        return exp % 2 == 0 ? Sign::PLUS : Sign::MINUS;
    return base;
}

Sign operator*(Sign left, int right) {
    if (left == Sign::ZERO || right == 0)
        return Sign::ZERO;
    if ( (left == Sign::PLUS && right > 0) || (left == Sign::MINUS && right < 0) )
        return Sign::PLUS;
    return Sign::MINUS;
}

Sign operator*(int left, Sign right) {
    return right * left;
}

Sign operator*(Sign left, Sign right) {
    if (left == Sign::ZERO || right == Sign::ZERO)
        return left;
    return (left == right) ? Sign::PLUS : Sign::MINUS;
}

std::ostream& operator<<(std::ostream &out, const Sign &sign) {
    switch (sign) {
    case (Sign::PLUS):
        out << "+";
        break;
    case (Sign::MINUS):
        out << "-";
        break;
    default:
        out << "0";
    }
    return out;
}

/*------- SignVector -------*/

SignVector::SignVector(int size, Sign defaultValue) {
    if (size < 0)
        throw ProgramException("Negative size given during SignVector construction");
    elements_ = std::vector<Sign>(size, defaultValue);
}

const Sign& SignVector::operator[](int index) const {
    return elements_.at(index);
}

Sign& SignVector::operator[](int index) {
    return elements_.at(index);
}

void SignVector::removeAt(int index) {
    elements_.erase(elements_.begin() + index);
}

bool SignVector::isSufficient() const {
    for (unsigned i = 0; i < elements_.size(); ++i) {
        if (elements_[i] != Sign::ZERO)
            return true;
    }
    return false;
}

std::ostream& operator<<(std::ostream &out, const SignVector &signList) {
    out << "( ";
    for (auto elem : signList.elements_) {
        out << elem << ", ";
    }
    out << ")";
    return out;
}

}

}
