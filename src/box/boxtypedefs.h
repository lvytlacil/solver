#ifndef BOXTYPEDEFS_H
#define BOXTYPEDEFS_H

#include <memory>
#include <list>
namespace vytlalib {
namespace boxes {

class Box;
enum class Sign;
enum class BoxOrientation;
class OrientedBox;
class SignVector;

/**
 * @typedef A Unique pointer to BaseBox.
 */
typedef std::unique_ptr<Box> UPtrBox;

/**
 * @typedef A List of Box instances.
 */
typedef std::list<Box> BoxList;

/**
 * @typedef A Pair of a Box and a SignVector
 */
typedef std::pair<boxes::Box, boxes::SignVector> BoxSignVectorPair;

/**
 * @typedef A Sign covering, i.e. a list of pairs of a box and a sign vector.
 */
typedef std::list<BoxSignVectorPair> SignCoveringList;

}

}
#endif // BOXTYPEDEFS_H
