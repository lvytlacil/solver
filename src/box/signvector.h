#ifndef SIGNLIST_H
#define SIGNLIST_H

#include <iosfwd> // To forward declare std::ostream
#include <initializer_list>
#include <vector>

namespace vytlalib {
namespace boxes {

/*------- Sign -------*/
/**
 * @brief An enum to encapsulate possible values of SignVector components values.
 */
enum class Sign {PLUS, MINUS, ZERO};

/**
 * @brief Computes base^exp, where base is a Sign and exp is an integer.
 *
 * @param base The Sign to be the base of the operation.
 * @param exp The exponent of the operation.
 * @return if base is PLUS, then PLUS.
 * if base is ZERO, then ZERO.
 * if base is MINUS, then PLUS, if exp is even, and MINUS otherwise.
 */
Sign pow(Sign base, int exp);

/**
 * @brief Computes the product of an int and a Sign.
 * @param left The input integer.
 * @param right The input Sign.
 * @return The sign of left * right, where right
 * is substituted with 1, if it is PLUS, -1 if it is MINUS, and 0 otherwise.
 */
Sign operator*(int left, Sign right);

/**
 * @see operator*(int, Sign)
 */
Sign operator*(Sign left, int Right);
Sign operator*(Sign left, Sign right);

/**
 * @brief Sends the given Sign instance to the given std::ostream.
 * @param out The given output stream.
 * @param sign The given Sign
 * @return The output stream @p out.
 */
std::ostream& operator<<(std::ostream &out, const Sign &sign);

/*------- SignVector -------*/
/**
 * @brief Represents a vector of signs.
 */
class SignVector {
private:
    // Stores the elements
    std::vector<Sign> elements_;

public:

    /**
     * @brief Creates a sign vector of the given size, filled with ZEROs.
     * @param size The size of the vector
     * @pre size >= 0, otherwise an exception is thrown.
     */
    SignVector(int size):
        SignVector(size, Sign::ZERO) { }

    /**
     * @brief Creates a sign vector of the given size, filled with the given value.
     * @param size The size of the vector.
     * @param defaultValue The value used to fill the vector.
     * @pre size >= 0, otherwise an exception is thrown.
     */
    SignVector(int size, Sign defaultValue);

    /**
     * @brief Creates a sign vector from the given initializer list.
     * @param argList The initializer list used to create the sign vector.
     */
    SignVector(std::initializer_list<Sign> argList) {
        for (Sign s : argList) {
            elements_.push_back(s);
        }
    }

    /**
     * @brief Compares this instance to other instance of the same type.
     * @param other The SignList to compare with this one.
     * @return True, if this and other has same size and elements on respective positions
     * are equal. Otherwise false.
     */
    bool operator==(const SignVector &other) const {
        return elements_ == other.elements_;
    }

    /**
     * @brief Return the negation of this == other.
     * @param other The SignList to compare with this one.
     * @return True, if this == other returns false. Otherwise false.
     */
    bool operator!=(const SignVector &other) const {
        return !(*this == other);
    }

    /**
     * @brief Gets a constant reference to the sign on the given index.
     * @param index Index of the sign to be retrieved.
     * @return Sign at the given index.
     * @pre 0 <= index < size, otherwise an exception is thrown.
     */
    const Sign& operator[](int index) const;

    /**
     * @brief Gets a non-constant reference to the sign on the given index.
     * @param index Index of the sign to be retrieved.
     * @return Sign at the given index.
     * @pre 0 <= index < size, otherwise an exception is thrown.
     */
    Sign& operator[](int index);

    /**
     * @brief Gets the size of this sign vector.
     * @return The size of this vector.
     */
    int getSize() const {
        return elements_.size();
    }

    /**
     * @brief Removes the sign at the given position, reducing the size of the sign vector.
     * @param index The index of the sign to be removed.
     * @pre 0 <= index < size, otherwise undefined behavior occurs.
     */
    void removeAt(int index);

    /**
     * @brief Checks whether the given sign vector is sufficient, i.e. contains at least
     * one non zero entry.
     * @param signVector
     * @return True, if the vector contains at least one non-zero element.
     */
    bool isSufficient() const;

    /**
     * @brief Sends the given SignList instance to the given std::ostream.
     * @param out Stream to accept the Signlist.
     * @param signList SignList to send to the std::ostream.
     * @return The same std::ostream @p out passed as an argument.
     */
    friend std::ostream& operator<<(std::ostream &out, const SignVector &signList);
};

}

}
#endif // SIGNLIST_H
