#ifndef BASEBOX_H
#define BASEBOX_H

#include <memory>
#include "boxtypedefs.h"
#include "../component.h"
#include "signvector.h"
#include "../bisector/bisectorfwd.h"
#include "../function/functionfwd.h"
#include "../degree/degreecomputationfwd.h"

namespace vytlalib {

namespace boxes {

// Forward declarations
class Box;

/**
 * @brief An empty box (set).
 */
extern const Box EMPTY_BOX;

/**
 * @brief Checks if all sign coverings in the given sign covering are sufficient.
 *
 * A sign covering is sufficient, if it has at least one non-zero element.
 * @param sl The given list of sign covering.
 * @return True, if every sign covering in the given list is sufficient. Otherwise false.
 */
bool isSignCoveringListDetermined(const SignCoveringList &sl);

/**
 * @brief Writes the given pair of a box and a sign covering to the given output stream.
 * @param out The given output stream.
 * @param sc The given pair of a box and a sign covering.
 * @return Output stream referenced by @p out.
 */
std::ostream& operator<<(std::ostream &out, const BoxSignVectorPair &sc);


/*------- BoxOrientation -------*/

/**
 * @brief Encapsulates possible values of topological orientation of a box.
 */
enum class BoxOrientation {POSITIVE, NEGATIVE};

/**
 * @brief Computes the product of two orientations.
 * @param a BoxOrientation.
 * @param b BoxOrientation.
 * @return The result of @p a * @p b, i.e. POSITIVE if @p a == @p b, otherwise NEGATIVE.
 */
BoxOrientation operator*(const BoxOrientation &a, const BoxOrientation &b);

/**
 * @brief Negates the given orientation.
 * @param a BoxOrientation.
 * @return The result of !@p a, i.e. POSITIVE if @p a is NEGATIVE and NEGATIVE if @p a is POSITIVE.
 */
BoxOrientation operator!(const BoxOrientation &a);


/**
 * @brief Class representing a box, which is a cartesian product
 * of closed real intervals.
 */
class Box
{
    // Friend classes
    friend class degree::Degree;
private:
    // Orientation
    BoxOrientation orientation_;
    // Vector storing actual components
    std::vector<Component> components_;

    //Computes topological face at given index with the given rank.
    Box computeTopologicalFace(int index, bool bottom, int rank) const;

public:

    /**
     * @brief Constructs a box with no components (empty set) with positive orientation.
     */
    Box():
        orientation_(BoxOrientation::POSITIVE), components_() { }

    /**
     * @brief Constructs a Box having the given number of components
     * and having the given component in every dimension. The box will have positive orientation.
     * @param size The number of components this box will have.
     * @param component Component to initialize every dimension of this box.
     */
    Box(int size, const Component &component);

    /**
     * @brief Constructs a Box with the given orientation, having the given number of components
     * and having the given component in every dimension
     * @param size The number of components this box will have.
     * @param component Component to initialize every dimension of this box.
     * @param orientation The orientation this box will have.
     */
    Box(int size, const Component &component, BoxOrientation orientation);

    /**
     * @brief Constructs a box with positive orientation from the given vector of components.
     * The i-th component will be the copy of i-th vector element.
     * @param components Vector of components to initialize this instance.
     */
    Box(std::vector<Component> components):
        orientation_(BoxOrientation::POSITIVE), components_(std::move(components)) { }

    /**
     * @brief Constructs a box with the given orientation from the given vector of components.
     * The i-th component will be the copy of i-th vector element.
     * @param components Vector of components to initialize this instance.
     * @param orientation The orientation this box will have.
     */
    Box(std::vector<Component> components, BoxOrientation orientation):
        orientation_(orientation), components_(std::move(components)) { }

    /**
     * @brief Computes a face of this box in the given dimension (index).
     *
     * If this box has components I_1, ..., I_n, @p index = k and I_k = [a, b],
     * then a box whose k-th component is either [a, a] or [b, b], depending
     * on the value of @p bottom, with the rest of the components unchanged, is returned.
     * @param index Which component to collapse. See full description.
     * @param bottom If true, then lower face is computed, i.e. I_k becomes [a, a]. Otherwise
     * upper face is computed, i.e. I_k becomes [b, b]. See full description.
     * @return Computed face.
     * @pre 0 <= index < getSize()
     */
    Box computeTopologicalFace(int index, bool bottom) const;

    /**
     * @brief Computes the set of oriented faces of this box.
     *
     * For a box B = I_1, ..., I_n, where I_j1, ..., I_jk are all
     * non degenerated faces of B, j1 < ... < jk, the order of computed faces in the
     * resulting list is following:
     * #computeTopologicalFace(j1, false), #computeTopologicalFace(j1, true),
     * ...,
     * #computeTopologicalFace(jk, false), #computeTopologicalFace(jk, true)
     * @return The set of oriented faces of this box as a list of boxes.
     */
    BoxList computeInducedBoundary() const;

    /**
     * @brief Compares this box to given other box.
     * @param other
     * @return True, if instances are semantically equal, meaning that they
     * have the same number of components, they have the same component in every dimension
     * and they have the say orientation.
     */
    bool operator==(const Box &other) const {
        return (orientation_ == other.orientation_ && components_ == other.components_);
    }

    /**
     * @brief A shorthand for !(this == other).
     * @param other
     * @return
     */
    bool operator!=(const Box &other) const {
        return (orientation_ != other.orientation_ || components_ != other.components_);
    }

    /**
     * @brief Sends the given box to the given std::ostream.
     * @param out The given output stream.
     * @param box The given box.
     * @return Output stream referenced by @p out.
     */
    friend std::ostream& operator<<(std::ostream &out, const Box &box);

    /**
     * @brief Bisects this instance with the given bisector and returns
     * the second part of the bisection result.
     * (This instance itself becomes the first part of the result.)
     * @param bisector Bisector to perform the bisection with.
     * @return Second part of the bisection result.
     */
    Box bisectBy(const bisector::IBisector &bisector);

    /**
     * @brief Bisects this instance according to already computed bisection result and returns
     * the second part of the bisection result.
     * (This instance itself becomes the first part of the result.)
     * @param result BisectionResult according to which the bisection will be performed.
     * @return Second part of the bisection result wrapped in unique pointer.
     */
    Box bisectBy(const bisector::BisectionResult &result);

    /**
     * @brief Gets the size of this box, which is the number of components it has.
     * @return The size of this box as integer.
     */
    int getSize() const {
        return components_.size();
    }

    /**
     * @brief Gets the topological dimension of this box, i.e. the number of
     * non-degenerated components.
     * @return The topological dimension of this box.
     */
    int getTopologicalDimension() const;

    /**
     * @brief Returns the i-th component, i.e. the component with index i.
     * @param i Index of the component.
     * @return The i-th component.
     * @pre 0 <= i < getSize()
     */
    const Component& getComp(size_t i) const;

    /**
     * @brief Sets the i-th component (i.e. the component with index i) to be the
     * copy of the given component.
     * @param i Index of the component to set.
     * @param component The new value of the i-th component.
     * @pre 0 <= i < getSize()
     */
    void setComp(size_t i, const Component &component);

    /**
     * @brief Creates a new box from this instance by collapsing the i-th component [a, b]
     * into [a, a].
     * @param i
     * @return Created sub-box.
     * @pre 0 <= i < getSize()
     */
    void collapseToLower(size_t i);

    /**
     * @brief Creates a new box from this instance by collapsing the i-th component [a, b]
     * into [b, b]
     * @param i
     * @return Created sub-box.
     * @pre 0 <= i < getSize()
     */
    void collapseToUpper(size_t i);

    /**
     * @brief Returns the index of the longest non-degenerated component or -1, if no such exists (i.e.
     * the box is degenerated)
     * @return Integer i, 0 <= i < getSize(), or -1 for degenerated box.
     */
    int getLongestDim() const;

    /**
     * @brief Computes the width of the longest component.
     * @return The width of the longest component.
     */
    double getDiam() const;

    /**
     * @brief Determines, whether this is a sub-box of the given other box, i.e. other is a subset
     * of this and this and other have the same topological dimension.
     * @param other Box
     * @return True, if this is subbox of other, otherwise false.
     */
    bool isSubboxOf(const Box &other) const;

    /**
     * @brief Determines, whether this is a subset of the given other box.
     * @param other Box
     * @return True, if this is a subset of other, otherwise false.
     */
    bool isSubsetOf(const Box &other) const;

    /**
     * @brief Determines, whether this box contains the zero point (0, 0, ..., 0)
     * @return True, if this contains the zero point. Otherwise false.
     */
    bool containsZero() const;

    /**
     * @brief Computes a sign vector for this box, where the i-th element corresponds
     * to the sign of the i-th component of the this box.
     *
     * If [a, b] is a Component and a > 0, then the corresponding sign is +.
     * If b < 0, then the corresponding sign is -.
     * Otherwise, the corresponding size is 0.
     * @return Sign vector for the given box.
     */
    SignVector computeSignVector() const;

    /**
     * @brief Computes the volume of this box.
     * @return Component containing the exact value of the volume.
     */
    Component getVolume() const;

    BoxOrientation getOrientation() const {
        return orientation_;
    }

    /**
     * @brief Sets the orientation for this box.
     * @param orientation The orientation to be set.
     */
    void setOrientation(BoxOrientation orientation) {
        orientation_ = orientation;
    }
};



}

}

#endif // BASEBOX_H
