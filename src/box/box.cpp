#include "box.h"
#include "signvector.h"
#include "../message/messageprocessor.h"
#include "../function/function.h"
#include "../bisector/bisector.h"

namespace vytlalib {

using bisector::BisectionResult;
using bisector::Bisector;
using bisector::IBisector;
using function::IFunction;
using message::ProgramException;

namespace boxes {

const Box EMPTY_BOX = Box();

/*---- namespace functions -----*/

bool isSignCoveringListDetermined(const SignCoveringList &sl) {
    for (const auto& signCovering : sl) {
        if (!signCovering.second.isSufficient())
            return false;
    }
    return true;
}

std::ostream& operator<<(std::ostream &out, const BoxSignVectorPair &sc) {
    out << (sc.first) << " " << sc.second;
    return out;
}

/*------- BoxOrientation -------*/

BoxOrientation operator*(const BoxOrientation &a, const BoxOrientation &b) {
    return (a == b) ? BoxOrientation::POSITIVE : BoxOrientation::NEGATIVE;
}

BoxOrientation operator!(const BoxOrientation &a) {
    return a == BoxOrientation::POSITIVE ? BoxOrientation::NEGATIVE : BoxOrientation::POSITIVE;
}

std::ostream& operator<<(std::ostream &out, const BoxOrientation &orientation) {
    out << (orientation == BoxOrientation::POSITIVE ? "POS" : "NEG");
    return out;
}

/*-------------- Box -----------------*/

Box Box::bisectBy(const IBisector &bisector) {
    // Analyze for bisection
    BisectionResult result = bisector.bisect(*this);
    if (result.index < 0)
        return EMPTY_BOX;
    vytlassert( (result.index >= 0 && result.index < getSize()),
                "Bisection result index must be >= 0 and < size of the box");

    const Component &b = getComp(result.index);
    if (result.step == b.lower() || result.step == b.upper())
        return EMPTY_BOX;
    vytlassert( (result.step > b.lower() && result.step < b.upper()),
                "Bisection step must be stricly within the components endpoints.");

    // Actual bisection
    Box other = Box(*this);
    other.setComp(result.index, Component(result.step, b.upper()));
    setComp(result.index, Component(b.lower(), result.step));

    return other;
}

Box Box::bisectBy(const bisector::BisectionResult &result) {
    vytlassert( (result.index >= 0 && result.index < getSize()),
                "Bisection result index must be >= 0 and < size of the box");
    const Component &b = getComp(result.index);
    vytlassert( (result.step > b.lower() && result.step < b.upper()),
                "Bisection step must be stricly within the components endpoints.");

    // Actual bisection
    Box other = Box(*this);
    other.setComp(result.index, Component(result.step, b.upper()));
    setComp(result.index, Component(b.lower(), result.step));

    return other;
}

Box Box::computeTopologicalFace(int index, bool bottom, int rank) const {
    Box result = Box(*this);
    const Component &b = result.getComp(index);
    // induced orientation is computed by the definition of faces of a box.
    if (bottom) {
        result.setComp(index, Component(b.lower(), b.lower()));
        result.orientation_ = orientation_ * (rank % 2 == 0 ? BoxOrientation::POSITIVE : BoxOrientation::NEGATIVE);
    } else {
        result.setComp(index, Component(b.upper(), b.upper()));
        result.orientation_ = orientation_ * (rank % 2 == 1 ? BoxOrientation::POSITIVE : BoxOrientation::NEGATIVE);
    }

    return result;
}

Box Box::computeTopologicalFace(int index, bool bottom) const {
    if (index < 0 || index >= static_cast<int>(components_.size()))
        throw ProgramException("Index out of bounds");
    int rank = 0;
    // rank of a component I_k of a box B = I_1, ..., I_n is the number of non-degenerated
    // components in the sequence I_1, ..., I_k
    for (int i = 0; i <= index; ++i) {
        if (!components_[i].isConstant())
            ++rank;
    }

    return computeTopologicalFace(index, bottom, rank);
}

//--- Friend functions

std::ostream& operator<<(std::ostream &out, const Box &box) {
    out << "Box";
    out << "[ ";
    for (auto c : box.components_) {
        out << c << " ";
    }
    out << "]";
    out << (box.orientation_ == BoxOrientation::POSITIVE ? "POS" : "NEG");
    return out;
}

//--- public methods and ctors

Box::Box(int size, const Component &component):
    orientation_(BoxOrientation::POSITIVE) {
    if (size < 0)
        throw ProgramException("Negative size given during box construction");
    components_ = std::vector<Component>(size, component);
}

Box::Box(int size, const Component &component, BoxOrientation orientation):
    orientation_(orientation) {
    if (size < 0)
        throw ProgramException("Negative size given during box construction");
    components_ = std::vector<Component>(size, component);
}

BoxList Box::computeInducedBoundary() const {
    BoxList result;
    int rank = 0;
    for (int i = 0; i < getSize(); ++i) {
        if (!(getComp(i).isConstant())) {
            ++rank;
            result.push_back(computeTopologicalFace(i, true, rank));
            result.push_back(computeTopologicalFace(i, false, rank));
        }
    }
    return result;
}


const Component& Box::getComp(size_t i) const {
    // Using 'at' to perform bounds checking
    return components_.at(i);
}

void Box::setComp(size_t i, const Component &component) {
    // Using 'at' to perform bounds checking
    components_.at(i) = component;
}

void Box::collapseToLower(size_t i) {
    // Using 'at' to perform bounds checking
    components_.at(i) = Component(components_[i].lower(), components_[i].lower());
}

void Box::collapseToUpper(size_t i) {
    // Using 'at' to perform bounds checking
    components_.at(i) = Component(components_[i].upper(), components_[i].upper());
}

int Box::getTopologicalDimension() const {
    int result = 0;
    for (auto component : components_) {
        if (!component.isConstant())
            ++result;
    }
    return result;
}

int Box::getLongestDim() const {
    double longestWidth = 0.0;
    int longestDim = -1;

    int dim = components_.size();
    for (int i = 0; i < dim; ++i) {
        double currentWidth = components_[i].width();

        if (currentWidth > longestWidth) {
            longestWidth = currentWidth;
            longestDim = i;
        }
    }
    return longestDim;
}

double Box::getDiam() const {
    return getComp(getLongestDim()).width();
}

bool Box::isSubboxOf(const Box &other) const {
    if (getSize() != other.getSize())
        return false;
    for (uint i = 0; i < components_.size(); ++i) {
        if (!components_[i].isSubsetOf(other.components_[i]) ||
            (components_[i].isConstant() && !other.components_[i].isConstant()) )
            return false;
    }
    return true;
}

bool Box::isSubsetOf(const Box &other) const {
    if (components_.size() != other.components_.size())
        return false;
    for (uint i = 0; i < components_.size(); ++i) {
        if (!components_[i].isSubsetOf(other.components_[i]))
            return false;
    }
    return true;
}

bool Box::containsZero() const {
    for (auto &c : components_) {
        if (!c.contains(0))
            return false;
    }
    return true;
}

SignVector Box::computeSignVector() const {
    // TODO Assert on box size > 0
    SignVector result(getSize(), Sign::ZERO);
    for (int i = 0; i < getSize(); ++i) {
        if (getComp(i).lower() > 0.0) // TODO: Replace 0.0 with constants
            result[i] = Sign::PLUS;
        else if (getComp(i).upper() < 0.0)
            result[i] = Sign::MINUS;
    }
    return result;
}

Component Box::getVolume() const {
    Component result("1.0");
    for (const Component &c : components_) {
        result *= Component(c.width());
    }
    return result;
}

}
}
