#include <ostream>
#include "scanner.h"
namespace vytlalib {
namespace scanner {

std::ostream& operator <<(std::ostream &out, const TokenType &type) {
    std::string result;
    switch (type) {
    case TokenType::init:
        out << "init";
        break;
    case TokenType::eof:
        out << "eof";
        break;
    case TokenType::plus:
        out << "+";
        break;
    case TokenType::minus:
        out << "-";
        break;
    case TokenType::mul:
        out << "*";
        break;
    case TokenType::div:
        out << "/";
        break;
    case TokenType::pow:
        out << "^";
        break;
    case TokenType::lparen:
        out << "(";
        break;
    case TokenType::rparen:
        out << ")";
        break;
    case TokenType::lbracket:
        out << "[";
        break;
    case TokenType::rbracket:
        out << "]";
        break;
    case TokenType::comma:
        out << ",";
        break;
    case TokenType::semicolon:
        out << ";";
        break;
    case TokenType::sqr:
        out << "sqr";
        break;
    case TokenType::sqrt:
        out << "sqrt";
        break;
    case TokenType::sqrtn:
        out << "sqrtn";
        break;
    case TokenType::sin:
        out << "sin";
        break;
    case TokenType::cos:
        out << "cos";
        break;
    case TokenType::tan:
        out << "tan";
        break;
    case TokenType::asin:
        out << "asin";
        break;
    case TokenType::acos:
        out << "acos";
        break;
    case TokenType::atan:
        out << "atan";
        break;
    case TokenType::log:
        out << "log";
        break;
    case TokenType::exp:
        out << "exp";
        break;
    case TokenType::abs:
        out << "abs";
        break;
    case TokenType::identifier:
        out << "ident";
        break;
    case TokenType::number:
        out << "num";
        break;
    case TokenType::ret:
        out << "ret";
        break;
    case TokenType::pi:
        out << "constant pi";
        break;
    case TokenType::e:
        out << "constant e";
        break;
    default:
        out << "???";
        break;
    }
    return out;
}

std::ostream& operator<<(std::ostream& out, Token token) {
    out << token.getType();
    if (token.getValue() != "")
        out << ": " << token.getValue();
    return out;
}

Token Scanner::extractToken() {
    // consume white spaces
    while (isWhiteSpace(peekChar()))
        getChar();

    if (isEofInput())
        return Token(TokenType::eof);

    char ch = getChar();
    switch (ch) {
    case '#':
        while (!isNewLine(peekChar()) && !isEofInput()) {
            getChar();
        }
        return extractToken();
    case '+':
        return Token(TokenType::plus);
    case '-':
        return Token(TokenType::minus);
    case '*':
        return Token(TokenType::mul);
    case '/':
        return Token(TokenType::div);
    case '(':
        return Token(TokenType::lparen);
    case ')':
        return Token(TokenType::rparen);
    case '[':
        return Token(TokenType::lbracket);
    case ']':
        return Token(TokenType::rbracket);
    case ',':
        return Token(TokenType::comma);
    case ';':
        return Token(TokenType::semicolon);
    case '^':
        return Token(TokenType::pow);
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9': {
        return scanNumberIntegralPart(ch);
    }
    case '.': {
        // TODO: must be followed by at least one digit
        return scanNumberFractionalPart(ch);
    }
    default:
        // TODO: report error if the ch is not letter
        return scanIdentifierOrFunction(ch);
    }
}

Token Scanner::scanNumberIntegralPart(char acc) {
    std::string result(1, acc);
    while (isDigit(peekChar())) {
        result += getChar();
    }
    if (peekChar() == '.') {
        result += getChar();
        return scanNumberFractionalPart(result);
    } else {
        return Token(TokenType::number, result);
    }
}

Token Scanner::scanNumberFractionalPart(char acc) {
    std::string result(1, acc);
    return scanNumberFractionalPart(result);
}

Token Scanner::scanNumberFractionalPart(std::string &acc) {
    while (isDigit(peekChar())) {
        acc += getChar();
    }
    return Token(TokenType::number, acc);
}

Token Scanner::scanIdentifierOrFunction(char acc) {
    std::string result(1, acc);
    while (isLetterOrDigit(peekChar())) {
        result += getChar();
    }

    if (result == "sqr") {
        return Token(TokenType::sqr);
    } else if (result == "sqrt") {
        return Token(TokenType::sqrt);
    } else if (result.length() > 4 && result.substr(0, 4) == "sqrt") {
        std::string arg = result.substr(4);
        bool allDigits = true;
        for (char ch : arg) {
            if (!isDigit(ch)) {
                allDigits = false;
                break;
            }
        }
        return allDigits ? Token(TokenType::sqrtn, arg) : Token(TokenType::identifier, result);
    } else if (result == "sin") {
        return Token(TokenType::sin);
    } else if (result == "cos") {
        return Token(TokenType::cos);
    } else if (result == "tan" || result == "tg") {
        return Token(TokenType::tan);
    } else if (result == "asin" || result == "arcsin") {
        return Token(TokenType::asin);
    } else if (result == "acos" || result == "arccos") {
        return Token(TokenType::acos);
    } else if (result == "atan" || result == "arctan" || result == "arctg") {
        return Token(TokenType::atan);
    } else if (result == "log") {
        return Token(TokenType::log);
    } else if (result == "exp") {
        return Token(TokenType::exp);
    } else if (result == "abs") {
        return Token(TokenType::abs);
    } else if (result == "e") {
        return Token(TokenType::e);
    } else if (result == "pi") {
        return Token(TokenType::pi);
    } else if (result == "ret" || result == "return") {
        return Token(TokenType::ret);
    } else {
        return Token(TokenType::identifier, result);
    }
}

}

}
