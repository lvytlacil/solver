#include <iostream>
#include "ast.h"
namespace vytlalib {
namespace ast {

/*-------------- AstVisitor interface --------------------*/

void Node::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void FormalArgumentList::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void Expression::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void ReturnStatement::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void FunctionDefinition::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void Number::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void NumberInterval::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void Identifier::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void BinOp::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void UnaryFunc::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void UnaryFuncWithIntArg::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void NullaryFunc::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

void UnaryMinus::visitBy(AstVisitor *visitor) {
    visitor->visit(this);
}

/*-------------------- toStream overloads --------------------------------------*/

void Node::toStream(std::ostream &out) {
    out << "Node";
}

void FormalArgumentList::toStream(std::ostream &out) {
    out << "Args[";
    for (uint i = 0; i < argSize; ++i) {
        out << i << " ";
    }
    out << "]";
}

void Expression::toStream(std::ostream &out) {
    out << "Expression";
}

void ReturnStatement::toStream(std::ostream &out) {
    out << "Return";
    out << "(dim:" << components.size() << ")";
}

void FunctionDefinition::toStream(std::ostream &out) {
    out << "FunctionDefinition";
}

void Number::toStream(std::ostream &out) {
    out << "Num(val:" << value;
}

void NumberInterval::toStream(std::ostream &out) {
    out << "NumInt[" << leftValue << ", " << rightValue << "]";
}

void Identifier::toStream(std::ostream &out) {
    out << "Ident(id: " << index << ")";
}

void BinOp::toStream(std::ostream &out) {
    out << "BinaryOp";
}

void UnaryFunc::toStream(std::ostream &out) {
    out << "UnaryFunc";
}

void UnaryFuncWithIntArg::toStream(std::ostream &out) {
    out << "UnaryFuncWithIntArg";
}

void NullaryFunc::toStream(std::ostream &out) {
    out << "Constant";
}

void UnaryMinus::toStream(std::ostream &out) {
    out << "UnaryMinus";
}



/*------------------ TreePostOrderLinearizer -----------------------*/

std::ostream& operator <<(std::ostream &out, const TreePostOrderLinearizer::Instruction &instr) {
    out << "[";
    typedef TreePostOrderLinearizer::OpCode OpCode;
    switch (instr.opcode) {
    case OpCode::unknown:
        out << "unknown";
        break;
    case OpCode::arglist:
        out << "arglist";
        break;
    case OpCode::ret:
        out << "ret";
        break;
    case OpCode::funcdef:
        out << "funcdef";
        break;
    case OpCode::num:
        out << "num";
        break;
    case OpCode::numinterval:
        out << "numinterval";
        break;
    case OpCode::ident:
        out<< "ident";
        break;
    case OpCode::binop:
        out << "binop";
        break;
    case OpCode::unaryfunc:
        out << "unaryfunc";
        break;
    case OpCode::unaryfunc_intarg:
        out << "unaryfunc_intarg";
        break;
    case OpCode::nullaryfunc:
        out << "nullaryfunc";
        break;
    case OpCode::unaryminus:
        out << "unaryminus";
        break;
    default:
        out << "?";
    }
    out << "] ";
    out << *instr.node;
    return out;
}

std::vector<TreePostOrderLinearizer::Instruction> TreePostOrderLinearizer::linearize(Node *root) {
    root->visitBy(this);
    return data_;
}

void TreePostOrderLinearizer::visit(Node *node) {
    assert(false);
}

void TreePostOrderLinearizer::visit(Expression *node) {
    assert(false); // TODO
}

void TreePostOrderLinearizer::visit(BinOp *node) {
    // we want rhs on the top of lhs
    node->lhs->visitBy(this);
    node->rhs->visitBy(this);
    data_.push_back(Instruction(OpCode::binop, node));
}

void TreePostOrderLinearizer::visit(FormalArgumentList *node) {
    data_.push_back(Instruction(OpCode::arglist, node));
}

void TreePostOrderLinearizer::visit(FunctionDefinition *node) {
    // We want argList to be on top of the list of expressions, and those to be
    // on top of return statement
    node->retStatement->visitBy(this);
    for (auto expr : node->expressions) {
        expr->visitBy(this);
    }
    node->argList->visitBy(this);
    data_.push_back(Instruction(OpCode::funcdef, node));
}

void TreePostOrderLinearizer::visit(Identifier *node) {
    data_.push_back(Instruction(OpCode::ident, node));
}

void TreePostOrderLinearizer::visit(NullaryFunc *node) {
    data_.push_back(Instruction(OpCode::nullaryfunc, node));
}

void TreePostOrderLinearizer::visit(Number *node) {
    data_.push_back(Instruction(OpCode::num, node));
}

void TreePostOrderLinearizer::visit(NumberInterval *node) {
    // we want right argument to be on the top of left argument
    node->left->visitBy(this);
    node->right->visitBy(this);
    data_.push_back(Instruction(OpCode::numinterval, node));
}

void TreePostOrderLinearizer::visit(ReturnStatement *node) {
    // We want the 0-th component to be on the top
    for (int i = node->components.size() - 1; i >= 0; --i) {
        node->components[i]->visitBy(this);
    }
    data_.push_back(Instruction(OpCode::ret, node));
}

void TreePostOrderLinearizer::visit(UnaryFunc *node) {
    node->arg->visitBy(this);
    data_.push_back(Instruction(OpCode::unaryfunc, node));
}

void TreePostOrderLinearizer::visit(UnaryFuncWithIntArg *node) {
    node->arg->visitBy(this);
    data_.push_back(Instruction(OpCode::unaryfunc_intarg, node));
}

void TreePostOrderLinearizer::visit(UnaryMinus *node) {
    node->arg->visitBy(this);
    data_.push_back(Instruction(OpCode::unaryminus, node));
}

/*---------------- Printer -------------------------*/

class Printer : public AstVisitor {
private:
    std::ostream &out;
    std::string separator = " ";

public:

    Printer(std::ostream &out):
        out(out) { }

    void visit(Node * node) override {
        out << "INode (?)" << separator;
    }

    void visit(FormalArgumentList * node) override {
        out << "[";
        for (uint i = 0; i < node->argSize; ++i) {
            out << "ID: " << i;
        }
        out << "]" << separator;
    }

    void visit(Expression * node) override {
        out << "Expression (?)" << separator;
    }

    virtual void visit(ReturnStatement * node) {
        out << "ret[";
        for (auto c : node->components) {
            c->visitBy(this);
        }
        out << "]";
    }

    virtual void visit(FunctionDefinition * node) {
        node->argList->visitBy(this);
        for (auto exp : node->expressions) {
            exp->visitBy(this);
        }
        node->retStatement->visitBy(this);
    }

    virtual void visit(Number * node) {
        out << node->value << separator;
    }

    virtual void visit(NumberInterval * node) {
        out << "[";
        node->left->visitBy(this);
        out << ", ";
        node->right->visitBy(this);
        out << "]" << separator;
    }

    virtual void visit(Identifier * node) {
        out << "ID:" << node->index << separator;
    }

    virtual void visit(BinOp * node) {
        node->lhs->visitBy(this);
        out << "BINOP" << separator;
        node->rhs->visitBy(this);
    }

    virtual void visit(UnaryFunc * node) {
        out << "UNARY(";
        node->arg->visitBy(this);
        out << ")" << separator;
    }

    virtual void visit(UnaryFuncWithIntArg * node) {
        out << "UNARYIARG" << node->n << "(";
        node->arg->visitBy(this);
        out <<  ")" << separator;
    }

    virtual void visit(NullaryFunc * node) {
        out << "CONSTANT" << separator;
    }

    virtual void visit(UnaryMinus * node) {
        out << "-";
        node->arg->visitBy(this);
        out << separator;
    }
};

std::ostream& operator<<(std::ostream &out, Node &node) {
    Printer p(out);
    node.visitBy(&p);
    return out;
}

/*---------------Tree duplicator----------------------*/
void TreeDuplicator::visit(Node *node) {
    assert(false);
}

void TreeDuplicator::visit(Expression *node) {
    assert(false);
}

void TreeDuplicator::visit(BinOp *node) {
    node->lhs->visitBy(this);
    std::unique_ptr<Expression> lhs = std::move(interExpr_);
    node->rhs->visitBy(this);
    std::unique_ptr<Expression> rhs = std::move(interExpr_);
    interExpr_.reset(new BinOp(lhs.release(), rhs.release(), node->type));
}

void TreeDuplicator::visit(FormalArgumentList *node) {
    interFormalArgList_.reset(new FormalArgumentList(node->argSize));
}

void TreeDuplicator::visit(FunctionDefinition *node) {
    interFuncDef_.reset(new FunctionDefinition());
    node->argList->visitBy(this);
    interFuncDef_->argList = std::move(interFormalArgList_);
    for (auto& expr : node->expressions) {
        expr->visitBy(this);
        interFuncDef_->expressions.push_back(std::move(interExpr_));
    }
    node->retStatement->visitBy(this);
    interFuncDef_->retStatement = std::move(interRetStmt_);
}

void TreeDuplicator::visit(Identifier *node) {
    interExpr_.reset(new Identifier(node->index));
}

void TreeDuplicator::visit(NullaryFunc *node) {
    interExpr_.reset(new NullaryFunc(node->type));
}

void TreeDuplicator::visit(Number *node) {
    interExpr_.reset(new Number(node->value));
}

void TreeDuplicator::visit(NumberInterval *node) {
    node->left->visitBy(this);
    std::unique_ptr<Expression> left = std::move(interExpr_);
    node->right->visitBy(this);
    std::unique_ptr<Expression> right = std::move(interExpr_);
    interExpr_.reset(new NumberInterval(left.release(), right.release()));
}

void TreeDuplicator::visit(ReturnStatement *node) {
    interRetStmt_.reset(new ReturnStatement());
    for (auto& expr : node->components) {
        expr->visitBy(this);
        interRetStmt_->components.push_back(std::move(interExpr_));
    }
}

void TreeDuplicator::visit(UnaryFunc *node) {
    node->arg->visitBy(this);
    interExpr_.reset(new UnaryFunc(interExpr_.release(), node->type));
}

void TreeDuplicator::visit(UnaryFuncWithIntArg *node) {
    node->arg->visitBy(this);
    interExpr_.reset(new UnaryFuncWithIntArg(node->n, interExpr_.release(), node->type));
}

void TreeDuplicator::visit(UnaryMinus *node) {
    node->arg->visitBy(this);
    interExpr_.reset(new UnaryMinus(interExpr_.release()));
}


}
}
