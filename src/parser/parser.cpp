#include <sstream>
#include "../message/messageprocessor.h"
#include "parser.h"
#include "../function/function.h"
#include "ast.h"
#include "scanner.h"
#include "sstream"
namespace vytlalib {

using namespace ast;
using scanner::Token;
using scanner::TokenType;
using scanner::Scanner;
using input::Input;
using function::Function;
using message::Message;
using message::Severity;
using message::ProgramException;
static auto &mproc = message::IMessageProcessor::mproc;

namespace parser {

bool FunctionParser::isSeparator(scanner::TokenType type) {
    return type == TokenType::comma || type == TokenType::semicolon;
}

scanner::Token FunctionParser::consume(scanner::TokenType type) {
    check(type);
    assert(current_.getType() == type);
    scanNext();
    return getCurrent();
}

scanner::Token FunctionParser::consumeSeparator() {
    if (!isSeparator(current_.getType())) {
        throw ProgramException("Parser: Excpected separator, but some other token was found.");
    }
    do {
        scanNext();
    } while (isSeparator(current_.getType()));
    return getCurrent();
}

void FunctionParser::check(scanner::TokenType type) {
    if (type != current_.getType()) {
        std::stringstream ss;
        ss << "Expected " << type << " terminal, but got " << current_.getType();
        Message msg(ss.str() , typeid(*this).name(), Severity::error);
        mproc->process(msg);
    }
}


std::shared_ptr<FunctionDefinition> FunctionParser::parse(std::string in) {
    std::stringstream ss(in);
    return parse(ss);
}


std::shared_ptr<FunctionDefinition> FunctionParser::parse(std::istream &in) {
    std::unique_ptr<Input> input(new Input(in));
    scanner_.reset(new Scanner(std::move(input)));

    scanNext(); // Get the first token

    return std::shared_ptr<FunctionDefinition>(parseFunctionDefinition());
}

std::shared_ptr<FunctionDefinition> FunctionParser::parseBoxExpression(std::string in) {
    std::stringstream ss(in);
    return parseBoxExpression(ss);
}

std::shared_ptr<ast::FunctionDefinition> FunctionParser::parseBoxExpression(std::istream &in) {
    std::unique_ptr<Input> input(new Input(in));
    scanner_.reset(new Scanner(std::move(input)));

    scanNext(); // Get the first token

    return std::shared_ptr<ast::FunctionDefinition>(parseBoxExpression());
}

// S ::= '[' [ EXPRESSION ] ']' , { [ SEPARATOR ] }
ast::FunctionDefinition* FunctionParser::parseBoxExpression() {
    auto fnc = std::unique_ptr<FunctionDefinition>(new FunctionDefinition());
    auto args = std::unique_ptr<FormalArgumentList>(new FormalArgumentList(0));
    fnc->argList.reset(args.release()); // Empty arg list
    // No expressions, just return statement

    auto retStatement = std::unique_ptr<ReturnStatement>(parseReturnStatement());
    /*consume(TokenType::lbracket);
    while (getCurrent().getType() != TokenType::rbracket) {
        retStatement->pushComponent(parseExpression());
        consumeSeparator();
    }
    check(TokenType::rbracket); // Only check, do not scan next*/
    fnc->retStatement.reset(retStatement.release());
    return fnc.release();
}

// S ::= ARGS , [EXPRESSION] , RET_STATEMENT
FunctionDefinition* FunctionParser::parseFunctionDefinition() {
    auto fnc = std::unique_ptr<FunctionDefinition>(new FunctionDefinition());
    fnc->argList.reset(parseArgs());
    fnc->retStatement.reset(parseReturnStatement());
    return fnc.release();
    /*
    while (true) {
        switch (current_.getType()) {
        case TokenType::eof: {
            std::stringstream ss;
            ss << "Unexpected end of input ";
            Message msg(ss.str(), typeid(*this).name(), Severity::error);
            mproc->process(msg);
            return fnc.release();
        }
        case TokenType::ret:
            fnc->retStatement.reset(parseReturnStatement());
            return fnc.release();
        default:
            fnc->expressions.push_back(std::shared_ptr<Node>(parseExpression()));
            break;
        }
    }*/
}

// ARGS ::= '[' , [ IDENT ] , ']'
FormalArgumentList* FunctionParser::parseArgs() {
    consume(TokenType::lbracket);
    while (current_.getType() == TokenType::identifier) {
        string s = current_.getValue();
        if (symbolTable_.count(s) == 0) {
            int index = symbolTable_.size();
            symbolTable_[s] = index;
        } else {
            std::stringstream ss;
            ss << "Formal argument " << s << " redeclared.";
            Message msg(ss.str(), typeid(*this).name(), Severity::error);
            mproc->process(msg);
        }
        consume(TokenType::identifier);
        // Consume optional separators
        if (isSeparator(current_.getType())) {
            consumeSeparator();
        }
    }
    consume(TokenType::rbracket);
    // Create and return arglist node
    auto argList = std::unique_ptr<FormalArgumentList>(new FormalArgumentList(symbolTable_.size()));
    return argList.release();
}

// RET_STATEMENT ::= '[' , [ EXPRESSION, (';' | ',') ] , ']'
ReturnStatement* FunctionParser::parseReturnStatement() {
    consume(TokenType::lbracket);

    std::unique_ptr<ReturnStatement> ret(new ReturnStatement());
    while (current_.getType() != TokenType::rbracket) {
        ret->pushComponent(parseExpression());
        if (current_.getType() != TokenType::rbracket) {
            consumeSeparator();
        }
    }
    check(TokenType::rbracket); // Only check, do not scan next
    return ret.release();
}

// EXPRESSION ::= E1 , { (+ | -) , E1}
Expression *FunctionParser::parseExpression() {
    std::unique_ptr<Expression> node(parseE1());
    while (true) {
        switch (getCurrent().getType()) {
        case TokenType::plus:
            consume(TokenType::plus);
            node.reset(new BinOp(node.release(), parseE1(), BinOp::Type::add));
            break;
        case TokenType::minus:
            consume(TokenType::minus);
            node.reset(new BinOp(node.release(), parseE1(), BinOp::Type::sub));
            break;
        default:
            return node.release();
        }
    }
}

// E1 ::= E2 , { (* | /) , E2 }
Expression *FunctionParser::parseE1() {
    std::unique_ptr<Expression> node(parseE2());
    while (true) {
        switch (getCurrent().getType()) {
        case TokenType::mul:
            consume(TokenType::mul);
            node.reset(new BinOp(node.release(), parseE2(), BinOp::Type::mul));
            break;
        case TokenType::div:
            consume(TokenType::div);
            node.reset(new BinOp(node.release(), parseE2(), BinOp::Type::div));
            break;
        default:
            return node.release();
        }
    }
}

// E2 ::= E3 , { ^ , E3 }
Expression *FunctionParser::parseE2() {
    std::unique_ptr<Expression> node(parseE3());
    while (true) {
    switch (getCurrent().getType()) {
    case TokenType::pow:
        consume(TokenType::pow);
        node.reset(new BinOp(node.release(), parseE3(), BinOp::Type::pow));
        break;
    default:
        return node.release();
    }
    }
}

// E3 ::= ( (+ | -) , E3 ) | E4
Expression *FunctionParser::parseE3() {
    bool positive = true;
    while (true) {
        switch (current_.getType()) {
        case TokenType::minus:
            consume(TokenType::minus);
            positive = !positive;
            break;
        case TokenType::plus:
            consume(TokenType::plus);
            break;
        default:
            if (positive) {
                return parseE4();
            } else {
                auto result = std::unique_ptr<Expression>(new UnaryMinus(parseE4()));
                return result.release();
            }
        }
    }
}

// E4 ::= NUMBER | NUMINTERVAL | IDENT | UNARYFUNC | NULLARYFUNC | ( '(' , EXPRESSION , ')' )
Expression *FunctionParser::parseE4() {
    switch (getCurrent().getType()) {
    case TokenType::number: {
        std::unique_ptr<Number> result (new Number(getCurrent().getValue()));
        consume(TokenType::number);
        return result.release();
    }
    case TokenType::lbracket:
        return parseNumberInterval();
    case TokenType::identifier: {
        string s = current_.getValue();
        if (symbolTable_.count(s) > 0) {
            std::unique_ptr<Identifier> result (new Identifier(symbolTable_[s]));
            consume(TokenType::identifier);
            return result.release();
        } else {
            std::stringstream ss;
            ss << "Undeclared identifier\"" << s << "\" encountered.";
            Message msg(ss.str(), typeid(*this).name(), Severity::error);
            mproc->process(msg);
        }
    }
    case TokenType::abs:
        consume(TokenType::abs);
        return parseUnaryFuncCall(UnaryFunc::Type::abs);
    case TokenType::acos:
        consume(TokenType::acos);
        return parseUnaryFuncCall(UnaryFunc::Type::acos);
    case TokenType::asin:
        consume(TokenType::asin);
        return parseUnaryFuncCall(UnaryFunc::Type::asin);
    case TokenType::atan:
        consume(TokenType::atan);
        return parseUnaryFuncCall(UnaryFunc::Type::atan);
    case TokenType::cos:
        consume(TokenType::cos);
        return parseUnaryFuncCall(UnaryFunc::Type::cos);
    case TokenType::exp:
        consume(TokenType::exp);
        return parseUnaryFuncCall(UnaryFunc::Type::exp);
    case TokenType::log:
        consume(TokenType::log);
        return parseUnaryFuncCall(UnaryFunc::Type::log);
    case TokenType::sin:
        consume(TokenType::sin);
        return parseUnaryFuncCall(UnaryFunc::Type::sin);
    case TokenType::sqr:
        consume(TokenType::sqr);
        return parseUnaryFuncCall(UnaryFunc::Type::sqr);
    case TokenType::sqrt:
        consume(TokenType::sqrt);
        return parseUnaryFuncCall(UnaryFunc::Type::sqrt);
    case TokenType::tan:
        consume(TokenType::tan);
        return parseUnaryFuncCall(UnaryFunc::Type::tan);
    case TokenType::sqrtn:
        return parseUnaryFuncWithIntArgCall(UnaryFuncWithIntArg::Type::sqrtn);
    case TokenType::e:
        consume(TokenType::e);
        return new NullaryFunc(NullaryFunc::Type::e);
    case TokenType::pi:
        consume(TokenType::pi);
        return new NullaryFunc(NullaryFunc::Type::pi);
    case TokenType::lparen: {
        consume(TokenType::lparen);
        std::unique_ptr<Expression> result(parseExpression());
        consume(TokenType::rparen);
        return result.release();
    }
    default:
        break;
    }
    std::stringstream ss;
    ss << "Unexpected token of type " << current_.getType();
    Message msg(ss.str(), typeid(*this).name(), Severity::error);
    mproc->process(msg);
    return nullptr; // not reached
}

UnaryFunc* FunctionParser::parseUnaryFuncCall(UnaryFunc::Type type) {
    consume(TokenType::lparen);
    std::unique_ptr<Expression> arg(parseExpression());
    consume(TokenType::rparen);
    return new UnaryFunc(arg.release(), type);
}

UnaryFuncWithIntArg* FunctionParser::parseUnaryFuncWithIntArgCall(UnaryFuncWithIntArg::Type type) {
    int intArg = std::stoi(getCurrent().getValue());
    consume(TokenType::sqrtn);
    consume(TokenType::lparen);
    std::unique_ptr<Expression> arg(parseExpression());
    consume(TokenType::rparen);
    return new UnaryFuncWithIntArg(intArg, arg.release(), type);
}

// NUMINTERVAL ::= '[' , EXPRESSION , ',' , EXPRESSION , ']'
NumberInterval *FunctionParser::parseNumberInterval() {
    consume(TokenType::lbracket);
    auto left = std::unique_ptr<Expression>(parseExpression());
    consumeSeparator();
    auto right = std::unique_ptr<Expression>(parseExpression());
    consume(TokenType::rbracket);
    return new NumberInterval(left.release(), right.release());
}

}

}
