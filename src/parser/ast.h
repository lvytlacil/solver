#ifndef AST_H
#define AST_H
#include <vector>
#include <iosfwd>
#include <memory>
#include "../component.h"
namespace vytlalib {
namespace ast {

class AstVisitor;

class BitField64 {
public:
    typedef unsigned long long UInt64Type;
    UInt64Type data_;
    static const UInt64Type NumberOne = 1;
    static const UInt64Type NumberZero = 0;

    BitField64(UInt64Type data):
        data_(data) { }
public:

    static BitField64 createEmpty() {
        return BitField64(NumberZero);
    }

    static BitField64 createFull() {
        return BitField64(~NumberZero);
    }

    static BitField64 firstN_On(uint index) {
        return BitField64(~(~NumberZero << index));
    }

    static BitField64 firstN_Off(uint index) {
        return BitField64(~NumberZero << index);
    }

    static BitField64 intersect(const BitField64 &left, const BitField64 &right) {
        return BitField64(left.data_ & right.data_);
    }

    static BitField64 unite(const BitField64 &left, const BitField64 &right) {
        return BitField64(left.data_ | right.data_);
    }

    void setOn(uint index) {
        data_ |= (1 << index);
    }

    void setOff(uint index) {
        data_ &= ~(1 << index);
    }

    void intersect(const BitField64 &other) {
        data_ &= other.data_;
    }

    void unite(const BitField64 &other) {
        data_ |= other.data_;
    }

    bool isOn(uint index) const {
        return (data_ & (1 << index)) != 0;
    }

    bool isOff(uint index) const {
        return (data_ & (1 << index)) == 0;
    }

    BitField64 setOnImmutable(uint index) {
        return (BitField64(data_ | (1 << index)));
    }

    BitField64 setOffImmutable(uint index) {
        return (BitField64(data_ & (1 << index)));
    }

    bool isSubsetOf(const BitField64 &other) const {
        return (data_ | other.data_) == other.data_;
    }

    void fillWithZeros() {
        data_ &= 0;
    }
};

class Node {
public:
    BitField64 identsInUse;
    boxes::Component value;

    Node(BitField64 identsInUse):
        identsInUse(identsInUse), value(boxes::Component("0")) { }
    Node(BitField64 identsInUse, boxes::Component value):
        identsInUse(identsInUse), value(value) { }

    virtual ~Node() { }
    virtual void visitBy(AstVisitor *visitor) = 0;

    friend std::ostream& operator <<(std::ostream &out, Node &node);
    virtual void toStream(std::ostream &out);
};

class FormalArgumentList : public Node {
public:
    uint argSize;

    FormalArgumentList(int size):
       Node(BitField64::firstN_On(size)), argSize(size) { }

    void visitBy(AstVisitor *visitor);
    void toStream(std::ostream &out) override;
};

class Expression : public Node {
public:

    Expression(BitField64 identsInUse):
        Node(identsInUse) { }
    Expression(BitField64 identsInUse, boxes::Component value):
        Node(identsInUse, value) { }

    void visitBy(AstVisitor *visitor) override;
    void toStream(std::ostream &out) override;
};

class ReturnStatement : public Node {
public:
    std::vector<std::shared_ptr<Expression>> components;

    ReturnStatement():
        Node(BitField64::createEmpty()) { }

    void pushComponent(Expression *expression) {
        components.push_back(std::shared_ptr<Expression>(expression));
        identsInUse.unite(expression->identsInUse);
    }

    ~ReturnStatement() override {
        for (auto c : components) {
            c.reset();
        }
    }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class FunctionDefinition : public Node {
public:
    std::shared_ptr<FormalArgumentList> argList;
    std::vector<std::shared_ptr<Node>> expressions;
    std::shared_ptr<ReturnStatement> retStatement;

    FunctionDefinition():
        Node(BitField64::createFull()) { }

    ~FunctionDefinition() override {
        for (auto expr : expressions) {
            expr.reset();
        }
    }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class Number : public Expression {
public:

    Number(std::string strValue):
        Expression(BitField64::createEmpty(), boxes::Component(strValue.c_str())) { }

    Number(boxes::Component component):
        Expression(BitField64::createEmpty(), component) { }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class NumberInterval : public Expression {
public:
    std::shared_ptr<Expression> left;
    std::shared_ptr<Expression> right;
    std::string leftValue;
    std::string rightValue;

    NumberInterval(Expression* left, Expression* right):
        Expression(BitField64::unite(left->identsInUse, right->identsInUse)), left(left), right(right) { }
    ~NumberInterval() override {
        left.reset();
        right.reset();
    }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class Identifier : public Expression {
public:
    unsigned index;
    Identifier(unsigned index):
        Expression(BitField64::createEmpty().setOnImmutable(index)), index(index) { }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class BinOp : public Expression {
public:
    enum class Type {
        add,
        sub,
        mul,
        div,
        pow
    };

    std::shared_ptr<Expression> lhs;
    std::shared_ptr<Expression> rhs;
    Type type;

    BinOp(Expression* lhs, Expression* rhs, Type type):
        Expression(BitField64::unite(lhs->identsInUse, rhs->identsInUse)), lhs(lhs), rhs(rhs), type(type) { }

    ~BinOp() override {
        lhs.reset();
        rhs.reset();
    }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class UnaryFunc : public Expression {
public:
    enum class Type {
        abs,
        sin,
        cos,
        tan,
        exp,
        log,
        asin,
        atan,
        acos,
        sqr,
        sqrt,
    };

    std::shared_ptr<Expression> arg;
    Type type;

    UnaryFunc(Expression *arg, Type type):
        Expression(arg->identsInUse), arg(arg), type(type) { }

    ~UnaryFunc() override {
        arg.reset();
    }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class UnaryFuncWithIntArg : public Expression {
public:
    enum class Type {
        sqrtn
    };

    int n;
    std::shared_ptr<Expression> arg;
    Type type;

    UnaryFuncWithIntArg(int n, Expression *arg, Type type):
        Expression(arg->identsInUse), n(n), arg(arg), type(type) { }

    ~UnaryFuncWithIntArg() override {
        arg.reset();
    }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class NullaryFunc : public Expression {
public:
    enum class Type {
        pi,
        e
    };

    Type type;

    NullaryFunc(Type type):
        Expression(BitField64::createEmpty()), type(type) { }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

class UnaryMinus : public Expression {
public:
    std::shared_ptr<Expression> arg;

    UnaryMinus(Expression* arg):
        Expression(arg->identsInUse), arg(arg) { }

    ~UnaryMinus() override {
        arg.reset();
    }

    void visitBy(AstVisitor *visitor) override final;
    void toStream(std::ostream &out) override;
};

/*- Visitor class */

class AstVisitor {
public:
    virtual ~AstVisitor() { }

    virtual void visit(Node * node) { }
    virtual void visit(FormalArgumentList * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(Expression * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(ReturnStatement * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(FunctionDefinition * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(Number * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(NumberInterval * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(Identifier * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(BinOp * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(UnaryFunc * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(UnaryFuncWithIntArg * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(NullaryFunc * node) { visit(static_cast<Node*>(node)); }
    virtual void visit(UnaryMinus * node) { visit(static_cast<Node*>(node)); }
};

class TreePostOrderLinearizer : public AstVisitor {
public:
    enum class OpCode {
        unknown,
        arglist,
        ret,
        funcdef,
        num,
        numinterval,
        ident,
        binop,
        unaryfunc,
        unaryfunc_intarg,
        nullaryfunc,
        unaryminus
    };

    struct Instruction {
        OpCode opcode;
        Node* node;

        Instruction(OpCode opcode, Node* instr):
            opcode(opcode), node(instr) { }

        friend std::ostream& operator<<(std::ostream &out, const Instruction &instr);
    };

private:
    std::vector<Instruction> data_;

public:

    ~TreePostOrderLinearizer() override { }

    std::vector<Instruction> linearize(Node *root);

    void visit(Node *node) override;
    void visit(FormalArgumentList *node) override;
    void visit(Expression *node) override;
    void visit(ReturnStatement *node) override;
    void visit(FunctionDefinition *node) override;
    void visit(Number *node) override;
    void visit(NumberInterval *node) override;
    void visit(Identifier *node) override;
    void visit(BinOp *node) override;
    void visit(UnaryFunc *node) override;
    void visit(UnaryFuncWithIntArg *node) override;
    void visit(NullaryFunc *node) override;
    void visit(UnaryMinus *node) override;
};

class TreeDuplicator : public AstVisitor {
private:
    std::unique_ptr<Node> interNode_;
    std::unique_ptr<Expression> interExpr_;
    std::unique_ptr<FormalArgumentList> interFormalArgList_;
    std::unique_ptr<FunctionDefinition> interFuncDef_;
    std::unique_ptr<ReturnStatement> interRetStmt_;
public:

    ~TreeDuplicator() override { }

    std::shared_ptr<FunctionDefinition> duplicate(FunctionDefinition* node) {
        node->visitBy(this);
        return std::move(interFuncDef_);
    }

    void visit(Node *node) override;
    void visit(FormalArgumentList *node) override;
    void visit(Expression *node) override;
    void visit(ReturnStatement *node) override;
    void visit(FunctionDefinition *node) override;
    void visit(Number *node) override;
    void visit(NumberInterval *node) override;
    void visit(Identifier *node) override;
    void visit(BinOp *node) override;
    void visit(UnaryFunc *node) override;
    void visit(UnaryFuncWithIntArg *node) override;
    void visit(NullaryFunc *node) override;
    void visit(UnaryMinus *node) override;

};

}

}

#endif // AST_H
