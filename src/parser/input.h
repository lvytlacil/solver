#ifndef INPUT_H
#define INPUT_H
#include <iosfwd>
namespace vytlalib {
namespace input {

class Input
{
private:
    std::istream &in_;
public:
    /**
     * @brief Returns the next character in the input, but does not extract it.
     * @return Next character in the input.
     */
    char peekChar();

    /**
     * @brief Extracts and returns the next character in the input.
     * @return Next character in the input.
     */
    char getChar();

    bool isWhiteSpace() {
        return isWhiteSpace(peekChar());
    }

    bool isLetter() {
        return isLetter(peekChar());
    }

    bool isDigit() {
        return isDigit(peekChar());
    }

    bool isLetterOrDigit() {
        return isLetterOrDigit(peekChar());
    }

    bool isEof();
    bool isNewLine(char ch);
    bool isWhiteSpace(char ch);
    bool isLetter(char ch);
    bool isDigit(char ch);
    bool isLetterOrDigit(char ch);

    Input(std::istream &in):
        in_(in) { }

};

}
}
#endif // INPUT_H
