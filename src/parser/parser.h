#ifndef PARSER_H
#define PARSER_H
#include <map>
#include <memory>
#include "ast.h"
#include "scanner.h"

namespace vytlalib {
namespace ast {
    class Node;
    class Number;
    class NumberInterval;
    class Identifier;
    class BinOp;
    class UnaryFunc;
    class UnaryFuncWithIntArg;
}

namespace function {
    class Function;
}

namespace parser {

class FunctionParser
{
private:
    // Scanner to fetch tokens
    std::unique_ptr<scanner::Scanner> scanner_;
    // Current token
    scanner::Token current_ = scanner::Token(scanner::TokenType::eof);
    // Mapping of identifiers to theird IDs.
    std::map<std::string, uint> symbolTable_;

    //void consumeOptionalSeparators();

    ast::FunctionDefinition* parseFunctionDefinition();
    ast::FormalArgumentList* parseArgs();
    ast::ReturnStatement* parseReturnStatement();
    ast::Expression* parseExpression();
    ast::Expression* parseE1();
    ast::Expression* parseE2();
    ast::Expression* parseE3();
    ast::Expression* parseE4();
    ast::NumberInterval* parseNumberInterval();
    ast::UnaryFunc* parseUnaryFuncCall(ast::UnaryFunc::Type);
    ast::UnaryFuncWithIntArg* parseUnaryFuncWithIntArgCall(ast::UnaryFuncWithIntArg::Type);
    ast::Identifier* parseIdentifier();

    ast::FunctionDefinition* parseBoxExpression();

    bool isSeparator(scanner::TokenType type);
    scanner::Token consume(scanner::TokenType type);
    scanner::Token consumeSeparator();
    void check(scanner::TokenType type);

    scanner::Token getCurrent() {
        return current_;
    }

    scanner::Token scanNext() {
        current_ = scanner_->scanNext();
        return getCurrent();
    }

public:

    std::shared_ptr<ast::FunctionDefinition> parse(std::string in);
    std::shared_ptr<ast::FunctionDefinition> parse(std::istream &in);
    std::shared_ptr<ast::FunctionDefinition> parseBoxExpression(std::string in);
    std::shared_ptr<ast::FunctionDefinition> parseBoxExpression(std::istream &in);



    // Function definition:
    /* S ::= ARGS , RET_STATEMENT
     * ARGS ::= '[' , [ IDENT , [ SEPARATOR ] ] , ']'
     * RET_STATEMENT ::= '[]' |   ( '[' [EXPRESSION , SEPARATOR] , EXPRESSION ']' )
     * EXPRESSION ::= E1 , { (+ | -) , E1}
     * E1 ::= E2 , { (* | /) , E2 }
     * E2 ::= E3 , { ^ , E3 }
     * E3 ::= ( (+ | -) , E3 ) | E4
     * E4 ::= NUMBER | INTERVAL | IDENT | UNARYFUNCCALL | NULLARYFUNC | ( '(' , EXPRESSION ')' )
     * INTERVAL ::= '[' , EXPRESSION , SEPARATOR , EXPRESSION , ']'
     * UNARYFUNCALL ::= UNARYFUNC , '(' , EXPRESSION , ')'
     * SEPARATOR ::= ';' | ','
     */

    // Box Expression:
    /* S ::= '[' [ EXPRESSION ] ']'
    */
};


}

}

#endif // PARSER_H
