#ifndef SCANNER_H
#define SCANNER_H
#include <iosfwd>
#include "input.h"
#include "memory"
namespace vytlalib {

namespace scanner {

enum class TokenType {
    init,
    eof,
    plus,
    minus,
    mul,
    div,
    pow,
    sqr,
    sqrt,
    sqrtn,
    sin,
    cos,
    tan,
    asin,
    acos,
    atan,
    log,
    exp,
    abs,
    pi,
    e,
    identifier,
    lparen,
    rparen,
    lbracket,
    rbracket,
    comma,
    semicolon,
    number,
    ret
};

std::ostream& operator <<(std::ostream &out, const TokenType &type);

class Token {
private:
    TokenType type;
    std::string value;

public:
    Token(TokenType t):
        type(t) { }

    Token(TokenType t, const std::string &v):
        type(t), value(v) { }

    TokenType getType() {
        return type;
    }

    std::string getValue() {
        return value;
    }

    bool operator ==(const Token &other) {
        return type == other.type && value == other.value;
    }

    bool operator !=(const Token &other) {
        return !(*this == other);
    }

    friend std::ostream& operator<<(std::ostream& out, Token token);
};

class Scanner
{
private:

    std::unique_ptr<input::Input> input_;

    char peekChar() {
        return input_->peekChar();
    }

    char getChar() {
        return input_->getChar();
    }

    bool isWhiteSpace(char ch) {
        return input_->isWhiteSpace(ch);
    }

    bool isDigit(char ch) {
        return input_->isDigit(ch);
    }

    bool isLetter(char ch) {
        return input_->isLetter(ch);
    }

    bool isLetterOrDigit(char ch) {
        return input_->isLetterOrDigit(ch);
    }

    bool isEofInput() {
        return input_->isEof();
    }

    bool isNewLine(char ch) {
        return input_->isNewLine(ch);
    }

    Token scanNumberIntegralPart(char acc);
    Token scanNumberFractionalPart(char acc);
    Token scanNumberFractionalPart(std::string &acc);
    Token scanIdentifierOrFunction(char acc);


    Token extractToken();

public:

    /**
     * @brief Scans the next token and returns it.
     * @return The scanned token.
     */
    Token scanNext() {
        return extractToken();
    }

    Scanner(std::unique_ptr<input::Input> input):
        input_(std::move(input)) { }
};


/*
class ComputationParamsScanner {
public:
    enum class TokenType {
        rbracket,
        lbracket,
        str
    };

    class Token {
    private:
        TokenType type;
        std::string value;
    public:
        Token(TokenType t):
            type(t) { }

        Token(TokenType t, const std::string &v):
            type(t), value(v) { }

        TokenType getType() {
            return type;
        }

        std::string getValue() {
            return value;
        }

        bool operator ==(const Token &other) {
            return type == other.type && value == other.value;
        }

        bool operator !=(const Token &other) {
            return !(*this == other);
        }
    };



};*/

}

}

#endif // SCANNER_H
