#include <iostream>
#include "input.h"
namespace vytlalib {
namespace input {

bool Input::isEof() {
    return in_.eof();
}

char Input::getChar() {
    return in_.get();
}

char Input::peekChar() {
    return in_.peek();
}

bool Input::isDigit(char ch) {
    return '0' <= ch && ch <= '9';
}

bool Input::isLetter(char ch) {
    return ('a' <= ch && ch <= 'z') || ('A' <= ch && ch <= 'Z');
}

bool Input::isLetterOrDigit(char ch) {
    return isLetter(ch) || isDigit(ch);
}

bool Input::isWhiteSpace(char ch) {
    return ch == ' ' || ch == '\n' || ch == '\t';
}

bool Input::isNewLine(char ch) {
    return ch == '\n';
}

}
}
