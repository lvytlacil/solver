#include "common.h"
#include "../parser/input.h"
#include <iostream>
#include <algorithm>
namespace vytlalib {

using vytlalib::input::Input;

namespace ui {
namespace common {

static void skipWhiteSpaces(Input &input) {
    while (!input.isEof() && input.isWhiteSpace()) {
        input.getChar();
    }
}

std::vector<std::string> scanComputationParams(std::istream &in) {
    Input input(in);
    //skipWhiteSpaces(input);

    while (input.peekChar() != '[' && !input.isEof()) {
        input.getChar();
    }

    std::vector<std::string> result;

    if (input.peekChar() == '[') {
        input.getChar();

        std::string item;
        while (!input.isEof() && input.peekChar() != ']') {
            skipWhiteSpaces(input);
            item = "";
            while (!input.isEof() && !input.isWhiteSpace() && input.peekChar() != ']') {
                item += input.getChar();
            }
            if (!item.empty()) {
                result.push_back(item);
            }
        }
    }
    if (input.peekChar() == ']')
        input.getChar();
    return result;
}

const std::string ParamParser::emptyStr_ = std::string("");

const std::string& ParamParser::getValue(const std::string &opt) const {
    auto it = std::find(this->tokens_.begin(), this->tokens_.end(), opt);
    if (it != this->tokens_.end() && ++it != this->tokens_.end()) {
        return *it;
    }
    return emptyStr_;
}

bool ParamParser::paramExists(const std::string &opt) const {
    return std::find(this->tokens_.begin(), this->tokens_.end(), opt)
            != this->tokens_.end();
}

}
}
}
