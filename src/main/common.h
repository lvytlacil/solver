#ifndef COMMON_H
#define COMMON_H
#include <iosfwd>
#include <vector>
#include <stack>
#include <string>
#include <memory>
#include <sstream>
#include "../message/messageprocessor.h"
#include "../component.h"

namespace vytlalib {
using boxes::Component;
using message::ProgramException;
namespace ui {
namespace common {

enum class FuncType {ast, linear, lincached};

std::vector<std::string> scanComputationParams(std::istream &in);

/**
 * @brief Helper class to parse the input parameters of computations.
 */
class ParamParser {
private:
    // Scanned tokens
    std::vector<std::string> tokens_;
    // Empty string
    static const std::string emptyStr_;

public:
    /**
     * @brief Creates the ParamParser for the given vector of scanned tokens.
     * @param The vector of scanned tokens.
     */
    ParamParser(std::vector<std::string> tokens):
        tokens_(tokens) { }

    /**
     * @brief Creates the ParamParser for the vector of scanned tokens
     * constructed from the given input stream using scanComputationParams
     * function.
     * @param in The input stream to parse the parameters from.
     */
    ParamParser(std::istream &in):
        tokens_(scanComputationParams(in)) { }

    /**
     * @brief Returns the value for the given parameter.
     * @param opt The given parameter
     * @return The option as string or an empty string, if the argument is not present.
     */
    const std::string& getValue(const std::string &opt) const;

    /**
     * @brief Checks if the given parameter exists.
     * @param opt The given parameter.
     * @return True if the parameter exists, false otherwise.
     */
    bool paramExists(const std::string &opt) const;

};

using std::endl;
class JSONStringBuilder {
private:
    std::stringstream ss_;

    enum class Token {obj, arr};

    // Monitorst te depth
    std::stack<std::pair<Token, int> > st_;

    void pop(Token level) {
        if (st_.empty()) {
            throw ProgramException("Trying to end non-existing array or object.");
        }
        if (level != st_.top().first) {
            throw ProgramException("Mismatching array or object closing parenthesis");
        }
        st_.pop();
    }

    void comma() {
        if (st_.empty())
            return;

        if (st_.top().second > 0) {
            ss_ << ", " << endl;
        }
        ++st_.top().second;
    }

public:

    void addValue(const std::string& name, const std::string& value) {
        comma();
        ss_ << "\"" << name << "\" : " << "\"" <<  value << "\"";
    }

    void addValue(const std::string& name, int value) {
        comma();
        ss_ << "\"" << name << "\" : " <<  value;
    }

    void addValue(const std::string& name, double value) {
        comma();
        ss_ << "\"" << name << "\" : " <<  value;
    }

    void addValue(const std::string& name, Component value) {
        comma();
        ss_ << "\"" << name << "\" : " << "\"" <<  value << "\"";
    }

    /* For arrays */

    void addValue(const string& value) {
        comma();
        ss_ << "\"" <<  value << "\"";
    }

    void addValue(int value) {
        comma();
        ss_ << value;
    }

    void addValue(double value) {
        comma();
        ss_ << value;
    }

    void addValue(Component value) {
        comma();
        ss_ << "\"" <<  value << "\"";
    }

    /* Arrays and Objects */

    void startArray(const std::string& name) {
        comma();
        ss_ << "\"" << name << "\" : " << "[" << endl;
        st_.emplace(Token::arr, 0);
    }

    void startArray() {
        comma();
        st_.emplace(Token::arr, 0);
        ss_ << "[" << endl;
    }

    void endArray() {
        pop(Token::arr);
        ss_ << "]";
    }

    void startObject(const std::string& name) {
        comma();
        st_.emplace(Token::obj, 0);
        ss_ << "\"" << name << "\" : " << "{" << endl;
    }

    void endObject() {
        pop(Token::obj);
        ss_ << "}";
    }

    void startObject() {
        comma();
        st_.emplace(Token::obj, 0);
        ss_ << "{" << endl;
    }

    void beginRoot() {
        st_.emplace(Token::obj, 0);
        ss_ << "{" << endl;
    }

    void endRoot() {
        pop(Token::obj);
        if (!st_.empty()) {
            throw ProgramException("Parentheses do not match.");
        }
        ss_ << "}" << endl;
    }

    std::string toString() {
        return ss_.str();
    }

};

}
}
}
#endif // COMMON_H
