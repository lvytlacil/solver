#include <iostream>
#include <string>
#include <cstring>
#include "../degree/idegreecomputation.h"
#include "../degree/paralleldegreecomputation.h"
#include "../bisector/bisector.h"
#include "../box/box.h"
#include "../function/function.h"
#include "../parser/input.h"
#include "../message/messageprocessor.h"
#include "common.h"

using namespace vytlalib::boxes;
using namespace vytlalib::bisector;
using namespace vytlalib::degree;
using namespace vytlalib::function;
using namespace vytlalib::message;
using namespace vytlalib::ui::common;
using namespace std;

static const string IntroductionMessage = "Topological degree calculator";

static void parseOptions(std::istream &in, UPtrIBisector &bisector,
                    UPtrIDegreeComputation &dc, FuncType &funcType,
                         bool &scOnly) {
    cout << "Options: ";
    ParamParser paramParser(in);

    bisector.reset(new Bisector(0.0)); // Default value
    if (paramParser.paramExists("-d")) {
        const std::string &bisThresholdStr = paramParser.getValue("-d");
        if (bisThresholdStr.empty())
            throw "Missing value for -d option.";
        double bisThreshold = 0.0;
        try {
            bisThreshold = std::stod(bisThresholdStr);
        } catch (...) {
            throw ("Double value expected for -d option");
        }
        bisector.reset(new Bisector(bisThreshold));
    }

    dc.reset(new Degree()); // Default value
    if (paramParser.paramExists("-t")) {
        const std::string &workerThreadsStr = paramParser.getValue("-t");
        if (workerThreadsStr.empty()) {
            throw "Missing value for -t option.";
        }
        int workerThreads = 1;
        try {
            workerThreads = std::stol(workerThreadsStr);
            if (workerThreads <= 0) {
                throw "";
            }
        } catch (...) {
            throw ("Positive integer value expected for -t option");
        }
        dc.reset(new ParallelDegree(workerThreads));
    }

    funcType = FuncType::ast; // Default value
    if (paramParser.paramExists("-f")) {
        const std::string &bisThresholdStr = paramParser.getValue("-f");
        if (bisThresholdStr.empty()) {
            throw "Missing value for -f option.";
        }
        else if (bisThresholdStr == "ast") {
            funcType = FuncType::ast;
        } else if (bisThresholdStr == "linear") {
            funcType = FuncType::linear;
        } else if (bisThresholdStr == "lincached") {
            funcType = FuncType::lincached;
        }
    }

    scOnly = false; // Default value
    if (paramParser.paramExists("-sc")) {
        scOnly = true;
    }
}

static const std::string helpStr = "CLI for Topological Degree computation\n"
        "Enter input as instructed.\n"
        "1) Params. Enter computation parameters enclosed in [], or just"
        "type [] for default values.\n"
        "-f (ast|linear|lincached): which interpretation strategy to use\n"
        "-d #double: domain box face bisection threshold (boundary refinement)\n"
        "-t #int: number of worker threads to use in combinatorial phase\n\n"
        "-sc : compute sign covering only"
        "2) Identifier list. Enter identifiers (variables) enclosed in []\n"
        "3) Function body. Enter the function body, as a list of arithmetical expresions "
        "separated by ',' or ';', enclosed in [] as a whole.\n"
        "4) Domain boxes: Intervals separated by ',' or ';', one for each identifier, enclosed "
        "in [] as a whole.\n\n";

int main(int argc, char* argv[]) {
    cout << IntroductionMessage << endl;

    for (int i = 0; i < argc; ++i) {
        if (strcmp("-h", argv[i]) == 0) {
            cout << helpStr << endl;
            return 0;
        }
    }

    cout << "Welcome to Topological Degree Computation. Run it again with -h to display help.\n";

    try {

        FuncType funcType = FuncType::ast;
        UPtrIDegreeComputation dc;
        UPtrIBisector bisector;
        bool scOnly = false;

        // Options
        parseOptions(cin, bisector, dc, funcType, scOnly);

        // Function
        cout << "Function definition: ";
        UPtrIFunction func;
        switch (funcType) {
        case FuncType::ast:
            func = IFunction::parseRecursive(cin);
            break;
        case FuncType::linear:
            func = IFunction::parseLinear(cin);
            break;
        default:
            func = IFunction::parseLinearCached(cin);
            break;
        }

        // Input box
        cout << "Input box: ";
        Box inputBox = Function::evalBoxExpression(cin);

        //cout << ((Bisector*) bisector.get())->getThreshold() << endl;
        // The computation itself

        // Mute cout, to prevent possile gaol verbosity
        streambuf* origBuf = cout.rdbuf();
        cout.rdbuf(nullptr);

        Component::init();

        BoxList boundary = inputBox.computeInducedBoundary();
        auto signCovering = func->computeSignCovering(boundary, vytlalib::boxes::EMPTY_BOX,
                                                      *bisector);
        Component::cleanup();
        // Restore cout
        cout.rdbuf(origBuf);


        cout << "<< Done computing sign covering >>" << endl << flush;
        if (!scOnly) {
            if (vytlalib::boxes::isSignCoveringListDetermined(signCovering)) {
                cout << "Topological degree = " << dc->deg(signCovering, LeastFrequentSelectionStrategy()) << endl;
                cout << "-------------------\nAdditional info:\n";
                cout << "Pairs in sign covering = " << signCovering.size() << endl;
                cout << "Total no. of selected boxes = " << vytlalib::degree::selectedBoxesCount << endl;
                cout << "Total no. of non-selected boxes = " << vytlalib::degree::nonSelectedBoxesCount
                     << endl;
            } else {
                cout << "Timeout" << endl;
            }
        } else {
            if (vytlalib::boxes::isSignCoveringListDetermined(signCovering)) {
                cout << "Sufficient sign covering wrt. given function FOUND.\n";
            } else {
                cout << "Sufficient sign covering wrt. given function NOT FOUND\n";
            }
            cout << "Pairs in sign covering = " << signCovering.size() << endl;
        }

    } catch (const char* msg) {
        cout << msg << endl;
        std::exit(1);
    }
    return 0;
}
