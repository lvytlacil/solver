#include <iostream>
#include <cstdlib>
#include "common.h"
#include "../bisector/bisector.h"
#include "../function/function.h"
#include "../function/functionevaluator.h"
#include "../degree/idegreecomputation.h"
#include "../degree/paralleldegreecomputation.h"
#include "../solver/algorithm.h"
#include "../parser/parser.h"
#include "../parser/ast.h"
#include "../message/messageprocessor.h"
#include <cstring>
#include <list>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <string>
#include <map>
#include "../vibes.h"

using namespace vytlalib::boxes;
using namespace vytlalib::bisector;
using namespace vytlalib::degree;
using namespace vytlalib::function;
using namespace vytlalib::message;
using namespace vytlalib::solver;
using namespace vytlalib::ui::common;
using namespace std;

typedef std::unique_ptr<Solver> UPtrSolver;
enum class OutputFormat {json, statsonly, vibes};

static void parseOptions(std::istream &in, FuncType &funcType, UPtrIBisector &paramBisector,
                         double &faceBisThreshold, UPtrSolver &solver,
                         int &capacity, OutputFormat &format);

static void buildStats(JSONStringBuilder &js, BoxList &yesList,
                                 BoxList &noList, Box &parDomain);
static void buildJSONDetailedInfo(JSONStringBuilder &js, const Box& inputBox,
                                  const BoxList &yesList, const BoxList &noList);

void drawBox(const Box& box, const char* params) {
    if (box.getSize() == 1) {
        vibes::drawBox(box.getComp(0).lower(), box.getComp(0).upper(), 0, 1, params);
    } else if (box.getSize() == 2) {
        vibes::drawBox(box.getComp(0).lower(), box.getComp(0).upper(),
                       box.getComp(1).lower(), box.getComp(1).upper(), params);
    }
}

void plot(const Box& parDomain, const BoxList &yesList, const BoxList &noList) {
    int size = parDomain.getSize();
    if (size != 2 && size != 1) {
        return;
    }

    vibes::beginDrawing();
    vibes::newFigure("Sample");

    drawBox(parDomain, "w[w]");

    for (const auto& box : yesList) {
        vytlassert(box.getSize == size, "Every sub-box must have the same size as the domain");
        drawBox(box, "b[g]");
    }

    for (const auto& box : noList) {
        vytlassert(box.getSize == size, "Every sub-box must have the same size as the domain");
        drawBox(box, "b[r]");
    }
    vibes::axisAuto();
    vibes::endDrawing();
}

static const std::string helpStr = "CLI for Solver\n"
        "Enter input as instructed.\n"
        "1) Params. Enter computation parameters enclosed in [], or just"
        "type [] for default values.\n"
        "-s (static|bisectonly|bisectandkeep|tree|grid): which frame type to use\n"
        "-f (ast|linear|lincached): which interpretation strategy to use\n"
        "-d #double: param. box bisection width threshold\n"
        "-r #double: param. box bisection ratio\n"
        "-c #int: frame items collection capacity\n"
        "-E #double: domain box face bisection threshold modifier\n"
        "-o (statsonly|json|vibes): output mode\n\n"
        "2) Identifier list. Enter identifiers (variables and parameters) enclosed in []\n"
        "3) Function body. Enter the function body, as a list of arithmetical expresions "
        "separated by ',' or ';', enclosed in [] as a whole.\n"
        "4) No. of parameters: The number of identifiers from the end of the list, to be "
        "treated as parameters.\n"
        "5) Domain boxes: Intervals separated by ',' or ';', one for each identifier, enclosed "
        "in [] as a whole.\n\n";

int main(int argc, char* argv[]) {

    for (int i = 0; i < argc; ++i) {
        if (strcmp("-h", argv[i]) == 0) {
            cout << helpStr << endl;
            return 0;
        }
    }

    cout << "Welcome to Solver. Run it again with -h to display help.\n";

    try {
        FuncType funcType = FuncType::ast; // interpretation
        UPtrIBisector paramBisector; // for bisecting parameter boxes
        double faceBisThreshold = 1.0; // for bisecting faces of sub-boxes of the domain
        UPtrSolver solver; // frame type
        int capacity = 64; // capacity of the frames items collection
        OutputFormat format = OutputFormat::statsonly;

        // Options
        parseOptions(cin, funcType, paramBisector, faceBisThreshold, solver, capacity,
                     format);

        // Function
        cout << "Function definition: ";
        UPtrIFunction func;
        switch (funcType) {
        case FuncType::ast:
            func = IFunction::parseRecursive(cin);
            break;
        case FuncType::linear:
            func = IFunction::parseLinear(cin);
            break;
        default:
            func = IFunction::parseLinearCached(cin);
            break;
        }

        // Number of parameters
        cout << "Number of parameters: ";
        int paramCount;
        cin >> paramCount;
        if (paramCount <= 0 || paramCount >= func->getDomainDim()) {
            throw ProgramException("The parameter count must be entered, so that"
                                   " there is at least one parameter and one variable.");
        }

        int varCount = func->getDomainDim() - paramCount;
        if (varCount != func->getImageDim()) {
            throw ProgramException("Number of variables must equal the number of the equations");
        }


        vytlassert(varCount > 0, "Number of variables must be positive.");
        vytlassert(varCount < func->getDomainDim(), "Number of variables must be strictly less"
                   " than the image dimension of the function.");
        vytlassert(varCount + paramCount == func->getDomainDim(), "Sum of the number of"
                   " variables and parameters must equal the image dim. of the func.");

        // Input box
        cout << "Input box: ";
        Box inputBox = Function::evalBoxExpression(cin);

        if (inputBox.getTopologicalDimension() != func->getDomainDim() ||
            inputBox.getSize() != func->getDomainDim()) {
            throw ProgramException("The dimension and size of the input box must"
                                   " equal the number of equations");
        }

        // Create domain and param boxes
        std::vector<Component> domainComp;
        std::vector<Component> paramComp;
        for (int i = 0; i < varCount; ++i) {
            domainComp.push_back(inputBox.getComp(i));
        }
        for (int i = varCount; i < func->getDomainDim(); ++i) {
            paramComp.push_back(inputBox.getComp(i));
        }
        Box varDomain(domainComp);
        Box parDomain(paramComp);

        // Check if graphic output selected
        if (format == OutputFormat::vibes && parDomain.getSize() > 2) {
            ProgramException("Graphical output via vibes only available for 1 or 2 parameters.");
        }

        // Actually compute

        // Mute cout, to prevent possile gaol verbosity
        streambuf* origBuf = cout.rdbuf();
        cout.rdbuf(nullptr);

        Component::init();
        Solver &s = *solver;
        BoxList yesList, noList;
        s.solve(varDomain, parDomain, *paramBisector, faceBisThreshold, capacity, *func,
                Degree(), yesList, noList);
        Component::cleanup();
        // Print statistics

        // Restore cout
        cout.rdbuf(origBuf);

        // Output detailed result
        switch (format) {
        case OutputFormat::vibes:
            plot(parDomain, yesList, noList);
            break;
        case OutputFormat::json: {
            JSONStringBuilder detailed;            
            detailed.beginRoot();
            buildStats(detailed, yesList, noList, parDomain);
            buildJSONDetailedInfo(detailed, parDomain, yesList, noList);
            detailed.endRoot();
            cout << detailed.toString();
            break;
        }
        case OutputFormat::statsonly: {
            JSONStringBuilder js;
            js.beginRoot();
            buildStats(js, yesList, noList, parDomain);
            js.endRoot();
            cout << js.toString();
        }
        default:
            break;
        }
    } catch (ProgramException e) {
        cout << e.what() << endl;
        exit(1);
    } catch (const char* msg) {
        cout << msg << endl;
        exit(1);
    }
}

static void buildStats(JSONStringBuilder &js, BoxList &yesList,
                                 BoxList &noList, Box &parDomain) {
    js.startObject("statistic");
    js.addValue("yesboxescount", static_cast<int>(yesList.size()));
    js.addValue("noboxescount", static_cast<int>(noList.size()));

    Component c("0.0");
    for (const Box& box : yesList) {
        c += box.getVolume();
    }
    c /= parDomain.getVolume();
    js.addValue("yesarea", c);

    c = Component("0.0");
    for (const Box& box : noList) {
        c += box.getVolume();
    }
    c /= parDomain.getVolume();
    js.addValue("noarea", c);
    js.endObject();
}

static void buildJSONDetailedInfo(JSONStringBuilder &js, const Box& inputBox,
                                  const BoxList &yesList, const BoxList &noList) {
    js.startObject("inputbox");
    js.startArray("components");
    for (int i = 0; i < inputBox.getSize(); ++i) {
        const Component &c = inputBox.getComp(i);
        js.startArray();
        js.addValue(c.lower());
        js.addValue(c.upper());
        js.endArray();
    }
    js.endArray();
    js.endObject();

    js.startArray("yesboxes");
    for (const Box& box : yesList) {        
        js.startObject();
        js.startArray("components");
        for (int i = 0; i < box.getSize(); ++i) {
            const Component &c = box.getComp(i);
            js.startArray();
            js.addValue(c.lower());
            js.addValue(c.upper());
            js.endArray();
        }
        js.endArray();
        js.endObject();        
    }
    js.endArray();

    js.startArray("noboxes");
    for (const Box& box : noList) {        
        js.startObject();
        js.startArray("components");
        for (int i = 0; i < box.getSize(); ++i) {
            const Component &c = box.getComp(i);
            js.startArray();
            js.addValue(c.lower());
            js.addValue(c.upper());
            js.endArray();
        }
        js.endArray();
        js.endObject();
    }
    js.endArray();
}

static void parseOptions(std::istream &in, FuncType &funcType, UPtrIBisector &paramBisector,
                         double &faceBisThreshold, UPtrSolver &solver,
                         int &capacity, OutputFormat &format) {
    cout << "Options: ";
    ParamParser paramParser(in);

    // Function type
    funcType = FuncType::ast; // Default value
    if (paramParser.paramExists("-f")) {
        const std::string &bisThresholdStr = paramParser.getValue("-f");
        if (bisThresholdStr.empty()) {
            throw "Missing value for -f option.";
        }
        else if (bisThresholdStr == "ast") {
            funcType = FuncType::ast;
        } else if (bisThresholdStr == "linear") {
            funcType = FuncType::linear;
        } else if (bisThresholdStr == "lincached") {
            funcType = FuncType::lincached;
        }
    }

    double bisThreshold = 0.1; // def. value
    double bisRatio = 0.5; // def. value

    // Parameter bisector threshold
    if (paramParser.paramExists("-d")) {
        const std::string &bisThresholdStr = paramParser.getValue("-d");
        if (bisThresholdStr.empty())
            throw "Missing value for -d option.";
        try {
            bisThreshold = std::stod(bisThresholdStr);
        } catch (...) {
            throw ("Double value expected for -d option");
        }
    }

    if (paramParser.paramExists("-r")) {
        const std::string &bisThresholdStr = paramParser.getValue("-r");
        if (bisThresholdStr.empty())
            throw "Missing value for -r option.";
        try {
            bisRatio = std::stod(bisThresholdStr);
            if (bisRatio <= 0.0 || bisRatio >= 1.0)
                throw "";
        } catch (...) {
            throw ("Double value strictly between 0.0 and 1.0 expected for -r option");
        }
    }

    paramBisector.reset(new Bisector(bisThreshold, bisRatio));

    // Edge bisection threshold
    faceBisThreshold = 1.0; // default value

    if (paramParser.paramExists("-E")) {
        const std::string &edgeBisThresholdStr = paramParser.getValue("-E");
        if (edgeBisThresholdStr.empty())
            throw "Missing value for -E option.";
        try {
            faceBisThreshold = std::stod(edgeBisThresholdStr);
            if (faceBisThreshold <= 0)
                throw "";
        } catch (...) {
            throw ("Positive double expected for -E option");
        }
    }

    // Solver
    solver.reset(new GridFrameSolver); // Default value
    if (paramParser.paramExists("-s")) {
        const std::string &solverStr = paramParser.getValue("-s");
        if (solverStr.empty()) {
            throw "Missing value for -s option.";
        } else if (solverStr == "static") {
            solver.reset(new StaticFrameSolver());
        } else if (solverStr == "splitonly" || solverStr == "bisectonly") {
            solver.reset(new SplitOnlyFrameSolver());
        } else if (solverStr == "splitandkeep" || solverStr == "bisectandkeep") {
            solver.reset(new SplitAndKeepFrameSolver());
        } else if (solverStr == "tree") {
            solver.reset(new TreeFrameSolver());
        }
    }

    // Frame capacity
    capacity = 64; // def. value
    if (paramParser.paramExists("-c")) {
        const std::string &capacityStr = paramParser.getValue("-c");
        if (capacityStr.empty())
            throw "Missing value for -c option.";
        try {
            capacity = std::stol(capacityStr);
            if (capacity <= 0)
                throw "";
        } catch (...) {
            throw ("Capacity must be positive");
        }
    }

    // Output mode
    format = OutputFormat::statsonly; // Default value
    if (paramParser.paramExists("-o")) {
        const std::string &formatStr = paramParser.getValue("-o");
        if (formatStr.empty()) {
            throw "Missing value for -o option.";
        } else if (formatStr == "json") {
            format = OutputFormat::json;
        } else if (formatStr == "vibes") {
            format = OutputFormat::vibes;
        }
    }

    bool verbose = paramParser.paramExists("-v");
#ifndef NDEBUG
    verbose = true;
#endif
    if (verbose) {
        cout << "---- Input params -------------\n";
        cout << "Function type: " << ( (int) funcType) << endl;
        cout << "Parameter bisector threshold: " << bisThreshold << endl;
        cout << "Parameter bisector ratio: " << bisRatio << endl;
        cout << "Domain sub-boxes face bisector threshold: " << faceBisThreshold << endl;
        cout << "Solver: " << typeid(*solver).name() << endl;
        cout << "Capacity: " << capacity << endl;
        cout << "Output format: " << (int) format << endl;
        cout << "--------------------------------\n";
    }
}


