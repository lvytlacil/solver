#include <stack>
#include <mutex>
#include <thread>
#include <deque>
#include <condition_variable>
#include "../box/box.h"
#include "../bisector/bisector.h"
#include "../function/function.h"
#include "../degree/idegreecomputation.h"
#include "algorithm.h"
#include <iostream>
using namespace std;
namespace vytlalib {
using boxes::Box;
using boxes::OrientedBox;
using boxes::BoxList;
using degree::IDegree;
using function::IFunction;
using function::ByteCodeFunction;
using function::UPtrIFunction;
using bisector::IBisector;
using bisector::Bisector;


namespace solver {

/*------ Solver -------*/

static double computeThreshold(double varsTopWidth, double paramsTopWidth, double paramsActWidth,
                               double signCovFaceThreshold) {
    return (paramsActWidth / paramsTopWidth) * (varsTopWidth / signCovFaceThreshold);
}

void Solver::performPaving(const boxes::Box &vars, const boxes::Box &params,
                           UPtrWorklist topWorklist,
                           const IBisector &bisector, double signCovFaceThreshold,
                           const IDegree &dc,
                           BoxList &yesList, BoxList &noList) const {

    double varDomainWidth = vars.getDiam();
    double paramDomainWidth = params.getDiam();
    Bisector signCoveringBis(computeThreshold(varDomainWidth, paramDomainWidth,
                            topWorklist->getParamBox().getDiam(), signCovFaceThreshold));

    std::stack<StackFrame> st;
    st.push(StackFrame(std::move(topWorklist), Solution::unknown));

    while (!st.empty()) {
        StackFrame &frame = st.top();
        if (frame.result == Solution::no) {
            noList.push_back(frame.wlist->getParamBox());
            st.pop();
        } else if (frame.result == Solution::yes) {
            yesList.push_back(frame.wlist->getParamBox());
            st.pop();
        } else {
            // Create children, parent frame is reused and becomes left child
            UPtrWorklist rightBisectedList = frame.wlist->bisect(bisector);
            if (rightBisectedList != nullptr) {
                st.push(StackFrame(std::move(rightBisectedList), frame.result));
                // Eval right child, save result into its frame
                signCoveringBis.setThredhold(computeThreshold(varDomainWidth, paramDomainWidth,
                               st.top().wlist->getParamBox().getDiam(), signCovFaceThreshold));
                st.top().result = st.top().wlist->hasSolution(signCoveringBis, dc);
                // Eval left child, save result into its frame
                signCoveringBis.setThredhold(computeThreshold(varDomainWidth, paramDomainWidth,
                                frame.wlist->getParamBox().getDiam(), signCovFaceThreshold));
                frame.result = frame.wlist->hasSolution(signCoveringBis, dc);
            } else {
                st.pop();
            }
        }
    }
}

void StaticFrameSolver::solve(const boxes::Box &vars, const boxes::Box &params,
                            const bisector::IBisector &bisector, double widthThreshold, int capacity,
                            function::IFunction &function, const degree::IDegree &dc,
                            boxes::BoxList &yesList, boxes::BoxList &noList) const {

    UPtrWorklist top(new StaticFrame(params, function, vars));
    performPaving(vars, params, std::move(top), bisector, widthThreshold, dc, yesList, noList);
}

void SplitOnlyFrameSolver::solve(const boxes::Box &vars, const boxes::Box &params,
                            const bisector::IBisector &bisector, double widthThreshold, int capacity,
                            function::IFunction &function, const degree::IDegree &dc,
                            boxes::BoxList &yesList, boxes::BoxList &noList) const {
    UPtrWorklist top(new BisectOnlyFrame(params, function, vars, capacity));
    performPaving(vars, params, std::move(top), bisector, widthThreshold, dc, yesList, noList);
}

void SplitAndKeepFrameSolver::solve(const boxes::Box &vars, const boxes::Box &params,
                            const bisector::IBisector &bisector, double widthThreshold, int capacity,
                            function::IFunction &function, const degree::IDegree &dc,
                            boxes::BoxList &yesList, boxes::BoxList &noList) const {
    UPtrWorklist top(new BisectAndKeepFRame(params, function, vars, capacity));
    performPaving(vars, params, std::move(top), bisector, widthThreshold, dc, yesList, noList);
}

void TreeFrameSolver::solve(const boxes::Box &vars, const boxes::Box &params,
                            const bisector::IBisector &bisector, double widthThreshold, int capacity,
                            function::IFunction &function, const degree::IDegree &dc,
                            boxes::BoxList &yesList, boxes::BoxList &noList) const {
    UPtrWorklist top(new TreeFrame(params, function, vars, capacity));
    performPaving(vars, params, std::move(top), bisector, widthThreshold, dc, yesList, noList);
}

void GridFrameSolver::solve(const boxes::Box &vars, const boxes::Box &params,
                            const bisector::IBisector &bisector, double widthThreshold, int capacity,
                            function::IFunction &function, const degree::IDegree &dc,
                            boxes::BoxList &yesList, boxes::BoxList &noList) const {
    UPtrWorklist top(new GridFrame(params, function, vars, capacity));
    performPaving(vars, params, std::move(top), bisector, widthThreshold, dc, yesList, noList);
}



}
}

