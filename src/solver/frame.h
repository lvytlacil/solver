#ifndef FRAME_H
#define FRAME_H
#include <set>
#include <map>
#include "../box/box.h"
#include "framefwd.h"

namespace vytlalib {

namespace function {
    class IFunction;
}

namespace bisector {
    class IBisector;
}

namespace degree {
    class IDegree;
}

namespace solver {

/**
 * @brief Encapsulation of one particular sub-box of param box from the paving algorithm,
 * that maintains a structure of sub-boxes of the input domain box.
 */
class Frame {
private:
    // Bisects the underlying paramBox into two childs. This instance retains one of them,
    // and a copy of this instance is created and returned, that will have the second child as
    // its param box.
    virtual Frame *performBisection(const bisector::IBisector &bisector) = 0;

protected:
    /**
     * @brief The parameter sub-box associated with this worklist.
     */
    boxes::Box paramBox;
    /**
     * @brief Function associated with this worklist.
     */
    function::IFunction &func;

    /**
     * @brief Creates a new worklist associated with the given parameter sub-box and function.
     * @param paramBox Parameters sub-box
     * @param func Function
     */
    Frame(boxes::Box paramBox, function::IFunction &func):
        paramBox(paramBox), func(func) { }



public:
    /**
     * @brief Destroys the frame instance.
     */
    virtual ~Frame() { }

    /**
     * @brief Performs steps to decide, whether the underlying parameter box P' belongs to either the N list
     * or the Y list.
     *
     * First, the frame is pruned, removing all boxes from items collection guaranteed no to contain
     * solution. Then, topological degree is attempted to be computed. Finally, if degree is zero
     * or could not be computed, the items collection is refined.
     * @param bisector Bisector to pass to sign covering computation. It dictates, how subtle
     * the underlying boundary refining should be.
     * @param dc Implementation of algorithm for computing topological degree.
     * @return Value of Solution type. If Solution::yes is returned, then P belongs to Y.
     * If Solution::no is returned, then P belongs to N. Otherwise we can not tell.
     */
    virtual Solution hasSolution(const bisector::IBisector &bisector, const degree::IDegree &dc) = 0;

    /**
     * @brief Bisect the underlying parameter box P' into P1 and P2. This instance
     * will be modified, to now hold P1, and then clone to an other one holding P2.
     * @param bisector Bisector for bisecting P into P1, P2.
     * @return  Frame with P2 as underlying param box.
     */
    UPtrWorklist bisect(const bisector::IBisector &bisector) {
        return UPtrWorklist(performBisection(bisector));
    }

    /**
     * @brief Returns a constant reference to the underlying parameter box.
     * @return Constant reference to the underlying parameter box.
     */
    const boxes::Box& getParamBox() const {
        return paramBox;
    }

    /**
     * @brief Returns a reference to the underlying function.
     * @return Reference to the underlying function.
     */
    function::IFunction& getFunction() {
        return func;
    }

};

/**
 * @brief Implementation of the static frame design. Static frame design always
 * only hold one box in its items collection, passing it to any of its subsequent frames.
 */
class StaticFrame : public Frame {
private:

    // The one and only box this frame holds
    boxes::Box domainBox_;

    // Override of Frame's bisection
    StaticFrame* performBisection(const bisector::IBisector &bisector) override;
    
public:

    /**
     * @brief Creates a new StaticFrame instance.
     * @param paramBox The param box to associate this instance with.
     * @param func The function to associate this instance width.
     * @param domainBox The only item, that should be in items collection.
     * @param domainBoundary Precomputed boun
     */
    StaticFrame(boxes::Box paramBox, function::IFunction &func,
                   boxes::Box domainBox):
        Frame(paramBox, func), domainBox_(domainBox) { }

    /** @copydoc Frame::hasSolution()
    */
    Solution hasSolution(const vytlalib::bisector::IBisector &bisector, const degree::IDegree &dc) override;
};

/**
 * @brief Implementation of split-only frame, that maintains a list of sub-boxes in its items
 * collection and can bisect them into smaller boxes, if capacity is sufficient.
 */
class BisectOnlyFrame : public Frame {
private:
    // The capacity of the list
    int capacity_;
    // List of stored sub-boxes
    std::list<boxes::Box> domainSubboxes_;

    // Override of Frame's bisection
    BisectOnlyFrame* performBisection(const bisector::IBisector &bisector) override;

    // Constructor used for duplicating
    BisectOnlyFrame(boxes::Box paramBox, const BisectOnlyFrame &other):
        Frame(paramBox, other.func), capacity_(other.capacity_),
        domainSubboxes_(other.domainSubboxes_) { }

public:

    /**
     * @brief Creates a new instance of SplitOnlyFrame.
     * @param paramBox The param box to associate this instance with.
     * @param func The function to associate this instance width.
     * @param domainBox The only item, that should be in items collection.
     * @param capacity Maximum number of boxes allowed to store.
     */
    BisectOnlyFrame(boxes::Box paramBox, function::IFunction &func,
                      const boxes::Box &domainBox, int capacity):
        Frame(paramBox, func), capacity_(capacity) {
        domainSubboxes_.push_back(domainBox);
    }

    /** @copydoc Frame::hasSolution()
     *
     * Size of the underlying items collection is at most double the size before the call.
     * If some original box is not removed from the collection by pruning,
     * then it is bisected into children
     * and replaced by them, if sufficient capacity, or left untouched otherwise.
     */
    Solution hasSolution(const bisector::IBisector &bisector, const degree::IDegree &dc) override;
};

/**
 * @brief Implementation of split-and-keep frame, that maintains a list of sub-boxes in its items
 * collection and can bisect them into smaller boxes, if capacity is sufficient, while keeping
 * their parents. Only leaves are ever bisected, and box X is removed, as soon as
 * either 0 notin F(X, P') is found out, or deg(F_P', X, 0) = 0.
 */
class BisectAndKeepFRame : public Frame {
private:
    int capacity_;
    // Pair of box and indication, whether it is a leaf
    std::list<std::pair<boxes::Box, bool> > domainSubboxes_;

    // Override of Frame's bisection
    BisectAndKeepFRame* performBisection(const bisector::IBisector &bisector) override;

    // Constructor for duplicating
    BisectAndKeepFRame(boxes::Box paramBox, const BisectAndKeepFRame &other):
        Frame(paramBox, other.func), capacity_(other.capacity_),
        domainSubboxes_(other.domainSubboxes_) { }

public:
    /**
     * @brief Construct a new SplitAndKeepFrame instance.
     * @param paramBox The param box to associate this instance with.
     * @param func The function to associate this instance width.
     * @param domainBox The only item, that should be in items collection.
     * @param capacity Maximum number of boxes allowed to store.
     */
    BisectAndKeepFRame(boxes::Box paramBox, function::IFunction &func,
                      const boxes::Box &domainBox, int capacity):
        Frame(paramBox, func), capacity_(capacity) {
        domainSubboxes_.push_back(std::make_pair(domainBox, true));
    }

    /** @copydoc Frame::hasSolution()
     *
     * Size of the underlying items collection is at most double the size before the call.
     * Only leaves are ever bisected, and box X is removed, as soon as
     * either 0 notin F(X, P') is found out, or deg(F_P', X, 0) = 0.
     */
    Solution hasSolution(const bisector::IBisector &bisector, const degree::IDegree &dc) override;
};

/**
 * @brief Implementation of tree based frame, that maintains sub-boxes in its items
 * collection in a tree structure and can bisect them into smaller boxes,
 * if capacity is sufficient, while keeping
 * their parents. Only leaves are ever bisected, and box X is removed, as soon as
 * either 0 notin F(X, P') is found out, or all of its children are removed (backward pruning)
 * or deg(F_P', X, 0) = 0. Also when a box is removed, its children are immediately
 * removed as well (forward pruning).
 */
class TreeFrame : public Frame {
private:
    // Forward decl. and typedefs
    struct Node;
    typedef std::unique_ptr<Node> UPtrNode;
    typedef std::list<UPtrNode> NodeList;

    // Structure representing a tree node. Stores sub-box of domain box.
    struct Node {
        boxes::Box domain; // The domain sub-box
        TreeFrame &tree; // Tree this node is associated with
        NodeList children; // List of children
        bool determined; // True, if deg(F, domain, 0) = 0 is guaranteed
        bool wasNotLeaf; // Flag to use during pruning

        // Ctor
        Node(boxes::Box domain, TreeFrame &tree, bool determined = false):
            domain(domain), tree(tree), children(), determined(determined) { }

        // Leaf property getter
        bool isLeaf() {
            return children.empty();
        }

        // Adds a child to this node,
        Node* addChild(boxes::Box domain, bool determined = false) {
            children.push_back(UPtrNode(new Node(domain, tree, determined)));
            ++tree.nodeCount_;
            return children.back().get();
        }

        // Removes child at the iterator pos
        NodeList::iterator removeChild(NodeList::iterator &it) {
            --tree.nodeCount_;
            return children.erase(it);
        }

        // Removes all children of this node
        inline void removeAllChildren() {
            auto it = children.begin();
            while (it != children.end()) {
                (*it)->removeAllChildren();
                it = removeChild(it);
            }
            assert(children.empty());
        }
    };


    // Max no. of nodes this tree can hold
    int capacity_;
    // Actual number of nodes currently in the tree
    int nodeCount_;
    // Root of the tree
    UPtrNode top_;

    /*---- Private methods ----*/
    // Checks if the tree is empty
    bool isEmpty();
    // Override of Frame's bisection
    TreeFrame* performBisection(const bisector::IBisector &bisector) override;

    // Manages the bisecting of leaves in items collection
    void bisectLeaves(Node* &leaf, Node* &parent);
    // Manages the pruning, removing nodes without zero
    void eliminateNodesWithoutZero();
    // Assertion test for node counting
    bool countTest() const;

    // Constructor used for duplicating
    TreeFrame(boxes::Box paramBox, const TreeFrame &other, function::IFunction &func);

public:
    /**
     * @brief Construct a new WorkTree instance.
     * @param paramBox The param box to associate this instance with.
     * @param func The function to associate this instance width.
     * @param domainBox The only item, that should be in items collection.
     * @param capacity Maximum number of boxes allowed to store.
     */
    TreeFrame(boxes::Box paramBox, function::IFunction &func,
                      const boxes::Box &domainBox, int capacity);

    /** @copydoc Frame::hasSolution()
     *
     * Only leaves are ever bisected, and box X is removed, as soon as
     * either 0 notin F(X, P') is found out, or all of its children are removed (backward pruning)
     * or deg(F_P', P', 0) = 0. Also when a box is removed, its children are immediately
     * removed as well (forward pruning).
     */
    Solution hasSolution(const bisector::IBisector &bisector, const degree::IDegree &dc) override;
};


/*------- WorkGraph ---------------*/

class GridFrame : public Frame {
private:
    class Node;
    typedef std::shared_ptr<boxes::Box> SPtrBox;
    //typedef std::list<std::pair<SPtrBox, Node*> > EdgeSet;
    typedef std::map<Node*, SPtrBox> EdgeMap;
    typedef std::list<SPtrBox> EdgeList;


    struct Node {
        SPtrBox domain;
        std::unique_ptr<EdgeMap[]> edges;
        std::unique_ptr<EdgeList[]> freeEdges;

        int getDoubleSize() const {
            return 2 * domain->getSize();
        }

        Node(boxes::Box domain):
            domain(new boxes::Box(std::move(domain))),
            edges(new EdgeMap[getDoubleSize()]),
            freeEdges(new EdgeList[getDoubleSize()]) {
            for (int i = 0; i < getDoubleSize(); ++i) {
                edges[i] = EdgeMap();
                freeEdges[i] = EdgeList();
            }
        }

        Node(const Node &other):
            Node(*other.domain) { }
    };

    // Assertion methods
    bool consistency() {
        for (auto& koko : nodes_) {
            Node* node = koko.get();
            for (int i = 0; i < node->getDoubleSize(); ++i) {
                EdgeMap& em = node->edges[i];
                for (auto& kv : em) {
                    Node *other = kv.first;

                    int otherI = i % 2 == 0 ? i + 1 : i - 1;
                    if (other->edges[otherI].count(node) != 1)
                        return false;
                }
            }
        }
        return true;
    }

    bool faceConsistency() {
        for (auto& nodeptr : nodes_) {
            Node* node = nodeptr.get();
            boxes::BoxList boundary = node->domain->computeInducedBoundary();

            for (int i = 0; i < node->getDoubleSize(); ++i) {
                EdgeMap em = node->edges[i];
                EdgeList el = node->freeEdges[i];
                for (auto &kv : em) {
                    boxes::Box* subface = kv.second.get();
                    bool ok = false;
                    for (boxes::Box &bndBox : boundary) {
                        if (subface->isSubboxOf(bndBox)) {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                        return false;
                }

                for (auto &subface : el) {
                    bool ok = false;
                    for (boxes::Box &bndBox : boundary) {
                        if (subface->isSubboxOf(bndBox)) {
                            ok = true;
                            break;
                        }
                    }
                    if (!ok)
                        return false;
                }
            }
        }
        return true;
    }

    bool faceDimension() {
        for (auto& nodeptr : nodes_) {
            Node* node = nodeptr.get();
            boxes::BoxList boundary = node->domain->computeInducedBoundary();

            for (int i = 0; i < node->getDoubleSize(); ++i) {
                EdgeMap em = node->edges[i];
                EdgeList el = node->freeEdges[i];
                for (auto &kv : em) {
                    boxes::Box* subface = kv.second.get();
                    if (subface->getTopologicalDimension() + 1 != node->domain->getTopologicalDimension())
                        return false;
                }

                for (auto &subface : el) {
                    if (subface->getTopologicalDimension() + 1 != node->domain->getTopologicalDimension())
                        return false;
                }
            }
        }
        return true;
    }

    bool isEmpty() {
        return nodes_.empty();
    }


    void eliminateNodesWithoutZero();
    void getCoveringOfConnectedComp(Node* node, const std::set<Node *> &visited,
                                    std::set<Node *> &currentComponent,
                                    boxes::SignCoveringList &sc,
                                    const vytlalib::bisector::IBisector &bisector);
    void refineNode(Node *leftNode, const bisector::IBisector &bisector);

    // Helper method to encapsulate the process of splitting faces
    void faceSplit(Node *left, Node *right, int index, int dim, double step);
    // Helper method when copying the structure
    Node* getOrCreateClone(Node* key, std::map<Node*, Node*> &map);


    GridFrame(boxes::Box paramBox, const GridFrame &other, function::IFunction &func);
    GridFrame* performBisection(const bisector::IBisector &bisector) override;

    std::list<std::unique_ptr<Node> > nodes_;

    int capacity_;

    // Used to store nodes, that were already part of some constructed connected component
    static std::set<Node*> visited;
    static std::set<Node*> currentComponent;


public:
    Solution hasSolution(const bisector::IBisector &bisector, const degree::IDegree &dc) override;


    GridFrame(boxes::Box paramBox, function::IFunction &func,
                      const boxes::Box &domainBox, int capacity);
};

}
}



#endif // FRAME_H
