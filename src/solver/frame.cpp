#include "../function/function.h"
#include "../bisector/bisector.h"
#include "../degree/idegreecomputation.h"
#include "../message/messageprocessor.h"
#include <stack>
#include "frame.h"
using namespace std;
namespace vytlalib {

using boxes::Box;
using boxes::SignCoveringList;
using bisector::IBisector;
using bisector::Bisector;
using degree::IDegree;
using function::IFunction;

namespace solver {

// Bisector with zero threshold
static const Bisector infBisector(0);

/*------------------- StaticWorklist -------------------*/
StaticFrame* StaticFrame::performBisection(const bisector::IBisector &bisector) {
    Box bisectedParamBox = paramBox.bisectBy(bisector);
    if (bisectedParamBox == boxes::EMPTY_BOX) {
        return nullptr;
    }
    return new StaticFrame(bisectedParamBox, func, domainBox_);
}

Solution StaticFrame::hasSolution(const IBisector &bisector, const IDegree &dc) {
    if (!func.eval(domainBox_, paramBox).containsZero()) {
        return Solution::no;
    }
    SignCoveringList sc = func.computeBoundarySignCovering(domainBox_,
                                                   paramBox, bisector);

    if (boxes::isSignCoveringListDetermined(sc) && dc.deg(sc) != 0) {
        return Solution::yes;
    }
    return Solution::unknown;
}

/*------ SplitOnlyWorkList --------*/

BisectOnlyFrame* BisectOnlyFrame::performBisection(const bisector::IBisector &bisector) {
    Box bisectedParamBox = paramBox.bisectBy(bisector);
    if (bisectedParamBox == boxes::EMPTY_BOX) {
        return nullptr;
    }
    return new BisectOnlyFrame(bisectedParamBox, *this);
}

Solution BisectOnlyFrame::hasSolution(const IBisector &bisector, const IDegree &dc) {
    auto it = domainSubboxes_.begin();
    while (it != domainSubboxes_.end()) {
        Box& domainBox = *it;
        if (!func.eval(domainBox, paramBox).containsZero()) {
            auto toBeDeleted = it++;
            domainSubboxes_.erase(toBeDeleted);
        } else {
            SignCoveringList sc = func.computeBoundarySignCovering(domainBox,
                                                           paramBox, bisector);
            //SignCoveringList sc = domainBox.computeBoundarySignCovering(paramBox, bisector, func);
            bool sufficient = boxes::isSignCoveringListDetermined(sc);
            if (sufficient && dc.deg(sc) != 0) {
                return Solution::yes;
            } else if (static_cast<int>(domainSubboxes_.size()) < capacity_) {
                Box rightChild = domainBox.bisectBy(infBisector);
                if (rightChild != boxes::EMPTY_BOX) {
                    domainSubboxes_.insert(it, rightChild);
                }
            }
            it++;
        }
    }
    return domainSubboxes_.empty() ? Solution::no : Solution::unknown;
}

/*------------ SplitAndKeepWorklist --------------------*/

BisectAndKeepFRame* BisectAndKeepFRame::performBisection(const bisector::IBisector &bisector) {
    Box bisectedParamBox = paramBox.bisectBy(bisector);
    if (bisectedParamBox == boxes::EMPTY_BOX) {
        return nullptr;
    }
    return new BisectAndKeepFRame(bisectedParamBox, *this);
}

Solution BisectAndKeepFRame::hasSolution(const IBisector &bisector, const IDegree &dc) {
    auto it = domainSubboxes_.begin();
    while (it != domainSubboxes_.end()) {
        Box& domainBox = it->first;
        bool& isLeaf = it->second;
        if (!func.eval(domainBox, paramBox).containsZero()) {
            it = domainSubboxes_.erase(it);
        } else {
            SignCoveringList sc = func.computeBoundarySignCovering(domainBox,
                                                           paramBox, bisector);
            bool sufficient = boxes::isSignCoveringListDetermined(sc);
            if (sufficient && dc.deg(sc) != 0) {
                return Solution::yes;
            } else if (sufficient && isLeaf) {
                if (static_cast<int>(domainSubboxes_.size()) < capacity_) {
                    // remove the parent box, by effectively replacing it by its left child
                    Box rightChild = domainBox.bisectBy(infBisector);
                    if (rightChild != boxes::EMPTY_BOX) {
                        domainSubboxes_.insert(it, std::make_pair(rightChild, true));
                        vytlassert(isLeaf, "Child must be marked as leaf.");
                    }
                } // else keep parent, insufficient capacity
            } else if (isLeaf) { // and not sufficient
                if (static_cast<int>(domainSubboxes_.size()) + 1 < capacity_) {
                    // bisect parent into leaves, but keep the parent as well
                    Box leftChild = Box(domainBox);
                    Box rightChild = leftChild.bisectBy(infBisector);
                    if (rightChild != boxes::EMPTY_BOX) {
                        isLeaf = false; // parent is no longer a leaf
                        domainSubboxes_.insert(it, std::make_pair(leftChild, true));
                        domainSubboxes_.insert(it, std::make_pair(rightChild, true));
                    }
                }
            } else if (sufficient) { // not a leaf, but sufficient and deg = 0
                // Remove this box, its children covered it
                it = domainSubboxes_.erase(it);
            }
            it++;
        }
    }

    for (const auto& boxIsLeafPair : domainSubboxes_) {
        if (boxIsLeafPair.second) {
            return Solution::unknown;
        }
    }

    return Solution::no;
}



/*---------------------- WorkTree -----------------------*/

// Method used for assertion testing
bool TreeFrame::countTest() const {
    int cnt = 0;
    std::stack<Node*> st;
    st.push(top_.get());
    while (!st.empty()) {
        Node* current = st.top();
        st.pop();
        for (auto &child : current->children) {
            st.push(child.get());
            ++cnt;
        }
    }
    return cnt == nodeCount_;
}

TreeFrame::TreeFrame(Box paramBox, IFunction &func,
                   const Box &domainBox, int capacity):
    Frame(paramBox, func), capacity_(capacity), nodeCount_(0) {
    top_.reset(new Node(boxes::EMPTY_BOX, *this)); // dummy node as root
    vytlassert(top_->isLeaf(), "Top dummy node must be a leaf initially.");
    top_->addChild(domainBox);
}

TreeFrame::TreeFrame(boxes::Box paramBox, const TreeFrame &other, function::IFunction &func):
    Frame(paramBox, func), capacity_(other.capacity_), nodeCount_(0) {

    // Make deep copy of the other tree
    // Start with the top
    top_ = UPtrNode(new Node(boxes::EMPTY_BOX, *this)); // Nothing to copy, it's just a dummy node
    std::stack<std::pair<Node*, Node*> > st;
    st.emplace(top_.get(), other.top_.get());

    while (!st.empty()) {
        Node* current = st.top().first;
        Node* otherCurrent = st.top().second;
        vytlassert(current != nullptr, "current node popped from stack cannot be null");
        vytlassert(otherCurrent != nullptr, "other node popped from stack cannot be null");
        st.pop();
        for (auto& otherChild : otherCurrent->children) {
            Node* child = current->addChild(otherChild->domain, otherChild->determined);
            st.emplace(child, otherChild.get());
        }
    }

    vytlassert(nodeCount_ == other.nodeCount_, "After duplication, sizes must match.");
}

TreeFrame* TreeFrame::performBisection(const bisector::IBisector &bisector) {
    Box bisectedParamBox = paramBox.bisectBy(bisector);
    if (bisectedParamBox == boxes::EMPTY_BOX) {
        return nullptr;
    }
    return new TreeFrame(bisectedParamBox, *this, this->func);
}

void TreeFrame::bisectLeaves(Node *&leaf, Node *&parent) {
    vytlassert(leaf->isLeaf(), "Only leaf can be bisected");
    if (leaf->determined) {
        vytlassert(nodeCount_ <= capacity_, "Size cannot exceed the capacity");
        if (nodeCount_ == capacity_) // No capacity left
            return;
        // Leaf can be removed, children will be attached to its parent instead
        vytlassert(parent != nullptr, "Parent cannot be null");
        auto rightChildBox = leaf->domain.bisectBy(infBisector);
        if (rightChildBox != boxes::EMPTY_BOX) {
            // bisection successful
            parent->addChild(rightChildBox);
        }
    } else {
        vytlassert(nodeCount_ <= capacity_, "Size cannot exceed the capacity");
        if (nodeCount_ >= capacity_ - 1) // No capacity left
            return;
        // leaf will be kept
        auto br = infBisector.bisect(leaf->domain);
        if (br.index > -1) {
            // bisection possible
            auto leftChildBox = Box(leaf->domain);
            auto rightChildBox = leftChildBox.bisectBy(br);
            vytlassert(rightChildBox != boxes::EMPTY_BOX, "Empty box cannot be result of"
                                                          " successful bisection.");
            leaf->addChild(leftChildBox);
            leaf->addChild(rightChildBox);
        }
    }
}


/* Performs DFS variation using stack, visiting each node twice. Once on the
 * way down (for forward pruning), like classic DFS, and once on the way up
 * (for backward pruning). */
void TreeFrame::eliminateNodesWithoutZero() {
    // Stack of nodes and indicator of first vist.

    std::stack<std::pair<Node*, bool> > st;
    vytlassert(top_ != nullptr, "Top dummy node cannot be null");
    st.emplace(top_.get(), true);

    while(!st.empty()) {
        Node* currentParent = st.top().first;
        bool &firstVisit = st.top().second;

        if (firstVisit) {
            firstVisit = false;
            // Examine children of current node
            auto& children = currentParent->children;
            auto it = children.begin();
            while (it != children.end()) {
                // Examine children of current node
                Node* child = it->get();
                if (func.eval(child->domain, paramBox).containsZero()) {
                    if (!child->isLeaf()) {
                        st.emplace(child, true); // Schedule first visit of the child
                        child->wasNotLeaf = true;
                    } else {
                        child->wasNotLeaf = false;
                    }
                    ++it;
                } else {
                    // Forward pruning at the child
                    child->removeAllChildren();
                    it = currentParent->removeChild(it);
                }
            }
        } else { // Second visit, attempts for backward pruning
            st.pop();
            auto& children = currentParent->children;
            auto it = children.begin();
            while (it != children.end()) {
                Node* child = it->get();
                if (child->isLeaf() && child->wasNotLeaf) {
                    // Backward pruning of the child
                    it = currentParent->removeChild(it);
                } else {
                    ++it;
                }
            }
        }
    }

    assert(countTest());
}

bool TreeFrame::isEmpty() {
    assert(top_ != nullptr);
    return top_->isLeaf();
}

Solution TreeFrame::hasSolution(const IBisector &bisector, const IDegree &dc) {
    assert(countTest());
    assert(!top_->isLeaf() || nodeCount_ == 0);

    eliminateNodesWithoutZero();

    if (isEmpty()) {
        return Solution::no;
    }

    std::stack<Node*> st;
    assert(top_ != nullptr);
    st.emplace(top_.get());

    while (!st.empty()) {
        Node* currentParent = st.top();
        st.pop();
        auto &children = currentParent->children;
        auto it = children.begin();
        while (it != children.end()) {
            Node* currentChild = it->get();

            // Only not yet determined leaves may underdo degree computation
            assert(!currentChild->determined || currentChild->isLeaf());
            if (!currentChild->determined) {
                // Attempt degree computation
                SignCoveringList sc = func.computeBoundarySignCovering(
                            currentChild->domain, paramBox, bisector);
                currentChild->determined = boxes::isSignCoveringListDetermined(sc);
                if (currentChild->determined && dc.deg(sc) != 0) {
                    return Solution::yes;
                }
            }

            if (currentChild->isLeaf()) { // leaf
                bisectLeaves(currentChild, currentParent);
                ++it;
            } else { // non leaf

                // If deg = 0 was computed for current, we don't need to keep it around anymore
                if (currentChild->determined) {
                    // Reattach child's children to parent's children
                    auto &grandChildren = currentChild->children;
                    currentParent->children.splice(currentParent->children.end(), grandChildren);

                    // Remove current child from parent
                    it = currentParent->removeChild(it);
                } else {
                    // Push this child onto the stack
                    st.emplace(currentChild);
                    ++it;
                }
            }
        }

    }

    return Solution::unknown;
}



}
}
