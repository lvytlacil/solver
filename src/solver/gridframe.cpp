#include "../function/function.h"
#include "../bisector/bisector.h"
#include "../degree/idegreecomputation.h"
#include <stack>
#include "frame.h"
using namespace std;
namespace vytlalib {
using boxes::Box;
using boxes::BoxList;
using boxes::SignCoveringList;
using bisector::IBisector;
using bisector::Bisector;
using boxes::Component;
using degree::IDegree;
using function::IFunction;

namespace solver {

// Bisector with zero threshold
static const Bisector infBisector(0);

// Set for storing visited nodes during oriented cubical set constructions
std::set<GridFrame::Node*> GridFrame::visited;
std::set<GridFrame::Node*> GridFrame::currentComponent;

/*------------- WorkGraph2 -------------- */
GridFrame::GridFrame(Box paramBox, IFunction &func, const Box &domainBox, int capacity):
    Frame(paramBox, func), capacity_(capacity) {
    nodes_.emplace_back(new Node(domainBox));
    auto& node = nodes_.back();

    BoxList boundary = node->domain->computeInducedBoundary();
    int i = 0;
    for (Box& face : boundary) {
        node->freeEdges[i].emplace_back(new Box(face));
        ++i;
    }

    /*
    bisector::Bisector b(.0000050);

    for (int i = 0; i < 7; ++i) {

        std::set<Node*> splitSet;
        for (auto& nodeptr : nodes_) {
            splitSet.insert(nodeptr.get());
        }

        for (Node* node : splitSet) {
            refineNode(node, b);
        }
    }*/
}

GridFrame* GridFrame::performBisection(const IBisector &bisector) {
    Box bisectedParamBox = paramBox.bisectBy(bisector);
    if (bisectedParamBox == boxes::EMPTY_BOX) {
        return nullptr;
    }
    return new GridFrame(bisectedParamBox, *this, this->func);
}

GridFrame::Node* GridFrame::getOrCreateClone(Node *key, std::map<Node *, Node *> &map) {
    if (map.count(key) == 0) {
        assert(key != nullptr);
        assert(key->domain != nullptr);
        nodes_.emplace_back(new Node(*key->domain));
        Node* result = nodes_.back().get();
        map[key] = result;
        return result;
    }
    return map[key];
}

GridFrame::GridFrame(boxes::Box paramBox, const GridFrame &other, function::IFunction &func):
    Frame(paramBox, func), capacity_(other.capacity_) {

    std::map<Node*, Node*> cloneMap;
    for (auto it = other.nodes_.begin(); it != other.nodes_.end(); ++it) {
        Node *otherNode = it->get();

        Node *currentNode = getOrCreateClone(otherNode, cloneMap);
        assert(currentNode != nullptr);

        for (int i = 0; i < otherNode->getDoubleSize(); ++i) {

            for (auto& nodeFacePair : otherNode->edges[i]) {
                Node* otherEndpoint = nodeFacePair.first;
                SPtrBox face = nodeFacePair.second;

                Node* currentEndpoint = getOrCreateClone(otherEndpoint, cloneMap);

                assert(currentEndpoint != nullptr);

                currentNode->edges[i][currentEndpoint] = std::move(face);

            }

            for (auto& face : otherNode->freeEdges[i]) {
                currentNode->freeEdges[i].push_back(face);
            }
        }
    }
}

void GridFrame::eliminateNodesWithoutZero() {
    auto it = nodes_.begin();
    while (it != nodes_.end()) {
        Node* node = it->get();
        assert(node != nullptr);
        if (!func.eval(*node->domain, paramBox).containsZero()) {
            // Detach from neighbours
            for (int i = 0; i < node->getDoubleSize(); ++i) {
                EdgeMap& edgeMap = node->edges[i]; // Edges in current dimension/direction
                // Index of opposing edges from neighbouring node
                int otherIndex = i % 2 == 0 ? i + 1 : i - 1;

                for (auto& nodeFacePair : edgeMap) {
                    // Over all edges in current dim/dir
                    Node* other = nodeFacePair.first;

                    assert(other != nullptr);
                    assert(!other->edges[otherIndex].empty());
                    assert(other->edges[otherIndex].count(node) > 0);

                    EdgeMap& otherEdgeMap = other->edges[otherIndex];
                    auto faceIt = otherEdgeMap.find(node);
                    assert(faceIt != otherEdgeMap.end()); // opposing edge exists
                    other->freeEdges[otherIndex].emplace_back(std::move(faceIt->second));
                    otherEdgeMap.erase(faceIt);
                } // for edge list
            } // for dimension/direction
            // Remove the actual node
            it = nodes_.erase(it);
        } else {
            ++it;
        }
    }

}

void GridFrame::getCoveringOfConnectedComp(Node *node, const std::set<Node *> &visited,
                                           std::set<Node *> &currentComponent,
                                           SignCoveringList &sc, const IBisector &bisector) {
    assert(currentComponent.size() == 0); // must start with an empty component
    assert(sc.size() == 0); // must start with an empty sign covering
    std::stack<Node*> st;
    st.push(node);
    // DFS to extend the component
    while (!st.empty()) {
        Node* current = st.top();
        st.pop();
        if (currentComponent.count(current) > 0) // To avoid processing the same boxes over
            continue;
        currentComponent.insert(current);

        for (int i = 0; i < current->getDoubleSize(); ++i) {
            // Edges without endpoints
            for (auto& face : current->freeEdges[i]) {
                SignCoveringList faceSc = func.computeSignCovering(*face, paramBox, bisector);

                // Cannot extend pass the end of the grid, so the
                // subface covering will be always accepted (spliced into sc)
                sc.splice(sc.end(), faceSc);
            }

            // Edges to other nodes
            EdgeMap& edgeSet = current->edges[i];
            for (auto& faceNodePair : edgeSet) { // Over edges in the given dim/dir
                Node* endPoint = faceNodePair.first;
                const SPtrBox &face = faceNodePair.second;

                assert(endPoint != nullptr);
                assert(face != nullptr);
                SignCoveringList faceSc = func.computeSignCovering(*face, paramBox, bisector);
                if (visited.count(endPoint) > 0 || boxes::isSignCoveringListDetermined(faceSc)) {
                    // Do not extend the component, if either the subface was sufficiently
                    // covered, or if that would mean adding a box from already processed
                    // different component.
                    sc.splice(sc.end(), faceSc);
                } else {
                    st.push(endPoint);
                }
            } // for edges in given dim/dir
        }
    }
}

Solution GridFrame::hasSolution(const IBisector &bisector, const IDegree &dc) {

    eliminateNodesWithoutZero();

    if (isEmpty()) {
        return Solution::no;
    }


    visited.clear();

    SignCoveringList sc;
    for (auto it = nodes_.begin(); it != nodes_.end(); ++it) {
        Node* node = it->get();

        if (visited.count(node) > 0) {
            // This node already was part of some evaluated connected component
            continue;
        }
        sc.clear();
        currentComponent.clear();

        getCoveringOfConnectedComp(node, visited, currentComponent, sc, bisector);
        if (boxes::isSignCoveringListDetermined(sc) && dc.deg(sc) != 0) {
            return Solution::yes;
        }


        // Update the set of previously visited nodes
        for (auto &node : currentComponent) {
            visited.insert(node);
        }

    }

    assert(consistency());
    assert(faceConsistency());

    auto nodesEnd = nodes_.end(); // To avoid splitting children created during this call

    for (auto it = nodes_.begin(); it != nodesEnd; ++it) {
        if (static_cast<int>(nodes_.size()) < capacity_) {
            refineNode(it->get(), infBisector);
        }
    }

    return Solution::unknown;
}

void GridFrame::faceSplit(Node *left, Node *right, int index, int dim , double step) {

    //double low = left->domain->getComp(dim).lower();
    //double high = right->domain->getComp(dim).upper();

    assert(low < high);
    EdgeMap& leftEdges = left->edges[index];
    EdgeMap& rightEdges = right->edges[index];
    assert(rightEdges.size() == 0); // Right has no edges with the given index yet
    // Scan all edges of left on given index
    auto it = leftEdges.begin();
    while (it != leftEdges.end()) {
        auto& nodeFacePair = *it;
        Node* endpoint = nodeFacePair.first;
        SPtrBox &leftFace = nodeFacePair.second;

        // lower/upper bound of component along which the split was made.
        double lower = leftFace->getComp(dim).lower();
        double upper = leftFace->getComp(dim).upper();
        assert(low <= lower && upper <= high);
        assert(rightEdges.count(endpoint) == 0);
        if (upper <= step) {
            // Edge stays in left
            ++it;
        } else if (step <= lower) {
            // Edge moved to right
            rightEdges[endpoint] = std::move(leftFace);
            it = leftEdges.erase(it);

            // Reconnect opposite edge to rightNode
            int otherIndex = index % 2 == 0 ? index + 1 : index - 1;
            EdgeMap& opposingEdges = endpoint->edges[otherIndex];
            auto faceIt = opposingEdges.find(left);
            assert(faceIt != opposingEdges.end());
            opposingEdges[right] = std::move(faceIt->second);
            opposingEdges.erase(faceIt);
        } else {
            // Face will be split
            assert(lower < step && step < upper);
            // Face of right child
            SPtrBox rightFace(new Box(*leftFace));
            rightFace->setComp(dim, Component(step, upper));
            rightEdges[endpoint] = std::move(rightFace);

            // Face of left child
            // Make hard copy of face if needed
            if (leftFace.use_count() > 1)
                leftFace.reset(new Box(*leftFace));
            leftFace->setComp(dim, Component(lower, step));


            // Now split the opposing edge as well
            int otherIndex = index % 2 == 0 ? index + 1 : index - 1;
            EdgeMap& opposingEdges = endpoint->edges[otherIndex];
            assert(opposingEdges.count(left) == 1);
            assert(opposingEdges.count(right) == 0);
            SPtrBox& leftOtherFace = opposingEdges[left];

            SPtrBox rightOtherFace(new Box(*leftOtherFace));
            rightOtherFace->setComp(dim, Component(step, upper));

            opposingEdges[right] = std::move(rightOtherFace);

            if (leftOtherFace.use_count() > 1)
                leftOtherFace.reset(new Box(*leftOtherFace));
            leftOtherFace->setComp(dim, Component(lower, step));
            ++it;

        }
    }



    EdgeList& leftFreeEdges = left->freeEdges[index];
    EdgeList& rightFreeEdges = right->freeEdges[index];
    assert(rightFreeEdges.size() == 0); // Right has no edges with the given index yet
    // Scan all edges of left on given index
    {
    auto it = leftFreeEdges.begin();
    while (it != leftFreeEdges.end()) {
        SPtrBox& leftFace = *it;

        const Component &bnd = leftFace->getComp(dim); // Component determining stay/move/split

        //cout << low << ":" << bnd.lower() << endl;
        //cout << bnd.upper() << ":" << high << endl;
        assert(low <= bnd.lower() && bnd.upper() <= high);
        if (bnd.upper() <= step) {
            // Edge stays in left
            ++it;
        } else if (step <= bnd.lower()) {
            // Edge moved to right
            rightFreeEdges.push_back(std::move(leftFace));
            it = leftFreeEdges.erase(it);
        } else {
            // Face will be split
            assert(bnd.lower() < step && step < bnd.upper());
            // Face of right child
            SPtrBox rightFace(new Box(*leftFace));
            rightFace->setComp(dim, Component(step, bnd.upper()));
            rightFreeEdges.push_back(std::move(rightFace));

            // Face of left child
            // Make hard copy of face if needed
            if (leftFace.use_count() > 1)
                leftFace.reset(new Box(*leftFace));
            leftFace->setComp(dim, Component(bnd.lower(), step));
            ++it;
        }
    }
    }
}

void GridFrame::refineNode(Node *leftNode, const IBisector &bisector) {
    // Check if it is possible to bisect
    SPtrBox &domain = leftNode->domain;
    auto br = bisector.bisect(*domain);
    int dim = br.index;
    double step = br.step;

    if (dim == -1)
        return;
    // Hard copy
    if (domain.use_count() > 1)
        domain.reset(new Box(*domain));

    // Actual domain bisection
    Box rightDomain = domain->bisectBy(br);
    assert(rightDomain != boxes::EMPTY_BOX);

    // New node creation
    nodes_.emplace_back(new Node(rightDomain));
    Node* rightNode = nodes_.back().get();

    for (int i = 0; i < domain->getSize(); ++i) {
        if (i == dim) {
            // Edges to upper are moved to the right child
            EdgeMap& leftEdges = leftNode->edges[2 * i + 1];
            EdgeList& leftFreeEdges = leftNode->freeEdges[2 * i + 1];
            EdgeMap& rightEdges = rightNode->edges[2 * i + 1];
            EdgeList& rightFreeEdges = rightNode->freeEdges[2 * i + 1];
            assert(rightEdges.size() == 0 && rightFreeEdges.size() == 0);

            rightFreeEdges.splice(rightFreeEdges.end(), leftFreeEdges);
            assert(leftFreeEdges.empty());

            rightEdges.swap(leftEdges);
            assert(leftEdges.empty());
            // Now opposite edges (from neigbours) must be repointed
            for (auto& nodeFacePair : rightEdges) {
                Node* endpoint = nodeFacePair.first;
                EdgeMap& opposingLeftEdges = endpoint->edges[2 * i];

                auto faceIt = opposingLeftEdges.find(leftNode);
                assert(faceIt != opposingLeftEdges.end());
                opposingLeftEdges[rightNode] = std::move(faceIt->second);
                opposingLeftEdges.erase(faceIt);
            }

            // Now establish the one new face left and right childs are connected with

            SPtrBox connectionFace(new Box(leftNode->domain->computeTopologicalFace(i, false)));
            SPtrBox connectionFaceOther(new Box(rightNode->domain->computeTopologicalFace(i, true)));

            leftEdges[rightNode] = std::move(connectionFace);
            assert(rightNode->edges[2 * i].empty());
            rightNode->edges[2 * i][leftNode] = std::move(connectionFaceOther);
        } else {
            faceSplit(leftNode, rightNode, 2 * i, dim, step);
            faceSplit(leftNode, rightNode, 2 * i + 1, dim, step);
        }
    }

    /*
    cout << "Hrany a ja:\n";
    cout << "( " << *leftNode->domain << " )" << endl;
    for (int i = 0; i < leftNode->getDoubleSize(); ++i) {
        EdgeMap& em = leftNode->edges[i];
        for (auto &kv : em) {
            cout << *kv.second << endl;
        }
        EdgeList& el = leftNode->freeEdges[i];
        for (auto &kv : el) {
            cout << "---" << *kv << endl;
        }
    }
    cout << endl;

    cout << "Hrany a ja:\n";
    cout << "( " << *rightNode->domain << " )" << endl;
    for (int i = 0; i < rightNode->getDoubleSize(); ++i) {
        EdgeMap& em = rightNode->edges[i];
        for (auto &kv : em) {
            cout << *kv.second << endl;
        }
        EdgeList& el = rightNode->freeEdges[i];
        for (auto &kv : el) {
            cout << "---" << *kv << endl;
        }
    }
    cout << endl << flush;*/
}




}


}
