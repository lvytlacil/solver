#ifndef ALGORITHM_H
#define ALGORITHM_H
#include "algorithm.h"
#include "frame.h"
#include "../box/boxtypedefs.h"
#include "../function/functionfwd.h"
#include "../bisector/bisectorfwd.h"
#include "../degree/degreecomputationfwd.h"

namespace vytlalib {

namespace solver {

class Solver {
private:
    struct StackFrame {
        UPtrWorklist wlist;
        Solution result;

        StackFrame(UPtrWorklist wlist, Solution result):
            wlist(std::move(wlist)), result(result) { }
    };
protected:
    virtual void performPaving(const boxes::Box &vars,
                               const boxes::Box &params,
                               UPtrWorklist topWorklist,
                               const bisector::IBisector &bisector, double signCovFaceThreshold,
                               const degree::IDegree &dc,
                               boxes::BoxList &yesList,
                               boxes::BoxList &noList) const;

public:
    virtual void solve(const boxes::Box &vars,
                       const boxes::Box &params,
                       const bisector::IBisector &bisector, double widthThreshold,
                       int capacity,
                       function::IFunction &function, const degree::IDegree &dc,
                       boxes::BoxList &yesList,
                       boxes::BoxList &noList) const = 0;
    virtual ~Solver() { }
};

class StaticFrameSolver : public Solver {
public:
    void solve(const boxes::Box &vars,
               const boxes::Box &params,
               const bisector::IBisector &bisector, double widthThreshold,
               int capacity,
               function::IFunction &function, const degree::IDegree &dc,
               boxes::BoxList &yesList,
               boxes::BoxList &noList) const override;
};

class SplitOnlyFrameSolver : public Solver {
public:
    void solve(const boxes::Box &vars,
               const boxes::Box &params,
               const bisector::IBisector &bisector, double widthThreshold,
               int capacity,
               function::IFunction &function, const degree::IDegree &dc,
               boxes::BoxList &yesList,
               boxes::BoxList &noList) const override;
};

class SplitAndKeepFrameSolver : public Solver {
public:
    void solve(const boxes::Box &vars,
               const boxes::Box &params,
               const bisector::IBisector &bisector, double widthThreshold,
               int capacity,
               function::IFunction &function, const degree::IDegree &dc,
               boxes::BoxList &yesList,
               boxes::BoxList &noList) const override;
};

class TreeFrameSolver : public Solver {
public:
    void solve(const boxes::Box &vars,
               const boxes::Box &params,
               const bisector::IBisector &bisector, double widthThreshold,
               int capacity,
               function::IFunction &function, const degree::IDegree &dc,
               boxes::BoxList &yesList,
               boxes::BoxList &noList) const override;
};

class GridFrameSolver : public Solver {
public:
    void solve(const boxes::Box &vars,
               const boxes::Box &params,
               const bisector::IBisector &bisector, double widthThreshold,
               int capacity,
               function::IFunction &function, const degree::IDegree &dc,
               boxes::BoxList &yesList,
               boxes::BoxList &noList) const override;
};

}
}
#endif // ALGORITHM_H
