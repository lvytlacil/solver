#ifndef FRAMEFWD_H
#define FRAMEFWD_H
#include <memory>

namespace vytlalib {
namespace solver {

class Frame;

/**
 * @brief Enumeration used to express the result of the
 * frame manipulating method IFrame::hasSolution
 */
enum class Solution {yes, no, unknown};

/**
 * @brief Unique pointer to Frame instance.
 */
typedef std::unique_ptr<Frame> UPtrWorklist;

}
}

#endif // WORKLISTFWD_H
