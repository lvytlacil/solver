#include "messageprocessor.h"
#include <iostream>
using std::endl;
namespace vytlalib {
namespace message {



void raise(std::initializer_list<std::string> msgParts) {
    std::string result = "";

    for (auto msgPart : msgParts) {
        result += msgPart;
    }

    throw result.c_str();
}

void raise(std::string msg) {
    throw msg.c_str();
}

    const std::unique_ptr<IMessageProcessor> IMessageProcessor
        ::mproc(new MessageProcessor(std::cout, Severity::error));


    std::ostream& operator<<(std::ostream &out, const Severity &severity) {
        switch (severity) {
        case Severity::info:
            out << "INFO";
            break;
        case Severity::warning:
            out << "WARNING";
            break;
        case Severity::error:
            out << "ERROR";
            break;
        case Severity::assertion:
            out << "ASSERTION ERROR";
            break;
        }
        return out;
    }

    void MessageProcessor::process(std::ostream &out, IMessage &message) {
        out << "Received message with severity: " << message.getSeverity() << endl;
        out << "From: " << message.getSender() << endl;
        out << "Content: " << message.getBody() << endl;

        if (message.getSeverity() >= minimalSeverityToTerminate) {
            throw "A too severe message was processed, that caused exception to be thrown.";
        }
    }

    void MessageProcessor::process(IMessage &message) {
        switch (message.getSeverity()) {
        case Severity::info:
            process(infoOut, message);
            break;
        case Severity::warning:
            process(warnOut, message);
            break;
        case Severity::error:
            process(errorOut, message);
            break;
        case Severity::assertion:
            process(assertOut, message);
            break;
        default:
            break;
        }
    }
}
}
