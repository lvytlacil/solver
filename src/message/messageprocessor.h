#ifndef MESSAGEPROCESSOR_H
#define MESSAGEPROCESSOR_H
#include <iosfwd>
#include <memory>
#include <string>
#include <sstream>

namespace vytlalib {
namespace message {

struct AssertionException : public std::exception {
    std::string text;

    AssertionException(const char* file, int line, const char* expression, const std::string &msg) {
        using namespace std;
        stringstream ss;
        ss << "AssertionExecption" << endl << msg << endl << "Line: " << line << ", "
            << "File: " << file << endl << "Expression: " << expression << endl;
        text = ss.str();
    }

    ~AssertionException() throw () {}
    const char* what() const throw() {
        return text.c_str();
    }
};

struct ProgramException : public std::exception {
    std::string text;

    ProgramException(const std::string &msg):
        text(msg) { }

    ProgramException(std::initializer_list<std::string> msgParts) {
        text = "";
        for (auto& part : msgParts) {
            text += part;
        }
    }

    ~ProgramException() throw () {}
    const char* what() const throw() {
        return text.c_str();
    }
};

void raise(std::initializer_list<std::string> msgParts);
void raise(std::string msg);

#ifndef NDEBUG
#define vytlassert(EXPRESSION, MESSAGE) if(!(EXPRESSION))\
{ throw vytlalib::message::AssertionException\
    (__FILE__, __LINE__, #EXPRESSION, MESSAGE); }
#else
#define vytlassert(EXPRESSION, MESSAGE)
#endif


enum class Severity {
    info = 0,
    warning = 1,
    error = 2,
    assertion = 3
};

std::ostream& operator<<(std::ostream &out, const Severity &severity);

class IMessage {
public:
    virtual Severity getSeverity() = 0;
    virtual const std::string& getBody() = 0;
    virtual const std::string& getSender() = 0;
};

class Message : public IMessage {
protected:
    const std::string sender;
    const std::string body;
    const Severity severity;

public:
    Message(std::string sender, std::string body, Severity severity):
        sender(sender), body(body), severity(severity) { }

    const std::string& getBody() override {
        return body;
    }

    const std::string& getSender() override {
        return sender;
    }

    Severity getSeverity() override {
        return severity;
    }


};

class MessageProcessor;

class IMessageProcessor {
public:
    static const std::unique_ptr<IMessageProcessor> mproc;

    virtual void process(IMessage &message) = 0;
};


class MessageProcessor : public IMessageProcessor
{
protected:
    std::ostream &infoOut;
    std::ostream &warnOut;
    std::ostream &errorOut;
    std::ostream &assertOut;
    Severity minimalSeverityToTerminate;

    void process(std::ostream &out, IMessage &message);


public:
    MessageProcessor(std::ostream &out, Severity termSeverity):
        infoOut(out), warnOut(out), errorOut(out), assertOut(out), minimalSeverityToTerminate(termSeverity) { }

    MessageProcessor(std::ostream &infoOut, std::ostream &warnOut, std::ostream &errorOut, std::ostream &assertOut,
                     Severity termSeverity):
        infoOut(infoOut), warnOut(warnOut), errorOut(errorOut), assertOut(assertOut),
        minimalSeverityToTerminate(termSeverity) { }

    void process(IMessage &message) override;
};

}
}

#endif // MESSAGEPROCESSOR_H
