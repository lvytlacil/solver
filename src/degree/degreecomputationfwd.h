#ifndef DEGREECOMPUTATIONFWD_H
#define DEGREECOMPUTATIONFWD_H
#include <memory>

namespace vytlalib {
namespace degree {

class IDegree;
class Degree;
class ParallelDegree;

/**
 * @brief A Unique pointer to IDegreeComputation instance.
 */
typedef std::unique_ptr<IDegree> UPtrIDegreeComputation;

}
}

#endif // DEGREECOMPUTATIONFWD_H
