#include "paralleldegreecomputation.h"
#include "../box/box.h"
#include <vector>
#include <thread>
#include <mutex>
#include <ctime>
namespace vytlalib {
using boxes::Component;
using boxes::Box;
using boxes::BoxList;
using boxes::OrientedBox;
using boxes::BoxOrientation;
using boxes::Sign;
using boxes::SignVector;
using boxes::BoxSignVectorPair;
using boxes::SignCoveringList;
namespace degree {

/**
 * @brief Encapsulates the parallel part of the computation.
 */
class FillFacesParallelWorker {
private:

    bool running_; // indicates if running to prevent multiple starts
    std::vector<std::thread> children_; // stores work threads

    // Parameters from fillFaces
    const ParallelDegree &pdc_; // to reuse code of some non-public methods
    SignCoveringList &selectedFaces_; // selected faces
    const SignCoveringList &non_; // non selected boxes, read-only
    int signIndex_; // selected index, read-only
    SignCoveringList &result_; // the computation result
    int childCount_; // Number of work threads

    std::vector<SignCoveringList> inputs;
    std::vector<SignCoveringList> outputs;

    // The method for work threads, where the actual work is done
    static void doWork(FillFacesParallelWorker *w, int i) {
        SignCoveringList::iterator toBeMoved; // Iterator used to move to selBox
        for (BoxSignVectorPair &selBoxSignListPair : w->inputs[i]) {

            BoxList boundary = selBoxSignListPair.first.computeInducedBoundary();
            // Do your actual work
            for (Box &selFace : boundary) {
                // Test intersection with each elem of non
                for (const auto& elem : w->non_) {
                    const Box &nonBox = elem.first;
                    int intersectionIndex = -1;
                    double step = 0.0;
                    bool nonTrivIntersection = w->
                            pdc_.determineBoundaryRelationship(selFace, nonBox, intersectionIndex, step);

                    if (nonTrivIntersection) {
                        // n-1-dim intersection; index and step have been written to by the method call
                        if (intersectionIndex == -1) {
                            // selFace is subset of nonBox, push to result and continue with checking next face
                            SignVector projected = elem.second;
                            assert(0 <= w->signIndex_ && w->signIndex_ < projected.getSize());
                            projected.removeAt(w->signIndex_);
                            // Save the result
                            w->outputs[i].push_back(BoxSignVectorPair(std::move(selFace), projected));
                            break;
                        } else {
                            // TODO: You may consider to actually delete item at iterator's position
                            // split boundaryBox and make sure sub-boxes will be examined
                            const Component& bisected = selFace.getComp(intersectionIndex);
                            auto left = Box(selFace);
                            auto right = Box(selFace);
                            left.setComp(intersectionIndex, Component(bisected.lower(), step));
                            right.setComp(intersectionIndex, Component(step, bisected.upper()));
                            boundary.push_back(std::move(left));
                            boundary.push_back(std::move(right));
                            break;
                        }
                    }
                    // else intersection has dim < n-1, nothing to do
                } // for loop over non selected boxes
            } // for loop over selected faces
        } // while
    }

public:
    /**
     * @brief Creates the instance, initializing all required data.
     * @param pdc Reference to the calling instance, to reuse some of its non-public code.
     * @param childCount The number of work threads to use.
     * @param selBoxes Selected boxes (The L_sel set). Will be mutated by this instance.
     * @param nonBoxes Non-selected boxes (The L_non set).
     * @param signIndex Selected sign index.
     * @param result Empty sign covering into which the result will be written.
     */
    FillFacesParallelWorker(const ParallelDegree &pdc,
                            int childCount, SignCoveringList &selBoxes, const SignCoveringList &nonBoxes,
                            int signIndex, SignCoveringList &result):
        running_(false), pdc_(pdc), selectedFaces_(selBoxes), non_(nonBoxes),
        signIndex_(signIndex), result_(result), childCount_(childCount) { }

    /**
     * @brief Starts the actual working process. Main thread is suspended until the work is done.
     */
    void run() {
        assert(!running_);
        assert(childCount_ > 0);

        // Init input collections
        for (int i = 0; i < childCount_; ++i) {
            inputs.push_back(SignCoveringList());
            outputs.push_back(SignCoveringList());
        }

        // Splice inputs
        long k = 0;
        auto it = selectedFaces_.begin();
        while (it != selectedFaces_.end()) {
            auto toBeSpliced = it++;
            inputs[k % childCount_].splice(inputs[k % childCount_].end(), selectedFaces_, toBeSpliced);
            ++k;
        }

        // Create work threads
        for (int i = 0; i < childCount_; ++i) {
            children_.push_back(std::thread(&FillFacesParallelWorker::doWork, this, i));
        }
        // Wait for work to finish
        for (auto& child : children_) {
            child.join();
        }

        // Splice results
        for (int i = 0; i < childCount_; ++i) {
            result_.splice(result_.end(), outputs[i]);
        }
    }

};

void ParallelDegree::fillFaces(SignCoveringList &selBoxes, const SignCoveringList &nonBoxes,
                                          int signIndex, SignCoveringList &result) const {
    FillFacesParallelWorker ffpw(*this, getWorkThreads(), selBoxes, nonBoxes, signIndex, result);
    ffpw.run();
}


int ParallelDegree::deg(const SignCoveringList &covering,
                                       const ISignIndexSelectionStrategy &strategy) const {
    selectedBoxesCount = 0;
    nonSelectedBoxesCount = 0;

    assert(boxes::isSignCoveringListDetermined(covering));
    Sign acc = Sign::PLUS;

    SignCoveringList actual;
    for (const auto &it : covering) {
        actual.push_back(BoxSignVectorPair(Box(it.first), it.second));
    }

    while (true) {
        if (actual.size() == 0)
           return 0;

        int boundaryDimension = actual.front().first.getTopologicalDimension();
        assert(boundaryDimension >= 0);
        assert(checkDimensions(actual, boundaryDimension));
        if (boundaryDimension == 0) {
            return Degree::compute(actual, acc);
        }

        // determine position and sign
        auto signSelection = strategy.selectSign(actual);
        int selectedIndex = signSelection.first;
        Sign selectedSign = signSelection.second;

        // create and fill lists of selected and non selected boxes
        std::list<BoxSignVectorPair> selected;
        std::list<BoxSignVectorPair> non;
        auto it = actual.begin();
        while (it != actual.end()) {
            auto toBeSpliced = it;
            ++it;
            // pair of oriented box and a sign list
            const auto& boxSignListPair = *toBeSpliced;
            if (selectedSign == boxSignListPair.second[selectedIndex]) {
                selected.splice(selected.end(), actual, toBeSpliced);
            }
            else {
                non.splice(non.end(), actual, toBeSpliced);
            }
        }

        selectedBoxesCount += selected.size();
        nonSelectedBoxesCount += non.size();

        assert(actual.size() == 0);
        // reduce dimension, actual will carry the new sign covering
        fillFaces(selected, non, selectedIndex, actual);


        assert(checkDimensions(actual, boundaryDimension - 1));

        // Update accumluator; note that selectedIndex is from 0 to d (instead from 1 to d + 1
        // and so we must compute -1^(selectedIndex) instead of -1^(selectedIndex + 1)
        acc = acc * utility::minusOnePower(selectedIndex) * selectedSign;
    }
}

}
}
