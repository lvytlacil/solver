#ifndef PARALLELDEGREECOMPUTATION_H
#define PARALLELDEGREECOMPUTATION_H
#include "idegreecomputation.h"

namespace vytlalib {

namespace degree {

// Forward declarations
class FillFacesParallelWorker;

/**
 * @brief Encapsulates the interface for the combinatorial phase of the
 * topological degree computation. This is a implementation, that
 * processes the L_sel set in each recursion step in parallel.
 */
class ParallelDegree : public Degree
{
    // Class to encapsulate parallel computation
    friend class FillFacesParallelWorker;
private:
    int workThreads_;


protected:
    /**
     * @brief Performs the dimension reduction phase for the degree computation algorithm,
     * by processing the selBoxes in parallel using threads.
     * @see DegreeComputation::fillFaces(boxes::SignCoveringList&, const boxes::SignCoveringList&,
     * int, boxes::SignCoveringList&)
     */
    void fillFaces(boxes::SignCoveringList &selBoxes,
                   const boxes::SignCoveringList &nonBoxes, int signIndex,
                   boxes::SignCoveringList &result) const;

public:
    /**
     * @brief Create an instance for executing the combinatorial phase using the given
     * number of work threads.
     *
     * The work threads not created until the computation starts and will be automatically terminated,
     * after the computation ends. One instance can be used repeatedly to perform computations.
     * @param workThreads The number of work threads to use.
     * @pre workThreads >= 1
     */
    ParallelDegree(int workThreads)
        :workThreads_(workThreads) { }

    /**
     * @brief Returns the number of work threads this instance will use during computation.
     * @return
     */
    int getWorkThreads() const {
        return workThreads_;
    }

    /**
     * @brief @see DegreeComputation::deg(const boxes::SignCoveringList&,
     * const SignSelectionStrategy&).
     */
    int deg(const boxes::SignCoveringList &covering, const ISignIndexSelectionStrategy &strategy) const override;

    /**
     * @brief @see DegreeComputation::deg(const boxes::SignCoveringList&).
     */
    int deg(const boxes::SignCoveringList &covering) const override {
        return deg(covering, DefaultSelectionStrategy());
    }
};

}

}
#endif // PARALLELDEGREECOMPUTATION_H
