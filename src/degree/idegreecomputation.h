#ifndef IDEGREECOMPUTATION_H
#define IDEGREECOMPUTATION_H
#include <list>
#include <memory>
#include <utility>
#include "degreecomputationfwd.h"
#include "../box/boxtypedefs.h"
#include "../bisector/bisectorfwd.h"
#include "../function/functionfwd.h"
#include "../utility/utility.h"

namespace vytlalib {

namespace degree {

/*------- Typedefs -------*/

/**
 * @brief Represents the strategy for selecting the sign and index during
 * the combinatorial phase of topological degree computation.
 */
class ISignIndexSelectionStrategy {
public:
    virtual std::pair<int, boxes::Sign> selectSign(
            const boxes::SignCoveringList &covering) const = 0;
};

/**
 * @brief Represents the strategy for selecting the sign and index during
 * the combinatorial phase of topological degree computation.
 *
 * This strategy simply selects (0, +) every time.
 */
class DefaultSelectionStrategy : public ISignIndexSelectionStrategy
{
public:
    /**
     * @brief Selects the sign and index for the combinatorial phase, by simply
     * setting the index to 0 and the sign to PLUS.
     * @param The sign covering to be the input of the combinatorial phase.
     * @return A pair of the selected index and sign, (0, PLUS).
     * @pre The input sign covering must not be empty.
     */
    std::pair<int, boxes::Sign> selectSign(
            const boxes::SignCoveringList &covering) const override;
};

/**
 * @brief Represents the least frequent selection strategy for selecting
 * the sign and index during the combinatorial phase of topological degree computation.
 *
 * This strategy selects the least frequent combination of the sign and index
 * in the given sign covering.
 */
class LeastFrequentSelectionStrategy : public ISignIndexSelectionStrategy {
public:
    /**
     * @brief Selects the least frequent combination of the sign and index
     * in the given sign covering.
     * @param The sign covering to be the input of the combinatorial phase.
     * @return A pair of the least frequent pair of index and sign.
     * If more pairs have the same lowest frequency of appereances, then
     * the one with lower index is returned and if they have the same index, the
     * one with PLUS is returned.
     * @pre The input sign covering must not be empty.
     */
    std::pair<int, boxes::Sign> selectSign(
            const std::list<boxes::BoxSignVectorPair> &covering) const override;
};

/**
 * @brief Used by specializations of IDegreeComputation to store the number of
 * selected boxes from the last call of compute of IDegreeComputation.
 * This servers just as an information/debug variable to us.
 */
extern long selectedBoxesCount;

/**
 * @brief Used by specializations of IDegreeComputation to store the number of
 * non-selected boxes from the last call of compute of IDegreeComputation.
 * This servers just as an information/debug variable to us.
 */
extern long nonSelectedBoxesCount;

/*------- IDegreeComputation -------*/

/**
 * @brief Encapsulates the interface for the combinatorial phase of the
 * topological degree computation.
 */
class IDegree {
public:

    /**
     * @brief Performs the combinatorial phase with the given input SignCovering.
     * and the given SignSelectionStrategy.
     * @param covering The input sufficient SignCovering.
     * @param strategy The SignSelectionStrategy to be used.
     * @return The result of the combinatorial phase with @p covering as the
     * input SignCovering.
     */
    virtual int deg(const boxes::SignCoveringList &covering,
                        const ISignIndexSelectionStrategy &strategy) const = 0;

    /**
     * @brief Performs the combinatorial phase with the given input SignCovering.
     * and default SignSelectionStrategy.
     * @param covering The input sufficient SignCovering.
     * @return The result of the combinatorial phase with @p covering as
     * the input SignCovering.
     */
    virtual int deg(const boxes::SignCoveringList &covering) const = 0;
};

/**
 * @brief Encapsulates the interface for the combinatorial phase of the
 * topological degree computation. This is a sequential implementation.
 */
class Degree : public IDegree
{
protected:
    // For assertions
    bool checkDimensions(const boxes::SignCoveringList &list, int dimension) const;

    /**
     * @brief Performs the computation at the bottom level of recursion in the combinatorial phase.
     *
     * This method was made protected for easier unit testing, but should otherwise be private.
     * @param covering The sign covering of a set of 0-boxes.
     * @param acc Sign accumulated so far during the combinatorial part (recursion).
     * @return The result of the combinatorial phase.
     */
    int compute(const boxes::SignCoveringList &covering, boxes::Sign acc) const;

    /**
     * @brief Multiplies the given Sign with the given BoxOrientation returning an int result.
     * @param left Sign parameter.
     * @param right BoxOrientation parameter.
     * @return If left is ZERO, then 0.
     * If left is PLUS and right is POSITIVE, or left is MINUS and right is NEGATIVE, then 1.
     * Otherwise -1.
     */
    int mul(const boxes::Sign &left, const boxes::BoxOrientation &right) const;

    /**
     * @brief Determines whether the given box of dimension n-1 has a n-1-dimensional intersection
     * with the given nonBox of dimension n.
     *
     * This is a helper method to determine, whether a box b from the set bounds needs
     * to be split.
     * This method was made protected for easier unit testing, but should otherwise be private.
     * @param face n-1 dimensional box.
     * @param nonBox n dimensional box.
     * @param index (out) if given boxes does not have n-1 dimensional intersection, then undefined.
     * If face is n-1 dimensional subset of nonBox, then -1.
     * Otherwise, the index of the first component of face, that non-trivially intersects
     * corresponding nonBox component, but is not its subset.
     * @param step (out) if given boxes does not have n-1 dimensional intersection or face
     * is subset of nonBox, then undefined.
     * Otherwise, one of the endpoints of intersection in the index-th dimension.
     * @return True if the given boxes have n-1 dimensional intersection, otherwise false.
     * @pre dimension of nonBox = dimension of face + 1
     */
    bool determineBoundaryRelationship(const boxes::Box &face, const boxes::Box &nonBox,
                                              int &index, double &step) const;

    /**
     * @brief Performs the dimension reduction phase for the degree computation algorithm.
     *
     * This encapsulates the proces of creating the set bounds from L_sel
     * and splitting boxes in bounds until each box b is either a subset of some box
     * from L_non or the dimension of its intersection with every box from L_non has
     * dimension lesser that b.
     * This method was made protected for easier unit testing, but should otherwise be private.
     *
     * @param selectedFaces (modified) List of selected boxes L_sel. (dim m)
     * @param non List of non selected boxes L_non. (dim m)
     * @param signIndex Index in the sign vector to be ommited in the reduced dimension.
     * @param result (out) Oriented boundary with reduced dimension (dim m - 1)
     */
    void fillFaces(boxes::SignCoveringList &selectedFaces, const boxes::SignCoveringList &non,
                   int signIndex,
                   boxes::SignCoveringList &result) const;

public:
    Degree() { }

    // See parent
    int deg(const boxes::SignCoveringList &covering, const ISignIndexSelectionStrategy &strategy) const override;

    // See parent
    int deg(const boxes::SignCoveringList &covering) const override {
        return deg(covering, DefaultSelectionStrategy());
    }
};



}

}
#endif // IDEGREECOMPUTATION_H
