#include <iostream>
#include <thread>
#include <mutex>
#include "idegreecomputation.h"
#include "../function/function.h"
#include "../bisector/bisector.h"
#include "../box/box.h"
#include "../message/messageprocessor.h"

namespace vytlalib {

using function::IFunction;
using bisector::IBisector;
using bisector::Bisector;
using bisector::BisectionResult;
using boxes::Component;
using boxes::Box;
using boxes::BoxList;
using boxes::OrientedBox;
using boxes::BoxOrientation;
using boxes::Sign;
using boxes::SignVector;
using boxes::BoxSignVectorPair;
using boxes::SignCoveringList;

namespace degree {

long selectedBoxesCount = 0;
long nonSelectedBoxesCount = 0;

/*------- Degree Computation -------*/
using namespace std;
bool Degree::checkDimensions(const SignCoveringList &list, int dimension) const {
    if (list.empty())
        return true;
    for (const auto& item : list) {
        if (dimension != item.first.getTopologicalDimension() ||
            dimension + 1 != item.second.getSize())
            return false;
    }
    return true;
}

int Degree::compute(const SignCoveringList &covering, Sign acc) const {
    vytlassert(acc != Sign::ZERO, "Sign must be non-ZERO");
    int sum = 0;
    for (const auto& elem : covering) {
        vytlassert(elem.second.getSize() == 1, "Sign vector must be of size 1");
        sum += mul(elem.second[0], elem.first.getOrientation());
    }
    vytlassert(sum % 2 == 0, "The sum must be even");
    return acc == Sign::PLUS ? sum / 2 : -sum / 2;
}

int Degree::mul(const Sign &left, const BoxOrientation &right) const {
    switch (left) {
    case Sign::PLUS:
        return right == BoxOrientation::POSITIVE ? 1 : -1;
    case Sign::MINUS:
        return right == BoxOrientation::POSITIVE ? -1 : 1;
    default:
        return 0;
    }
}

bool Degree::determineBoundaryRelationship(const Box &face, const Box &nonBox,
                                                      int &index, double &step) const {
    assert(nonBox.getSize() == face.getSize());
    assert(nonBox.getTopologicalDimension() == face.getTopologicalDimension() + 1);
    index = -1; // assume either trivial intersection or that boundary is a subset of nonbox
    const int size = face.getSize();
    // Scan all components of faces

    for (int i = size - 1; i >= 0; --i) {

        const double selLo = face.components_[i].interval_.left();
        const double selUp = face.components_[i].interval_.right();
        const double nonLo = nonBox.components_[i].interval_.left();
        const double nonUp = nonBox.components_[i].interval_.right();

        // max of (selLo, nonLo) utility::max(selLo, nonLo);
        const double lo = nonLo > selLo ? nonLo : selLo;
        // min of (selUp, nonUp) utility::min(selUp, nonUp);
        const double hi = selUp < nonUp ? selUp : nonUp;
        if (lo > hi || (lo == hi && selLo < selUp)) {
            // Boxes have at most n-2-dimensional intersection            
            return false;
        } else if (selLo < nonLo) {
            // Save index and step, face is not a subset of nonBox
            index = i;
            step = nonLo;
            assert(selLo < step && step < selUp);
        } else if (selUp > nonUp) {
            index = i;
            step = nonUp;
            assert(selLo < step && step < selUp);
        }
    }
    // Boxes have n-1-dimensional intersection
    return true;
}

void Degree::fillFaces(boxes::SignCoveringList &selectedFaces, const SignCoveringList &non, int signIndex,
                                  SignCoveringList &result) const {
    for (BoxSignVectorPair& boxSignVectorPair : selectedFaces) {
        auto boundary = boxSignVectorPair.first.computeInducedBoundary();

        for (Box &selFace : boundary) {
            // Test intersection with each elem of non
            for (const auto& elem : non) {

                const Box &nonBox = elem.first;
                int intersectionIndex = -1;
                double step = 0.0;

                if (determineBoundaryRelationship(selFace, nonBox, intersectionIndex, step)) {
                    // n-1-dim intersection; index and step have been written to by the method call
                    if (intersectionIndex == -1) {
                        // selFace is subset of nonBox, push to result and continue with checking next box
                        SignVector projected = elem.second;
                        vytlassert(0 <= signIndex && signIndex < projected.getSize(),
                                   "signIndex must be non-negative and not greater than the size of the"
                                   " sign vector");
                        projected.removeAt(signIndex);
                        result.push_back(BoxSignVectorPair(std::move(selFace), projected));
                        break;
                    } else {                        
                        // split selFace and make sure sub-boxes will be examined
                        const Component& bisected = selFace.getComp(intersectionIndex);
                        auto left = Box(selFace);
                        auto right = Box(selFace);
                        left.setComp(intersectionIndex, Component(bisected.lower(), step));
                        right.setComp(intersectionIndex, Component(step, bisected.upper()));
                        boundary.push_back(std::move(left));
                        boundary.push_back(std::move(right));
                        break;
                    }
                } // else intersection has dim < n-1, do not save such boundaryBox
            }
        }
    }
}

int Degree::deg(const SignCoveringList &covering, const ISignIndexSelectionStrategy &strategy) const {
    selectedBoxesCount = 0;
    nonSelectedBoxesCount = 0;

    vytlassert(boxes::isSignCoveringListDetermined(covering), "covering must be sufficient");
    Sign acc = Sign::PLUS;

    SignCoveringList actual;
    for (const auto &it : covering) {
        actual.push_back(BoxSignVectorPair(Box(it.first), it.second));
    }

    while (true) {        
        if (actual.size() == 0)
           return 0;

        int dim = actual.front().first.getTopologicalDimension();
        vytlassert(dim >= 0, "Dimension of the sign covering must be greater than zero.");
        vytlassert(checkDimensions(actual, dim),
                   "All boxes in the covering must be of the same dimension." );
        if (dim == 0) {
            return compute(actual, acc);
        }

        // determine the sign index and sign
        auto signSelection = strategy.selectSign(actual);
        int selectedIndex = signSelection.first;
        Sign selectedSign = signSelection.second;

        // create and fill lists of selected and non selected boxes
        std::list<BoxSignVectorPair> selected;
        std::list<BoxSignVectorPair> non;
        auto it = actual.begin();
        while (it != actual.end()) {
            auto toBeSpliced = it;
            ++it;
            // pair of oriented box and a sign list
            const auto& boxSignListPair = *toBeSpliced;
            if (selectedSign == boxSignListPair.second[selectedIndex]) {
                selected.splice(selected.end(), actual, toBeSpliced);
            }
            else {
                non.splice(non.end(), actual, toBeSpliced);
            }
        }

        selectedBoxesCount += selected.size();
        nonSelectedBoxesCount += non.size();

        assert(actual.size() == 0);

        // reduce dimension
        fillFaces(selected, non, selectedIndex, actual);

        assert(checkDimensions(actual, dim - 1));

        // Update accumluator; note that selectedIndex is from 0 to d (instead from 1 to d + 1
        // as in), and so we compute -1^(selectedIndex) instead of -1^(selectedIndex + 1)
        acc = acc * utility::minusOnePower(selectedIndex) * selectedSign;
    }
}

/*------ SignSelectionStrategy -------*/

std::pair<int, boxes::Sign> DefaultSelectionStrategy::selectSign(const std::list<BoxSignVectorPair> &covering) const {
    if (covering.empty())
        throw message::ProgramException("Input sign covering is empty");
    return std::make_pair(0, Sign::PLUS);
}
using namespace std;
std::pair<int, boxes::Sign> LeastFrequentSelectionStrategy::selectSign(const std::list<BoxSignVectorPair> &covering) const {
    if (covering.empty())
        throw message::ProgramException("Input sign covering is empty");


    int svDim = covering.front().second.getSize();
    // size of the array is double the dimension of sign vectors
    // and stored elements are counters for (index, sign):
    // (0, +), (0, -), (1, +), (1, -), ...
    int counterSize = 2 * svDim;
    std::unique_ptr<long[]> counter(new long[counterSize] {0});

    for (const auto &boxSignListPair : covering) {
        const SignVector &sv = boxSignListPair.second;
        vytlassert (sv.getSize() == svDim, "All encountered sign vectors must have the same size");
        for (int i = 0; i < svDim; ++i) {
            if (sv[i] == Sign::PLUS) {
                ++counter[2 * i];
            } else if (sv[i] == Sign::MINUS) {
                ++counter[2 * i + 1];
            }
        }
    }

    // Find index with the lowest count
    int index = 0;
    for (int i = 1; i < counterSize; ++i) {
        if (counter[i] < counter[index]) {
            index = i;
        }
    }

    return std::make_pair(index / 2, index % 2 == 0 ? Sign::PLUS : Sign::MINUS);
}

}

}
