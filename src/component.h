#ifndef BOUNDARY_H
#define BOUNDARY_H
#include <iosfwd>
#include <cassert>
#include <gaol/gaol.h>
#include "degree/degreecomputationfwd.h"

namespace vytlalib {

namespace evaluator {
    class StandardEvaluator;
}

namespace boxes {

/*------- Component -------*/

/**
 * @brief Represents a component of a box, i.e. closed interval [a, b], a <= b, that is mutable
 * and allows reliable computation using the underlying interval arithmetics library.
 */
class Component {
    friend class evaluator::StandardEvaluator;
    friend class degree::Degree;
private:
    // The actual stored interval.
    gaol::interval interval_;
    // Constructs a component from the given gaol interval.
    Component(gaol::interval interval):
        interval_(interval) { }

public:
    /**
     * @brief Constructs a new component [low, high].
     * @param low The lower endpoint.
     * @param high The upper endpoint.
     */
    Component(double low, double high);

    /**
     * @brief Constructs a new degenerated component [value, value].
     * @param value Value to use and both lower and upper endpoint.
     */
    Component(double value):
        Component(value, value) { }

    /**
     * @brief Creates a degenerated component [0, 0].
     */
    Component():
        interval_(gaol::interval("0")) { }

    /**
     * @brief Creates a component by parsing the given string.
     *
     * Accepted string formats depend on the underlying interval arithmetics library.
     * You can, however, always use strings of the form "[NUM, NUM]", "[NUM]", or "NUM"
     * where NUM stands for a string representing a floating-point number.
     * @param cstr String to be parsed into interval.
     */
    Component(const char* const cstr):
        interval_(cstr) { }

    /**
     * @brief Returns a Component enclosing pi.
     * @return Component containing pi.
     */
    static Component create_pi() {
        return Component(gaol::interval::pi());
    }

    /**
     * @brief Returns a Component enclosing e.
     * @return
     */
    static Component create_e() {
        return Component(gaol::exp(1));
    }

    /**
     * @brief Performs the initialization of the underlying interval arithmetics library,
     * like setting up trusted rounding mode etc.
     */
    static void init() {
        gaol::init();
    }

    /**
     * @brief Performs the clean-up of the underlying interval arithmetics library.
     */
    static void cleanup() {
        gaol::cleanup();
    }

    /**
     * @brief Determines, wheter this component equals given other component by comparing
     * their respective endpoints.
     * @param other Component this instance will be compared to.
     * @return True if both lower and upper bounds of compared instances are equal
     * to each other. Otherwise false.
     */
    bool operator==(const Component &other) const {
        return interval_.left() == other.interval_.left() && interval_.right() == other.interval_.right();
    }

    /**
     * @brief Returns the negation of this == other.
     * @param other Component this instance will be compared to.
     * @return True, if this == other is false. Otherwise false.
     */
    bool operator!=(const Component &other) const {
        return !((*this) == other);
    }

    /**
     * @brief Determines, whether this component is degenerated.
     * @return True if this component is degenerated (i.e. has the form [a, a]), otherwise false.
     */
    bool isConstant() const {
        return interval_.left() == interval_.right();
    }

    /**
     * @brief Determines, whether this component is degenerated interval enclosing an integer.
     * @return True, if this component is degenerated and encloses an integer.
     */
    bool isConstantInt() const {
        return interval_.is_an_int();
    }

    /**
     * @brief Returns the lower endpoint of this component.
     * @return Lower endpoint.
     */
    double lower() const {
        return interval_.left();
    }

    /**
     * @brief Returns the upper endpoint of this component.
     * @return Upper endpoint.
     */
    double upper() const {
        return interval_.right();
    }

    /**
     * @brief Computes the midpoint of this component [a, b], which is a number (a + b) / 2,
     * rounded to nearest.
     * @return Midpoint of this component.
     */
    double midpoint() const {
        return interval_.midpoint();
    }

    /**
     * @brief Computes the step made from the lower endpoint of this component [a, b] as
     * a + distance * (b - a).
     * @param distance Value dictating the magnitude and direction of the step.
     * @return Computed step.
     */
    double step(double distance = 0.5) const {
        return interval_.left() + distance * (width());
    }

    /**
     * @brief Computes a component [x, y] that is guaranteed to enclose the step
     * computed by @link step. i.e. a + distance * (b - a) lies in [x, y].
     * @param distance Value dictating the magnitude and direction of the step.
     * @return Component containing computed step.
     */
    Component stepComponent(double distance = 0.5) const;

    /**
     * @brief Computes a component [x, y] that is guaranteed to enclose the midpoint of
     * this component [a, b], i.e. (a + b) / 2 lies in [x, y].
     * @return Component containing the midpoint of this.
     */
    Component mid() const;

    /**
     * @brief Computes the width of this component [a, b].
     * @return Width computed as b - a.
     */
    double width() const {
        return interval_.width();
    }

    /**
     * @brief Computes a component [x, y] that is guaranteed to enclose the width of
     * this component.
     * @return Component enclosing width of this component.
     */
    Component widthComponent() const {
        return Component(gaol::interval(interval_.right()) - gaol::interval(interval_.left()));
    }

    /**
     * @brief Determines if the given value is contained in this component [a, b].
     * @param value Point value to be tested.
     * @return True if a <= value && value <= b, otherwise false.
     */
    bool contains(double value) const {
        return lower() <= value && value <= upper();
    }

    /**
     * @brief Determines, if this component [a, b] is a subset of the given other component [c, d].
     * @param other
     * @return True, if c <= a <= b <= d. Otherwise false.
     */
    bool isSubsetOf(const Component &other) const {
        return other.contains(lower()) && other.contains(upper());
    }

    /**
     * @brief Sends the given Component instance to the given std::ostream.
     * @param out Stream to accept the Component.
     * @param component Component to send to the std::ostream.
     * @return The same std::ostream passed as argument.
     */
    friend std::ostream& operator<<(std::ostream &out, const Component &component);

    /*--- Immutable operations, that are carried directly on gaol::interval*/

    /**
     * @brief Computes a component enclosing the sum of the two given components.
     * @param left
     * @param right
     * @return
     */
    friend Component operator+(const Component &left, const Component &right) {
        return Component(left.interval_ + right.interval_);
    }

    /**
     * @brief Computes a component enclosing the product of the two given components.
     * @param left
     * @param right
     * @return
     */
    friend Component operator*(const Component &left, const Component &right) {
        return Component(left.interval_ * right.interval_);
    }

    /**
     * @brief Computes a component enclosing the difference of the two given components.
     * @param left
     * @param right
     * @return
     */
    friend Component operator-(const Component &left, const Component &right) {
        return Component(left.interval_ - right.interval_);
    }

    /**
     * @brief Computes a component enclosing the ratio of the two given components.
     * @param left
     * @param right
     * @return
     */
    friend Component operator/(const Component &left, const Component &right) {
        return Component(left.interval_ / right.interval_);
    }

    /*--- Mutable operations, that are carried directly on gaol::interval*/

    /**
     * @brief Adds the given component to this component.
     * @param other
     */
    void operator +=(const Component &other) {
        interval_ += other.interval_;
    }

    /**
     * @brief Subtracts the given component from this component.
     * @param other
     */
    void operator -=(const Component &other) {
        interval_ -= other.interval_;
    }

    /**
     * @brief Multiplies this component by the given component.
     * @param other
     */
    void operator *=(const Component &other) {
        interval_ *= other.interval_;
    }

    /**
     * @brief Divides this component by the given component.
     * @param other
     */
    void operator /=(const Component &other) {
        interval_ /= other.interval_;
    }

    /**
     * @brief Negates this component.
     */
    void neg() {
        interval_ = -interval_;
    }

    /**
     * @brief Raises this component to the given integer power.
     * @param n
     */
    void pow(int n) {
        interval_ = gaol::pow(interval_, n);
    }

    /**
     * @brief Raises this component C to the given other component D
     * by computing exp(D * log(C)).
     * @param other
     */
    void pow(const Component &other) {
        interval_ = gaol::exp(other.interval_ * gaol::log(interval_));
    }

    /**
     * @brief Computes the absolute value of this component and assigns the result to
     * this component.
     */
    void abs() {
        interval_ = gaol::abs(interval_);
    }

    /**
     * @brief Computes the sine of this component and assigns the result to
     * this component.
     */
    void sin() {
        interval_ = gaol::sin(interval_);
    }

    /**
     * @brief Computes the cosine of this component and assigns the result to
     * this component.
     */
    void cos() {
        interval_ = gaol::cos(interval_);
    }

    /**
     * @brief Computes the tangent value of this component and assigns the result to
     * this component.
     */
    void tan() {
        interval_ = gaol::tan(interval_);
    }

    /**
     * @brief Computes the arcsine of this component and assigns the result to
     * this component.
     */
    void asin() {
        interval_ = gaol::asin(interval_);
    }

    /**
     * @brief Computes the arccosine of this component and assigns the result to
     * this component.
     */
    void acos() {
        interval_ = gaol::acos(interval_);
    }

    /**
     * @brief Computes the arctangent of this component and assigns the result to
     * this component.
     */
    void atan() {
        interval_ = gaol::atan(interval_);
    }

    /**
     * @brief Computes the square of this component and assigns the result to
     * this component.
     */
    void sqr() {
        interval_ = gaol::sqr(interval_);
    }

    /**
     * @brief Computes the square root of this component and assigns the result to
     * this component.
     */
    void sqrt() {
        interval_ = gaol::sqrt(interval_);
    }

    /**
     * @brief Computes the n-th root of this component and assigns the result to
     * this component.
     * @param n
     */
    void nthRoot(uint n) {
        if (n % 2 == 0) {
            interval_ = gaol::nth_root(interval_, n);
        } else {
            // required to treat odd root functions as functions defined on the whole R.
            interval_ = gaol::nth_root_rel(interval_, n, gaol::interval::universe());
        }
    }

    /**
     * @brief Computes the exponential of this component and assigns the result to
     * this component.
     */
    void exp() {
        interval_ = gaol::exp(interval_);
    }

    /**
     * @brief Computes the logarithm of this component and assigns the result to
     * this component.
     */
    void log() {
        interval_ = gaol::log(interval_);
    }

    /**
     * @brief Makes this component and empty set.
     */
    void emptySet() {
        interval_ = gaol::interval::emptyset();
    }

    /**
     * @brief Makes this component a component encolsing the pi constant.
     */
    void const_pi() {
        interval_ = gaol::interval::pi();
    }

    /**
     * @brief Makes this component a component encolsing the e constant.
     */
    void const_e() {
        interval_ = gaol::exp(1);
    }
};

}

}

#endif // BOUNDARY_H
