#include <ostream>
#include "component.h"

namespace vytlalib {
namespace boxes {

/*------- Component -------*/

Component::Component(double low, double high) {
    interval_ = gaol::interval(low, high);
}

std::ostream& operator<<(std::ostream &out, const Component &boundary) {
    if (boundary.isConstant()) {
        out << "[" << boundary.lower() << "]";
    } else {
        out << "[" << boundary.lower() << ", " << boundary.upper() << "]";
    }
    return out;
}

Component Component::stepComponent(double distance) const {
    // [a, a] + [distance, distance] * ([b, b] - [a, a])
    gaol::interval a = gaol::interval(interval_.left());
    gaol::interval result = a + gaol::interval(distance) * (gaol::interval(interval_.right()) - a);
    return Component(result);
}

Component Component::mid() const {
    // ([a, a] + [b, b]) / [2, 2]
    gaol::interval result = (gaol::interval(interval_.left()) +
                             gaol::interval(interval_.right())) / gaol::interval("2.0");
    return Component(result);
}

}
}
