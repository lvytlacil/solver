#ifndef UTILITY_H
#define UTILITY_H

#include <memory>

namespace vytlalib {
namespace utility {

template <class T>
const T& max(const T& a, const T& b) {
    return (a < b) ? b : a;
}

template <class T>
const T& min(const T& a, const T& b) {
    return (a < b) ? a : b;
}

inline double step(double a, double b, double distance) {
    return a + (b - a) * distance;
}

inline int minusOnePower(int exp) {
    return (exp % 2 == 0) ? 1 : -1;
}

}
}
#endif // UTILITY_H
