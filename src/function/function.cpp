#include "function.h"
#include "../bisector/bisector.h"
#include "functionevaluator.h"
#include "../parser/ast.h"
#include "../parser/parser.h"
#include <iostream>
#include <sstream>
#include <ctime>

namespace vytlalib {

using boxes::SignCoveringList;
using boxes::SignVector;
using boxes::BoxList;
using parser::FunctionParser;
using evaluator::StandardEvaluator;
using evaluator::FunctionEvaluator;
using evaluator::CachedEvaluator;
using evaluator::ByteCodeEvaluator;
using bisector::IBisector;
using bisector::Bisector;
using ast::BitField64;
using namespace ast;

namespace function {

/*----------------IFunction-------------------*/

std::ostream& operator <<(std::ostream &out, IFunction &f) {
    f.toStream(out);
    return out;
}

void IFunction::toStream(std::ostream &out) {
    out << "IFunction";
}

UPtrIFunction IFunction::parseRecursive(std::string in) {
    return Function::parse(in);
}

UPtrIFunction IFunction::parseRecursive(std::istream &in) {
    return Function::parse(in);
}

UPtrIFunction IFunction::parseLinear(std::string in) {
    return ByteCodeFunction::parse(in);
}

UPtrIFunction IFunction::parseLinear(std::istream &in) {
    return ByteCodeFunction::parse(in);
}

UPtrIFunction IFunction::parseLinearCached(std::string in) {
    return ByteCodeFunction::parseCached(in);
}

UPtrIFunction IFunction::parseLinearCached(std::istream &in) {
    return ByteCodeFunction::parseCached(in);
}

SignCoveringList IFunction::computeSignCovering(Box box, const Box &paramBox,
                                         const IBisector &bisector) {
    SignCoveringList signCoveringList;
    Box range = eval(box, paramBox);
    SignVector signVector = range.computeSignVector();
    signCoveringList.emplace_back(std::move(box), std::move(signVector));

    // Refine sign coverings
    auto it = signCoveringList.begin();
    while (it != signCoveringList.end()) {

        // If the box's sign list is not determined, split it into two subboxes,
        // evaluate each and push it to the list, then delete the original
        const SignVector &signList = it->second;
        if (!signList.isSufficient()) {
            auto& domainBox = it->first;

            // Bisect and check result
            auto result = domainBox.bisectBy(bisector);
            if (result == boxes::EMPTY_BOX) // Terminate, if stop condition reached
                break;

            // Evaluate splitted boxes, update result list
            it->second = eval(domainBox, paramBox).computeSignVector();
            SignVector rightSignList = eval(result, paramBox).computeSignVector();
            it = signCoveringList.emplace(it, std::move(result), std::move(rightSignList));
        } else
            ++it;
    }
    return signCoveringList;
}

SignCoveringList IFunction::computeSignCovering(BoxList boxList, const Box &paramBox,
                                         const IBisector &bisector) {
    SignCoveringList signCoveringList;
    for (Box& box : boxList) {
        Box range = eval(box, paramBox);
        SignVector signVector = range.computeSignVector();
        signCoveringList.emplace_back(std::move(box), std::move(signVector));
    }

    // Refine sign coverings
    auto it = signCoveringList.begin();
    while (it != signCoveringList.end()) {

        // If the box's sign list is not determined, split it into two subboxes,
        // evaluate each and push it to the list, then delete the original
        const SignVector &signList = it->second;
        if (!signList.isSufficient()) {
            auto& domainBox = it->first;

            // Bisect and check result
            auto result = domainBox.bisectBy(bisector);
            if (result == boxes::EMPTY_BOX) // Terminate, if stop condition reached
                break;

            // Evaluate splitted boxes, update result list
            it->second = eval(domainBox, paramBox).computeSignVector();
            SignVector rightSignList = eval(result, paramBox).computeSignVector();
            it = signCoveringList.emplace(it, std::move(result), std::move(rightSignList));
        } else
            ++it;
    }
    return signCoveringList;
}

static Bisector bis(0);


SignCoveringList IFunction::computeBoundarySignCovering(const Box& box, const Box &paramBox,
    const IBisector &bisector) {

    BoxList boxList = box.computeInducedBoundary();
    SignCoveringList signCoveringList;
    for (Box& face : boxList) {
        Box range = eval(face, paramBox);
        SignVector signVector = range.computeSignVector();
        signCoveringList.emplace_back(std::move(face), std::move(signVector));
    }

    // Refine sign coverings
    auto it = signCoveringList.begin();
    while (it != signCoveringList.end()) {
        // If the box's sign list is not determined, split it into two subboxes,
        // evaluate each and push it to the list, then delete the original
        const SignVector &signList = it->second;
        if (!signList.isSufficient()) {
            auto& domainBox = it->first;

            // Bisect and check result

            auto br = bisector.bisect(domainBox);
            if (br.index <= -1)
                break;

            auto result = domainBox.bisectBy(bisector);

            // Evaluate splitted boxes, update result list
            it->second = eval(domainBox, paramBox).computeSignVector();
            SignVector rightSignList = eval(result, paramBox).computeSignVector();
            it = signCoveringList.emplace(it, std::move(result), std::move(rightSignList));
        } else
            ++it;
    }

    return signCoveringList;
}


/*------------ Function ------------*/

void Function::toStream(std::ostream &out) {
    out << *root_;
}

Function::~Function() {    }

Function* Function::performCloning() const {
    TreeDuplicator dpl;
    Function* result = new Function(dpl.duplicate(root_.get()));
    result->evaluator_.reset(new StandardEvaluator(*result));
    return result;
}

UPtrFunction Function::parse(std::string in) {
    std::stringstream inStream(in);
    return parse(inStream);
}

UPtrFunction Function::parse(std::istream &in) {
    FunctionParser parser;
    UPtrFunction result(new Function(parser.parse(in)));
    result->evaluator_.reset(new StandardEvaluator(*result));
    return result;
}

Function::Function(std::shared_ptr<FunctionDefinition> root):
    root_(root) { }

Box Function::evalBoxExpression(std::string in) {
    std::stringstream inStream(in);
    return evalBoxExpression(inStream);
}

Box Function::evalBoxExpression(std::istream &in) {
    FunctionParser parser;
    Function result = Function(parser.parseBoxExpression(in));
    result.evaluator_.reset(new StandardEvaluator(result));
    return result.eval(boxes::EMPTY_BOX);
}



int Function::getDomainDim() {
    return root_->argList->argSize;
}

int Function::getImageDim() {
    return root_->retStatement->components.size();
}

Box Function::eval(const boxes::Box &domainBox) {
    return evaluator_->eval(domainBox);
}

Box Function::eval(const boxes::Box &domainBox, const boxes::Box &paramBox) {
    return evaluator_->eval(domainBox, paramBox);
}

bool Function::operator ==(const Function &other) {
    return root_ == other.root_;
}

bool Function::operator !=(const Function &other) {
    return root_ != other.root_;
}



/*----------------- ByteCodeFunction ------------------------*/

void ByteCodeFunction::toStream(std::ostream &out) {
    for (const auto& node : byteCode_) {
        out << node << std::endl;
    }
}

ByteCodeFunction* ByteCodeFunction::performCloning() const {
    TreeDuplicator dpl;
    ByteCodeFunction* result = new ByteCodeFunction(dpl.duplicate(root_.get()));
    result->evaluator_.reset(new ByteCodeEvaluator(*result));
    return result;
}

ByteCodeFunction::ByteCodeFunction(std::shared_ptr<FunctionDefinition> root):
    root_(root ) {
    domainDim_ = root_->argList->argSize;
    imageDim_ = root_->retStatement->components.size();

    TreePostOrderLinearizer lin;
    byteCode_ = lin.linearize(root_.get());
}

UPtrByteCodeFunction ByteCodeFunction::parse(std::string in) {
    std::stringstream ss(in);
    return parseCached(in);
}

UPtrByteCodeFunction ByteCodeFunction::parse(std::istream &in) {
    FunctionParser parser;
    UPtrByteCodeFunction result(new ByteCodeFunction(parser.parse(in)));
    result->evaluator_.reset(new ByteCodeEvaluator(*result));
    return result;
}

UPtrByteCodeFunction ByteCodeFunction::parseCached(std::string in) {
    std::stringstream ss(in);
    return parseCached(ss);
}

UPtrByteCodeFunction ByteCodeFunction::parseCached(std::istream &in) {
    FunctionParser parser;
    UPtrByteCodeFunction result(new ByteCodeFunction(parser.parse(in)));
    result->evaluator_.reset(new CachedEvaluator(*result));
    return result;
}

ByteCodeFunction::~ByteCodeFunction() { }

Box ByteCodeFunction::eval(const Box &domainBox, const Box &paramBox) {
    return evaluator_->eval(domainBox, paramBox);
}

Box ByteCodeFunction::eval(const Box &domainBox) {
    return evaluator_->eval(domainBox);
}

}
}
