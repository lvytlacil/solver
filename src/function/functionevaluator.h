#ifndef FUNCTIONEVALUATOR_H
#define FUNCTIONEVALUATOR_H
#include <ctime>
#include <map>
#include "../box/box.h"
#include "../parser/ast.h"
#include "../message/messageprocessor.h"

namespace vytlalib {

namespace function {
    class Function;
    class IByteCodeCommon;
    class ByteCodeFunction;
}

namespace evaluator {

/**
 * @brief Represents the environment of the interval function implementation,
 * which is simply the actual values of variables during a certain evaluation.
 * This class is not meant to be used outside of IFunction and
 * should become inner class of it in the future.
 */
class Environment {
private:
    int capacity_;
    boxes::Component* data_;
public:
    Environment(int capacity):
        capacity_(capacity), data_(new boxes::Component[capacity]) { }

    ~Environment() {
        delete [] data_;
    }

    boxes::Component& operator[](int index) {
        vytlassert(0 <= index && index < capacity_, "Index cannot be outside of the range.");
        return data_[index];
    }
};

/**
 * @brief Represents the classic AST interpreter for interval inclusion
 * function F, that evaluates the expressions F(B) by recursively
 * walking the AST.
 * This class is not meant to be used outside of IFunction and
 * should become inner class of it in the future.
 */
class StandardEvaluator : public ast::AstVisitor
{
protected:

    // Stored function
    function::Function& storedF_;
    // Environment
    Environment env_;
    // Intermediate evaluations; stored by value to bypass the constant wrapping
    boxes::Component intermediate_;
    /* Resulting box; NOTE: Instead of having intermediate_ and result_ separately,
     * we could introduce a new virtual runtime value type, but it would be overkill for this case */
    boxes::Box result_;

public:

    StandardEvaluator(function::Function &f);


    boxes::Box eval(const boxes::Box &domain) {
        return eval(domain, boxes::EMPTY_BOX);
    }
    boxes::Box eval(const boxes::Box &varDomain,
                               const boxes::Box &parDomain);

    virtual void visit(ast::Node * node) override;
    virtual void visit(ast::FormalArgumentList * node) override;
    virtual void visit(ast::Expression * node) override;
    virtual void visit(ast::ReturnStatement * node) override;
    virtual void visit(ast::FunctionDefinition * node) override;
    virtual void visit(ast::Number * node) override;
    virtual void visit(ast::NumberInterval * node) override;
    virtual void visit(ast::Identifier * node) override;
    virtual void visit(ast::BinOp * node) override;
    virtual void visit(ast::UnaryFunc * node) override;
    virtual void visit(ast::UnaryFuncWithIntArg * node) override;
    virtual void visit(ast::NullaryFunc * node) override;
    virtual void visit(ast::UnaryMinus * node) override;
};

/**
 * @brief Represents an interpreter of for interval inclusion
 * function F, that computes the F(B) expressions by
 * evaluating the precomuted linearization of the function AST.
 * This class is not meant to be used outside of IFunction and
 * should become inner class of it in the future.
 */
class ByteCodeEvaluator {
protected:
    // Stored function
    function::IByteCodeCommon& storedF_;


    // Environment
    Environment env_ = Environment(64);
    // Resulting box
    boxes::Box result_;

public:

    ByteCodeEvaluator(function::IByteCodeCommon &f);

    virtual boxes::Box eval(const boxes::Box &domain) {
        return eval(domain, boxes::EMPTY_BOX);
    }

    virtual boxes::Box eval(const boxes::Box &varDomain, const boxes::Box &parDomain);
};

/**
 * @brief Represents an interpreter of for interval inclusion
 * function F, that computes the F(B) expressions by
 * evaluating the precomuted linearization of the function AST and
 * caches the input as well as the intermediate values,
 * which it then aims to reuse in subsequent evaluations.
 * This class is not meant to be used outside of IFunction and
 * should become inner class of it in the future.
 */
class CachedEvaluator : public ByteCodeEvaluator {
private:
    ast::BitField64 lastInput;
public:
    CachedEvaluator(function::IByteCodeCommon &f);

    ast::BitField64 getLastInput() {
        return lastInput;
    }

    boxes::Box eval(const boxes::Box &varDomain) override {
        return eval(varDomain, boxes::EMPTY_BOX);
    }

    boxes::Box eval(const boxes::Box &varDomain, const boxes::Box &parDomain) override;

    boxes::Box eval(ast::BitField64 matchedVars, const boxes::Box &domain) {
        return eval(matchedVars, domain, boxes::EMPTY_BOX);
    }

    boxes::Box eval(ast::BitField64& matchedVars, const boxes::Box &varDomain,
                               const boxes::Box &parDomain);
};

}
}

#endif // FUNCTIONEVALUATOR_H
