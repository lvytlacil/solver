#ifndef FUNCTIONFWD_H
#define FUNCTIONFWD_H
#include <memory>

namespace vytlalib {
namespace function {

class IFunction;
class Function;
class ByteCodeFunction;
class CustomCachedFunction;

/**
 * @brief An unique pointer to IFunction.
 */
typedef std::unique_ptr<IFunction> UPtrIFunction;

/**
 * @brief An unique pointer to Function.
 */
typedef std::unique_ptr<Function> UPtrFunction;

/**
 * @brief An unique pointer to ByteCodeFunction.
 */
typedef std::unique_ptr<ByteCodeFunction> UPtrByteCodeFunction;

}
}
#endif // FUNCTIONFWD_H
