#ifndef INTERPRETER_H
#define INTERPRETER_H

#include <ctime>
#include <string>
#include <memory>
#include "functionfwd.h"
#include "../bisector/bisectorfwd.h"
#include "../box/box.h"
#include "../parser/ast.h"


namespace vytlalib {
using boxes::Box;
using boxes::Component;

namespace parser {
    class FunctionParser;
}

namespace evaluator {
    class FunctionEvaluator;
    class StandardEvaluator;
    class CachedEvaluator;
    class ByteCodeEvaluator;
}

namespace function {

/**
 * @brief Provides an algorithmical implementation of an interval
 * inclusion function for a punctual function f: R(n_1) --> R(n_2), possibly
 * parametrized by some sub-box P of R_m.
 */
class IFunction {
private:
    // Performs the cloning
    virtual IFunction* performCloning() const = 0;

public:

    /**
     * @brief Returns the dimension of the domain.
     * @return
     */
    virtual int getDomainDim() = 0;

    /**
     * @brief Returns the dimension of the image.
     * @return
     */
    virtual int getImageDim() = 0;

    /**
     * @brief Computes F(B), passing an empty box as the parameter sub-box.
     * @param domainBox The input variable domain box.
     * @return The box F(B) where B is @p domainBox.
     */
    virtual boxes::Box eval(const boxes::Box &domainBox) = 0;

    /**
     * @brief Computes F(B, P), with the given parameter sub-box P.
     * @param domainBox The input variable domain box.
     * @param paramBox The input parameter domain box.
     * @return The box F(B, P) where B is @p domainBox and P is @p paramBox.
     */
    virtual boxes::Box eval(const boxes::Box &domainBox, const boxes::Box &paramBox) = 0;

    /**
     * @brief Behaves as if a list of boxes containing the passed box and no
     * other element would be passed (along with the given parameter box and bisector)
     * to computeSignCovering(boxes::BoxList, boxes::Box, bisector::IBisector)
     */
    boxes::SignCoveringList computeSignCovering(boxes::Box box, const boxes::Box &paramBox,
                                         const bisector::IBisector &bisector);

    /**
     * @brief Creates a list of all oriented faces of the given box and then behaves
     * as if this list (along with the given parameter box and bisector) would be passed
     * to computeSignCovering(boxes::BoxList, boxes::Box, bisector::IBisector)
     */
    boxes::SignCoveringList computeBoundarySignCovering(const boxes::Box& box, const boxes::Box &paramBox,
                                         const bisector::IBisector &bisector);

    /**
     * @brief Attempts to compute a sufficient sign covering wrt. this function of the
     * given list of boxes or a refined set based on it.
     *
     * The method evaluates every box B in the given set under this function F and
     * assigns a sign vector v to B based on the signs of the components F(B), which
     * creates an initial sign covering.
     * If F(B) = 0 for some box B, then B is bisected into B1, B2 by the given bisector
     * and B is replaced by it. B1 and B2 are then evaluated under F(B) to get their
     * own sign vector assigned.
     * This process continues, until the constructed sign covering is sufficient
     * or the bisection is unsucessful.
     * @param boxList The input lists of boxes.
     * @param paramBox Parameter box to parametrize this function, you may pass
     * vytlalib::boxes::EMPTY_BOX if this function is meant not to be parametrized.
     * @param bisector The bisector instance used for bisecting.
     * @return Constructed sign covering of @p boxList or a refined set wrt. this function.
     * This may or may not be sufficient.
     */
    boxes::SignCoveringList computeSignCovering(boxes::BoxList boxList, const boxes::Box &paramBox,
                                         const bisector::IBisector &bisector);

    /**
     * @brief Parses the given string into an interval inclusion function using
     * the recursive interpreter for interpretation
     * @param in The input function definition string to parse.
     * @return Unique pointer to an IFunction using the recursive interpreter.
     */
    static UPtrIFunction parseRecursive(std::string in);

    /**
     * @brief Parses the given std::istream into an interval inclusion function using
     * the recursive interpreter for interpretation
     * @param in The input stream to parse the function definition from.
     * @return Unique pointer to an IFunction using the recursive interpreter.
     */
    static UPtrIFunction parseRecursive(std::istream &in);

    /**
     * @brief Parses the given string into an interval inclusion function using
     * the linear interpreter for interpretation
     * @param in The input function definition string to parse.
     * @return Unique pointer to an IFunction using the linear interpreter.
     */
    static UPtrIFunction parseLinear(std::string);

    /**
     * @brief Parses the given std::istream into an interval inclusion function using
     * the linear interpreter for interpretation
     * @param in The input stream to parse the function definition from.
     * @return Unique pointer to an IFunction using the linear interpreter.
     */
    static UPtrIFunction parseLinear(std::istream &in);

    /**
     * @brief Parses the given string into an interval inclusion function using
     * the linear-cached interpreter for interpretation
     * @param in The input function definition string to parse.
     * @return Unique pointer to an IFunction using the linear-cached interpreter.
     */
    static UPtrIFunction parseLinearCached(std::string);

    /**
     * @brief Parses the given std::istream into an interval inclusion function using
     * the linear-cached interpreter for interpretation
     * @param in The input stream to parse the function definition from.
     * @return Unique pointer to an IFunction using the linear-cached interpreter.
     */
    static UPtrIFunction parseLinearCached(std::istream &in);

    /**
     * @brief Clones this instance.
     * @return
     */
    UPtrIFunction clone() const {
        return UPtrIFunction(performCloning());
    }

    virtual ~IFunction() { }

    friend std::ostream& operator <<(std::ostream &out, IFunction &f);

    /**
     * @brief Sends this instance to the given output stream.
     * @param out
     */
    virtual void toStream(std::ostream &out);
};


/**
 * @brief Specialization of IFunction class that uses recursive interpreter to compute
 * images of boxes under the underlying function.
 */
class Function : public IFunction {
    friend class IFunction;
    friend class evaluator::StandardEvaluator;
private:
    // Stored interpreter
    std::shared_ptr<evaluator::StandardEvaluator> evaluator_;
    // Root of the AST
    std::shared_ptr<ast::FunctionDefinition> root_;

    // For cloning
    Function* performCloning() const override;

    // Private ctor
    Function(std::shared_ptr<ast::FunctionDefinition> root);

    // Parsing
    static UPtrFunction parse(std::string in);
    static UPtrFunction parse(std::istream &in);

public:

    ~Function() override;

    /**
     * @brief A convenient function that parses an arithmetic expression into
     * a scalar function with no variables and evaluates it, returning the result.
     * @param in The expression to parse as string.
     * @return The evaluated experssion.
     */
    static boxes::Box evalBoxExpression(std::string in);

    /**
     * @brief A convenient function that parses an arithmetic expression into
     * a scalar function with no variables and evaluates it, returning the result.
     * @param in The input stream to parse the expression from.
     * @return The evaluated experssion.
     */
    static boxes::Box evalBoxExpression(std::istream &in);

    /**
     * @brief @see IFunction::eval(const boxes::Box&).
     */
    boxes::Box eval(const boxes::Box &domainBox) override;

    /**
     * @brief @see IFunction::eval(const boxes::Box&).
     */
    boxes::Box eval(const boxes::Box &domainBox, const boxes::Box &paramBox) override;

    /**
     * @brief @see IFuntion::getDomainDim
     * @return
     */
    int getDomainDim() override;

    /**
     * @brief @see IFunction::getImageDim
     * @return
     */
    int getImageDim() override;

    /**
     * @brief Compares this to the other given instance.
     * @param other
     * @return
     */
    bool operator ==(const Function &other);

    /**
     * @brief Returns !(this == other)
     * @param other
     * @return
     */
    bool operator !=(const Function &other);

    /**
     * @brief @see IFunction::toStream
     */
    void toStream(std::ostream &out) override;
};

/**
 * @brief Specialization of IFunction class that uses linear interpreter
 * or linear cached interpreter to compute
 * images of boxes under the underlying function.
 */
class IByteCodeCommon : public IFunction {
    friend class IFunction;
    friend class evaluator::ByteCodeEvaluator;
    friend class evaluator::CachedEvaluator;
private:
    virtual std::vector<ast::TreePostOrderLinearizer::Instruction>& getByteCode() = 0;
};

class ByteCodeFunction : public IByteCodeCommon {
    friend class IFunction;
private:
    int domainDim_;
    int imageDim_;
    std::shared_ptr<ast::FunctionDefinition> root_;
    std::vector<ast::TreePostOrderLinearizer::Instruction> byteCode_;
    std::shared_ptr<evaluator::ByteCodeEvaluator> evaluator_;

    std::vector<ast::TreePostOrderLinearizer::Instruction>& getByteCode() override {
        return byteCode_;
    }

    ByteCodeFunction* performCloning() const override;

    ByteCodeFunction(std::shared_ptr<ast::FunctionDefinition> root);

    static UPtrByteCodeFunction parse(std::string in);
    static UPtrByteCodeFunction parse(std::istream &in);
    static UPtrByteCodeFunction parseCached(std::string in);
    static UPtrByteCodeFunction parseCached(std::istream &in);

public:
    ~ByteCodeFunction() override;

    boxes::Box eval(const boxes::Box &domainBox) override;

    boxes::Box eval(const boxes::Box &domainBox, const boxes::Box &paramBox) override;

    int getDomainDim() override {
        return domainDim_;
    }

    int getImageDim() override {
        return imageDim_;
    }

    void toStream(std::ostream &out) override;
};

}

}

#endif // INTERPRETER_H
