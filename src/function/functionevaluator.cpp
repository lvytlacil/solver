#include <sstream>
#include <string>
#include <vector>
#include <bitset>
#include "../component.h"
#include "functionevaluator.h"
#include "../bisector/bisector.h"
#include "../message/messageprocessor.h"
#include "function.h"
namespace vytlalib {
using namespace ast;
using message::Severity;
using message::Message;
using message::IMessageProcessor;
using boxes::Box;
using boxes::Component;
using boxes::BoxList;
using boxes::SignCoveringList;
using boxes::SignVector;
using function::Function;
using bisector::IBisector;


namespace evaluator {

static bool sendMessage(std::string content, Severity severity) {
    Message msg(typeid(StandardEvaluator).name(), content, severity);
    IMessageProcessor::mproc->process(msg);
    return true;
}

static void dimErrorMessage(int expected, int actual) {
    std::stringstream ss;
    ss << "Wrong input dimension. Expected " << expected << ", but got" << actual;
    sendMessage(ss.str(), Severity::error);
}



StandardEvaluator::StandardEvaluator(function::Function &f):
    storedF_(f), env_(64),
    result_(boxes::Box(f.getImageDim(), boxes::Component("0")))
{ }

static const boxes::Box* varDomainSt;
static const boxes::Box* parDomainSt;

boxes::Box StandardEvaluator::eval(const boxes::Box &varDomain,
                                   const boxes::Box &parDomain) {
    varDomainSt = &varDomain;
    parDomainSt = &parDomain;
    const int varSize = varDomain.getSize();
    const int parSize = parDomain.getSize();

    if (varSize + parSize != storedF_.getDomainDim()) {
        dimErrorMessage(storedF_.getDomainDim(), varSize + parSize);
    }

    for (int i = 0; i < varSize; ++i) {
        env_[i] = varDomain.getComp(i);
    }

    for (int i = 0; i < parSize; ++i) {
        env_[i + varSize] = parDomain.getComp(i);
    }

    try {


        for (auto expr : storedF_.root_->expressions) {
            expr->visitBy(this);
        }

        storedF_.root_->retStatement->visitBy(this);




    } catch (...) {
        throw;
    }

    return Box(result_);
}

void StandardEvaluator::visit(ast::Node * node) {
    vytlassert(false, "Missing implementation of some Node subclass");
}

void StandardEvaluator::visit(FormalArgumentList *node) {
    // Do nothing
}

void StandardEvaluator::visit(Expression *node) {
    vytlassert(false, "Missing implementation of some Function Syntax Tree node subtype");
}

void StandardEvaluator::visit(ReturnStatement *node) {
    int i = 0;
    for (auto exp : node->components) {
        exp->visitBy(this);
        result_.setComp(i, intermediate_);
        ++i;
    }
}

void StandardEvaluator::visit(FunctionDefinition *node) {
    node->argList->visitBy(this);
    for (auto expr : node->expressions) {
        expr->visitBy(this);
    }
    node->retStatement->visitBy(this);
}

void StandardEvaluator::visit(Number *node) {
    intermediate_ = node->value;
}

void StandardEvaluator::visit(NumberInterval *node) {
    // Evaluate right expression
    node->right->visitBy(this);
    auto right = intermediate_;
    node->left->visitBy(this);
    // Now intermediate_ holds the value of left expression

    if (intermediate_.lower() > right.upper()) {
        intermediate_.emptySet();
    } else {
        intermediate_ = Component(intermediate_.lower(), right.upper());
    }
}

void StandardEvaluator::visit(Identifier *node) {
    intermediate_ = env_[node->index];
}

void StandardEvaluator::visit(BinOp *node) {
    // Evaluate right operand
    node->rhs->visitBy(this);
    auto right = intermediate_;
    node->lhs->visitBy(this);
    // Now intermediate_ holds the evaluated left operand

    switch (node->type) {
    case BinOp::Type::add:
        intermediate_ += right;
        break;
    case BinOp::Type::sub:
        intermediate_ -= right;
        break;
    case BinOp::Type::mul:
        intermediate_ *= right;
        break;
    case BinOp::Type::div:
        intermediate_ /= right;
        break;
    case BinOp::Type::pow:
        if (right.isConstantInt()) {
            // Now intermediate is degenerated and it is castable into int
            intermediate_.pow(static_cast<int>(right.lower()));
        } else {
            // Calculate x ^ y as exp(y * ln(x))
            intermediate_.pow(right);
        }
        break;
    }

}

void StandardEvaluator::visit(NullaryFunc *node) {
    switch (node->type) {
    case NullaryFunc::Type::pi:
        intermediate_.const_pi();
        break;
    case NullaryFunc::Type::e:
        intermediate_.const_e();
        break;
    default:
        vytlassert(fasle, "Unimplemented nullary function (constant)");
    }
}

void StandardEvaluator::visit(UnaryFunc *node) {
    // eval argument
    node->arg->visitBy(this);

    switch (node->type) {
    case UnaryFunc::Type::abs:
        intermediate_.abs();
        break;
    case UnaryFunc::Type::acos:
        intermediate_.acos();
        break;
    case UnaryFunc::Type::asin:
        intermediate_.asin();
        break;
    case UnaryFunc::Type::atan:
        intermediate_.atan();
        break;
    case UnaryFunc::Type::cos:
        intermediate_.cos();
        break;
    case UnaryFunc::Type::exp:
        intermediate_.exp();
        break;
    case UnaryFunc::Type::log:
        intermediate_.log();
        break;
    case UnaryFunc::Type::sin:
        intermediate_.sin();
        break;
    case UnaryFunc::Type::sqr:
        intermediate_.sqr();
        break;
    case UnaryFunc::Type::sqrt:
        intermediate_.sqrt();
        break;
    case UnaryFunc::Type::tan:
        intermediate_.tan();
        break;
    default:
        vytlassert(false, "Unimplemented unary function");
    }
}

void StandardEvaluator::visit(UnaryFuncWithIntArg *node) {
    // eval arg
    node->arg->visitBy(this);
    switch (node->type) {
    case UnaryFuncWithIntArg::Type::sqrtn:
        intermediate_.nthRoot(node->n);
        break;
    default:
        vytlassert(false, "Unimplemented unary function with int argument");
    }
}

void StandardEvaluator::visit(UnaryMinus *node) {
    // eval arg
    node->arg->visitBy(this);
    intermediate_.neg();
}




/*---------------- ByteCodeEvaluator ----------------------*/

ByteCodeEvaluator::ByteCodeEvaluator(function::IByteCodeCommon &f):
    storedF_(f), env_(Environment(64)),
    result_(boxes::Box(f.getImageDim(), Component("0")))
{ }



Box ByteCodeEvaluator::eval(const boxes::Box &varDomain,
                            const boxes::Box &parDomain) {
    // Preparation phase
    const int varSize = varDomain.getSize();
    const int parSize = parDomain.getSize();

    if (varSize + parSize != storedF_.getDomainDim()) {
        dimErrorMessage(storedF_.getDomainDim(), varSize + parSize);
    }

    for (int i = 0; i < varSize; ++i) {
        env_[i] = varDomain.getComp(i);
    }

    for (int i = 0; i < parSize ; ++i) {
        env_[i + varSize] = parDomain.getComp(i);
    }

    // the computation phase
    typedef TreePostOrderLinearizer::OpCode OpCode;

    auto& byteCode = storedF_.getByteCode();
    for (TreePostOrderLinearizer::Instruction& instr : byteCode) {
        switch (instr.opcode) {
        case OpCode::unknown:
            vytlassert(false, "Unknown opcode");
            break;
        case OpCode::arglist:
            //st.push(Boundary(0.0)); // Push dummy value onto the stack
            break;
        case OpCode::ret: {
            ReturnStatement* ret = reinterpret_cast<ReturnStatement*>(instr.node);
            // top component on the stack is 0-th component of the resulting box
            int i = 0;
            for (auto &c : ret->components) {
                result_.setComp(i, c->value);
                ++i;
            }
            break;
        }
        case OpCode::funcdef: {
            // Nothing to do
            break;
        }
        case OpCode::num: {
            // Value already parsed and stored by parser for efficiency
            break;
        }
        case OpCode::numinterval: {
            NumberInterval* numinterval = reinterpret_cast<NumberInterval*>(instr.node);
            Component& left = numinterval->left->value;
            Component& right = numinterval->right->value;

            if (left.lower() > right.upper()) {
                numinterval->value.emptySet();
            } else {
                numinterval->value = Component(left.lower(), right.upper());
            }
            break;
        }
        case OpCode::ident: {
            Identifier* ident = reinterpret_cast<Identifier*>(instr.node);
            ident->value = env_[ident->index];
            break;
        }
        case OpCode::binop: {
            BinOp* binop = reinterpret_cast<BinOp*>(instr.node);
            Component& result = binop->value;
            result = binop->lhs->value; // copy of left
            Component& right = binop->rhs->value; // reference to right

            switch (binop->type) {
            case BinOp::Type::add:
                result += right;
                break;
            case BinOp::Type::sub:
                result -= right;
                break;
            case BinOp::Type::mul:
                result *= right;
                break;
            case BinOp::Type::div:
                result /= right;
                break;
            case BinOp::Type::pow:
                if (right.isConstantInt()) {
                    // Now intermediate is degenerated and it is castable into int
                    result.pow(static_cast<int>(right.lower()));
                } else {
                    // Calculate x ^ y as exp(y * ln(x))
                    result.pow(right);
                }
                break;
            }
            break;
        }
        case OpCode::unaryfunc: {
            UnaryFunc* unaryfunc = reinterpret_cast<UnaryFunc*>(instr.node);

            Component& result = unaryfunc->value;
            result = unaryfunc->arg->value; // copy
            switch (unaryfunc->type) {
            case UnaryFunc::Type::abs:
                result.abs();
                break;
            case UnaryFunc::Type::acos:
                result.acos();
                break;
            case UnaryFunc::Type::asin:
                result.asin();
                break;
            case UnaryFunc::Type::atan:
                result.atan();
                break;
            case UnaryFunc::Type::cos:
                result.cos();
                break;
            case UnaryFunc::Type::exp:
                result.exp();
                break;
            case UnaryFunc::Type::log:
                result.log();
                break;
            case UnaryFunc::Type::sin:
                result.sin();
                break;
            case UnaryFunc::Type::sqr:
                result.sqr();
                break;
            case UnaryFunc::Type::sqrt:
                result.sqrt();
                break;
            case UnaryFunc::Type::tan:
                result.tan();
                break;
            default:
                vytlassert(false, "Unimplemented unary function");
            }
            // We left arg on the stack, no pushing required
            break;
        }
        case OpCode::unaryfunc_intarg: {
            UnaryFuncWithIntArg* un_intarg = reinterpret_cast<UnaryFuncWithIntArg*>(instr.node);
            Component& result = un_intarg->value;
            result = un_intarg->arg->value; // copy of value

            switch (un_intarg->type) {
            case UnaryFuncWithIntArg::Type::sqrtn:
                result.nthRoot(un_intarg->n);
                break;
            default:
                vytlassert(false, "Unimplemented unary function with int argument");
            }
            // We left arg on the stack, no pushing required
            break;
        }
        case OpCode::nullaryfunc: {
            NullaryFunc* nullaryfunc = reinterpret_cast<NullaryFunc*>(instr.node);
            switch (nullaryfunc->type) {
            case NullaryFunc::Type::pi:
                nullaryfunc->value = Component::create_pi();
                break;
            case NullaryFunc::Type::e:
                nullaryfunc->value = Component::create_e();
                break;
            default:
                vytlassert(false, "Unimplemented nullary function (constant)");
            }
            break;
        }
        case OpCode::unaryminus: {
            UnaryMinus* unaryminus = reinterpret_cast<UnaryMinus*>(instr.node);
            Component& result = unaryminus->value;
            result = unaryminus->arg->value; // copy of argument
            result.neg();
            break;
        }
        default:
            vytlassert(false, "Unknown opcode");
        }
    }
    return Box(result_);
}

/*------------- CachedEvaluator -------------------------*/

CachedEvaluator::CachedEvaluator(function::IByteCodeCommon &f):
    ByteCodeEvaluator(f), lastInput(BitField64::createEmpty()) {
    Component zeroComponent("0");

    for (int i = 0; i < f.getDomainDim(); ++i) {
        env_[i] = zeroComponent;
    }
    ByteCodeEvaluator evaluator(f);
    Box initialInput(f.getDomainDim(), Component("0"));
    result_ = Box(evaluator.eval(initialInput));
}

boxes::Box CachedEvaluator::eval(const boxes::Box &varDomain, const boxes::Box &parDomain) {
    const int varSize = varDomain.getSize();
    const int parSize = parDomain.getSize();

    if (varSize + parSize != storedF_.getDomainDim()) {
        dimErrorMessage(storedF_.getDomainDim(), varSize + parSize);
    }

    BitField64 matchedVars = BitField64::createEmpty();
    for (int i = 0; i < varSize; ++i) {
        if (env_[i] != varDomain.getComp(i)) {
            env_[i] = varDomain.getComp(i);
        } else {
            matchedVars.setOn(i);
        }
    }

    for (int i = 0; i < parSize; ++i) {
        if (env_[i + varSize] != parDomain.getComp(i)) {
            env_[i + varSize] = parDomain.getComp(i);
        } else {
            matchedVars.setOn(i + varSize);
        }
    }

    return eval(matchedVars, varDomain, parDomain);
}

boxes::Box CachedEvaluator::eval(BitField64& matchedVars, const boxes::Box &varDomain,
                           const boxes::Box &parDomain) {
    lastInput = matchedVars;
    typedef TreePostOrderLinearizer::OpCode OpCode;

    auto& byteCode = storedF_.getByteCode();
    for (TreePostOrderLinearizer::Instruction& instr : byteCode) {
        if (instr.opcode == OpCode::ret) {
            ReturnStatement* ret = reinterpret_cast<ReturnStatement*>(instr.node);
            int i = 0;
            for (auto &c : ret->components) {
                if (!c->identsInUse.isSubsetOf(matchedVars)) {
                    result_.setComp(i, c->value);
                }
                ++i;
            }
            break;
        } else {
            if (instr.node->identsInUse.isSubsetOf(matchedVars)) {
                continue;
            }
            switch (instr.opcode) {
            case OpCode::unknown:
                vytlassert(false, "Unknown opcode");
                break;
            case OpCode::arglist:
                //st.push(Boundary(0.0)); // Push dummy value onto the stack
                break;
            case OpCode::ret: {
                vytlassert(false, "Unknown opcode");
            }
            case OpCode::funcdef: {
                // FunctionDefinition* fdef = reinterpret_cast<FunctionDefinition*>(instr.node.get());
                // Nothing to do
                break;
            }
            case OpCode::num: {
                // Value already parsed and stored by parser for efficiency
                break;
            }
            case OpCode::numinterval: {
                NumberInterval* numinterval = reinterpret_cast<NumberInterval*>(instr.node);
                Component& left = numinterval->left->value;
                Component& right = numinterval->right->value;

                if (left.lower() > right.upper()) {
                    numinterval->value.emptySet();
                } else {
                    numinterval->value = Component(left.lower(), right.upper());
                }
                break;
            }
            case OpCode::ident: {
                Identifier* ident = reinterpret_cast<Identifier*>(instr.node);
                ident->value = env_[ident->index];
                break;
            }
            case OpCode::binop: {
                BinOp* binop = reinterpret_cast<BinOp*>(instr.node);
                Component& result = binop->value;
                result = binop->lhs->value; // copy of left
                Component& right = binop->rhs->value; // reference to right

                switch (binop->type) {
                case BinOp::Type::add:
                    result += right;
                    break;
                case BinOp::Type::sub:
                    result -= right;
                    break;
                case BinOp::Type::mul:
                    result *= right;
                    break;
                case BinOp::Type::div:
                    result /= right;
                    break;
                case BinOp::Type::pow:
                    if (right.isConstantInt()) {
                        // Now intermediate is degenerated and it is castable into int
                        result.pow(static_cast<int>(right.lower()));
                    } else {
                        // Calculate x ^ y as exp(y * ln(x))
                        result.pow(right);
                    }
                    break;
                }
                break;
            }
            case OpCode::unaryfunc: {
                UnaryFunc* unaryfunc = reinterpret_cast<UnaryFunc*>(instr.node);

                Component& result = unaryfunc->value;
                result = unaryfunc->arg->value; // copy
                switch (unaryfunc->type) {
                case UnaryFunc::Type::abs:
                    result.abs();
                    break;
                case UnaryFunc::Type::acos:
                    result.acos();
                    break;
                case UnaryFunc::Type::asin:
                    result.asin();
                    break;
                case UnaryFunc::Type::atan:
                    result.atan();
                    break;
                case UnaryFunc::Type::cos:
                    result.cos();
                    break;
                case UnaryFunc::Type::exp:
                    result.exp();
                    break;
                case UnaryFunc::Type::log:
                    result.log();
                    break;
                case UnaryFunc::Type::sin:
                    result.sin();
                    break;
                case UnaryFunc::Type::sqr:
                    result.sqr();
                    break;
                case UnaryFunc::Type::sqrt:
                    result.sqrt();
                    break;
                case UnaryFunc::Type::tan:
                    result.tan();
                    break;
                default:
                    vytlassert(false, "Unimplemented unary function");
                }
                // We left arg on the stack, no pushing required
                break;
            }
            case OpCode::unaryfunc_intarg: {
                UnaryFuncWithIntArg* un_intarg = reinterpret_cast<UnaryFuncWithIntArg*>(instr.node);
                Component& result = un_intarg->value;
                result = un_intarg->arg->value; // copy of value

                switch (un_intarg->type) {
                case UnaryFuncWithIntArg::Type::sqrtn:
                    result.nthRoot(un_intarg->n);
                    break;
                default:
                    vytlassert(false, "Unimplemented unary function with int argument");
                }
                // We left arg on the stack, no pushing required
                break;
            }
            case OpCode::nullaryfunc: {
                NullaryFunc* nullaryfunc = reinterpret_cast<NullaryFunc*>(instr.node);
                switch (nullaryfunc->type) {
                case NullaryFunc::Type::pi:
                    nullaryfunc->value = Component::create_pi();
                    break;
                case NullaryFunc::Type::e:
                    nullaryfunc->value = Component::create_e();
                    break;
                default:
                    vytlassert(false, "Unimplemented nullary function (constant)");
                }
                break;
            }
            case OpCode::unaryminus: {
                UnaryMinus* unaryminus = reinterpret_cast<UnaryMinus*>(instr.node);
                Component& result = unaryminus->value;
                result = unaryminus->arg->value; // copy of argument
                result.neg();
                break;
            }
            default:
                vytlassert(false, "Unknown opcode");
            } // opcode switch for actual computation
        } // branch for actual computation
    } // iteration over instructions

    return Box(result_);
}

}

}
