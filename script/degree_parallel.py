import subprocess
import resource
import threading
import io
import sys
import re
from time import process_time


##### Script parameters

# Stores the actual inputs to measure. #THR, #FNC are placeholders, that the script
# will replace with actual number of threads and function evaluator type, respectively.
degreeInput = [
    """[-t #THR -f #FNC][x1, x2, x3, x4, x5, x6, x7, x8]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[-t #THR -f #FNC][x1, x2, x3, x4, x5, x6, x7, x8, x9]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[-t #THR -f #FNC][x1, x2, x3, x4, x5, x6, x7, x8, x9, x10]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2 - x10^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;
            2*x1*x10;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """

]



# Format string to pass to GNU time as argument.
gnuTimeFormat = "utime %U\nstime %S\nwall %e\ncpu %P"
# Full command to execute as a single string.
cmd = "/usr/bin/time -f \"{}\" ./degree".format(gnuTimeFormat)

# Patterns capturing groups in GNU time output with the values of interest.
utimePattern = re.compile(r'utime (\d+((\.)?\d*))')
stimePattern = re.compile(r'stime (\d+(\.?\d*))')
wallTimePattern = re.compile(r'wall (\d+(\.?\d*))')
cpuUsagePattern = re.compile(r'cpu (\d+(\.?\d*))%')

# Min and max values of thread number options that should be executed (i.e. for every input, measurement
# for threadsMin, threadsMin + 1, ..., threadsMax will be performed)
threadsMin = 1
threadsMax = 3

# No. of repeats of each measurement (i.e. for every pair of input and number of work threads)
repeats = 2





#####

# Check minimal required version. Version 3.5 is needed to use the subprocess module.
if sys.version_info < (3,5):
    raise Exception("You need to run this script with Python version 3.5 or higher.")

for item in degreeInput:
    print("Measured input: ", item, sep = '')
    print("-" * 20)
    for fnctype, j in [(x,y) for x in ["linear"] for y in range(threadsMin, threadsMax + 1)]:

        # Reset measured values
        utime = []
        stime = []
        wall = []
        cpuUsage = []

        print("Function evaluator type: ", fnctype)
        print("No. of work threads ", j, ' ')
        for i in range(repeats):  
            p = subprocess.Popen(cmd, shell = True, stderr = subprocess.STDOUT, stdout = subprocess.PIPE, stdin = subprocess.PIPE)
            output, err = p.communicate(input = item.replace("#THR", str(j)).replace("#FNC", fnctype).encode())
            output = output.decode()

            # Parse the output, save it
            try:
                utime.append(float(re.search(utimePattern, output).group(1)))
                stime.append(float(re.search(stimePattern, output).group(1)))
                cpuUsage .append(float(re.search(cpuUsagePattern, output).group(1)))
                wall.append(float(re.search(wallTimePattern, output).group(1)))
            except AttributeError as e:
                print("ERROR: String values successfully parsed, but error during their parsing to numerical form occured.\n")
                raise   
            except IndexError:
                print("ERROR: Could not parse the output of ", cmd, " ; Check the parameters at the top of the scripts.", sep = '')
                raise

            # Process and print the gathered information
            def avg(lst):
                try:
                    return sum(lst) / (float(len(lst)))
                except ZeroDivisionError:
                    print("ERROR: List cannot be empty.")
                    raise


        print("  No. of repeats: ", repeats, sep = '')
        print("  No. of work threads: ", j, sep = '')
        print("  User time: ", avg(utime), sep = '')
        print("  System time: ", avg(stime), sep = '')
        print("  User + system time: ", avg(utime) + avg(stime), sep = '')
        print("  Total running time (wall clock time): ", avg(wall), sep = '')
        print("  Percentage of CPU used during running time: ", avg(cpuUsage), "%", sep = '')
        print()

    print("#" * 20);

    # TODO: Add no. cores as an option
