import subprocess
import resource
import threading
import io
import sys
import re
from time import process_time


##### Script parameters

# Stores the actual inputs to measure. #THR, #FNC are placeholders, that the script
# will replace with actual number of threads and function evaluator type, respectively.
degreeInput = [
        
        (
        """[-sc -f #FNC]
            [x1 x2 x3 x4 x5]
            [x1*x2^2 + x1*x3^2 + x1*x4^2 + x1*x5^2 - 1.1*x1 + 1;
            x2*x1^2 + x2*x3^2 + x2*x4^2 + x2*x5^2 - 1.1*x2 + 1;
            x3*x1^2 + x3*x2^2 + x3*x4^2 + x3*x5^2 - 1.1*x3 + 1;
            x4*x1^2 + x4*x2^2 + x4*x3^2 + x4*x5^2 - 1.1*x4 + 1;
            x5*x1^2 + x5*x2^2 + x5*x3^2 + x5*x4^2 - 1.1*x5 + 1;]

            [ [-.8, 0], [-.8, 0], [-.8, 0], [-.8, 0], [-.8, 0] ]
            """
        ),
        (
        """
            [-sc -f #FNC]
            [x y z t u][
            -1 + 2*x^2 - 2*y^2 + 2*z^2 - 2*t^2 + 2*u^2;
            -1 + 2*x^3 - 2*y^3 + 2*z^3 - 2*t^3 + 2*u^3;
            -1 + 2*x^4 - 2*y^4 + 2*z^4 - 2*t^4 + 2*u^4;
            -1 + 2*x^5 - 2*y^5 + 2*z^5 - 2*t^5 + 2*u^5;
            -1 + 2*x^6 - 2*y^6 + 2*z^6 - 2*t^6 + 2*u^6;]
            [ [0.1, 1], [0.1, 1], [0.1, 1], [0.1, 1], [0.1, 1]]
        """
        ),
        (
        """
            [-sc -f #FNC]
            [x y z t u][
            sqrt5(atan(-1 + 2*x^2 - 2*y^2 + 2*z^2 - 2*t^2 + 2*u^2));
            sqrt5(atan(-1 + 2*x^3 - 2*y^3 + 2*z^3 - 2*t^3 + 2*u^3));
            sqrt5(atan(-1 + 2*x^4 - 2*y^4 + 2*z^4 - 2*t^4 + 2*u^4));
            sqrt5(atan(-1 + 2*x^5 - 2*y^5 + 2*z^5 - 2*t^5 + 2*u^5));
            sqrt5(atan(-1 + 2*x^6 - 2*y^6 + 2*z^6 - 2*t^6 + 2*u^6));]
            [ [0.1, 1], [0.1, 1], [0.1, 1], [0.1, 1], [0.1, 1]]
        """
        )
]



# Format string to pass to GNU time as argument.
gnuTimeFormat = "utime %U\nstime %S\nwall %e\ncpu %P"
# Full command to execute as a single string.
cmd = "sudo chrt -f 99 /usr/bin/time -f \"{}\" ./degree".format(gnuTimeFormat)

# Patterns capturing groups in GNU time output with the values of interest.
utimePattern = re.compile(r'utime (\d+((\.)?\d*))')
stimePattern = re.compile(r'stime (\d+(\.?\d*))')
wallTimePattern = re.compile(r'wall (\d+(\.?\d*))')
pairsInSignCovPattern = re.compile(r'Pairs in.*');

# No. of repeats of each measurement (i.e. for every pair of input and number of work threads)
repeats = 2



#####

# Check minimal required version. Version 3.5 is needed to use the subprocess module.
if sys.version_info < (3,5):
    raise Exception("You need to run this script with Python version 3.5 or higher.")

for item in degreeInput:
    print("Measured input: ", item, sep = '')
    print("-" * 20)
    for fnctype in ["ast", "linear", "lincached"]:

        # Reset measured values
        utime = []
        stime = []
        wall = []
        pairsInScText = ""

        print("Function evaluator type: ", fnctype)

        for i in range(repeats):  
            p = subprocess.Popen(cmd, shell = True, stderr = subprocess.STDOUT, stdout = subprocess.PIPE, stdin = subprocess.PIPE)
            output, err = p.communicate(input = item.replace("#FNC", fnctype).encode())
            output = output.decode()

            # Parse the output, save it
            try:
                utime.append(float(re.search(utimePattern, output).group(1)))
                stime.append(float(re.search(stimePattern, output).group(1)))
                wall.append(float(re.search(wallTimePattern, output).group(1)))
                pairsInScText = re.search(pairsInSignCovPattern, output).group(0) # simply overwrite it, because it is always the same
            except AttributeError as e:
                print("ERROR: String values successfully parsed, but error during their parsing to numerical form occured.\n")
                raise   
            except IndexError:
                print("ERROR: Could not parse the output of ", cmd, " ; Check the parameters at the top of the scripts.", sep = '')
                raise

        # Process and print the gathered information
        def avg(lst):
            try:
                return sum(lst) / (float(len(lst)))
            except ZeroDivisionError:
                print("ERROR: List cannot be empty.")
                raise

        print("  No. of repeats: ", repeats, sep = '')
        print("  User time: ", avg(utime), sep = '')
        print("  System time: ", avg(stime), sep = '')
        print("  User + system time: ", avg(utime) + avg(stime), sep = '')
        print("  Total running time (wall clock time): ", avg(wall), sep = '')
        print("  ", pairsInScText, sep='')
        print()

    print("#" * 20);

    # TODO: Add no. cores as an option
