import subprocess
import resource
import threading
import io
import sys
import re
from time import process_time


##### Script parameters

# Stores the actual inputs to topological degree computations implementations. Every element is
# a pair of strings. It is intended, that both strings in a pair always express the same function,
# the first one being in the syntactical form required by our implementation, and the other
# in the form required by the reference implementation.
degreeInput = [
    ("""[][x, y] [x + y; x - y;] [ [-1, 1], [-1, 1] ]""", """[x, y] x + y; x - y; [ [-1, 1], [-1, 1] ]"""),

    # Real representation of the complex function f(z)=z^2 on a box containing (0,0).")
    ("""[][x,y][x^2-y^2; 2*x*y;][[-1,2], [-1,2]]""", """[x,y] x^2-y^2; 2*x*y; [[-1,2], [-1,2]]""",),

    ("""[][x,y] [atan(log(x*y))+y; log(log(y+100));] [ [1,10],[10,20] ]""",
     """[x,y] ATAN(LOG(x*y))+y; LOG(LOG(y+100)); [ [1,10],[10,20] ]"""),

    # Example containing trigonometric functions and exponential
    ("""[][x,y,z] [cos(x)*sin(y)+cos(y)*sin(z); exp(x+y+z)-1; cos(y)*sin(z)-cos(z)*sin(y);] [ [-3,3], [-2,2], [-1,1]] """,
     """[x,y,z] COS(x)*SIN(y)+COS(y)*SIN(z); EXP(x+y+z)-1; COS(y)*SIN(z)-COS(z)*SIN(y); [ [-3,3], [-2,2], [-1,1]] """),

    # Identity function id:[-1,1]^3 -> R^3
    ("""[][x,y,z] [x; y; z;] [[-1,1], [-1,1], [-1,1]]""", 
     """[x,y,z] x; y; z; [[-1,1], [-1,1], [-1,1]]"""),

    # The linear function f_i(x)=\sum_{j\neq i} x_j in dimension 5.
    #The degree of a linear function is just the sign of the determinant (assuming the box contains 0).
    ("""[][x1, x2, x3, x4, x5] [x2+x3+x4+x5; x1+x3+x4+x5; x1+x2+x4+x5; x1+x2+x3+x5; x1+x2+x3+x4;] 
        [[-0.5,1], [-0.5,1], [-0.5,1], [-0.5,1], [-0.5,1]] """,
     """[x1, x2, x3, x4, x5] x2+x3+x4+x5; x1+x3+x4+x5; x1+x2+x4+x5; x1+x2+x3+x5; x1+x2+x3+x4; 
        [[-0.5,1], [-0.5,1], [-0.5,1], [-0.5,1], [-0.5,1]] """),

    ############# Variants of different parametrization for our implementation

    ("""[-f linear][x,y,z] [cos(x)*sin(y)+cos(y)*sin(z); exp(x+y+z)-1; cos(y)*sin(z)-cos(z)*sin(y);] [ [-3,3], [-2,2], [-1,1]] """,
     """[x,y,z] COS(x)*SIN(y)+COS(y)*SIN(z); EXP(x+y+z)-1; COS(y)*SIN(z)-COS(z)*SIN(y); [ [-3,3], [-2,2], [-1,1]] """),

    ("""[-f cached][x,y,z] [cos(x)*sin(y)+cos(y)*sin(z); exp(x+y+z)-1; cos(y)*sin(z)-cos(z)*sin(y);] [ [-3,3], [-2,2], [-1,1]] """,
     """[x,y,z] COS(x)*SIN(y)+COS(y)*SIN(z); EXP(x+y+z)-1; COS(y)*SIN(z)-COS(z)*SIN(y); [ [-3,3], [-2,2], [-1,1]] """),

    ("""[-t 3][x,y,z] [cos(x)*sin(y)+cos(y)*sin(z); exp(x+y+z)-1; cos(y)*sin(z)-cos(z)*sin(y);] [ [-3,3], [-2,2], [-1,1]] """,
     """[x,y,z] COS(x)*SIN(y)+COS(y)*SIN(z); EXP(x+y+z)-1; COS(y)*SIN(z)-COS(z)*SIN(y); [ [-3,3], [-2,2], [-1,1]] """),

    ("""[-t 3 -f linear][x,y,z] [cos(x)*sin(y)+cos(y)*sin(z); exp(x+y+z)-1; cos(y)*sin(z)-cos(z)*sin(y);] [ [-3,3], [-2,2], [-1,1]] """,
     """[x,y,z] COS(x)*SIN(y)+COS(y)*SIN(z); EXP(x+y+z)-1; COS(y)*SIN(z)-COS(z)*SIN(y); [ [-3,3], [-2,2], [-1,1]] """),

    ("""[-t 3 -f cached] [x,y,z] [cos(x)*sin(y)+cos(y)*sin(z); exp(x+y+z)-1; cos(y)*sin(z)-cos(z)*sin(y);] [ [-3,3], [-2,2], [-1,1]] """,
     """[x,y,z] COS(x)*SIN(y)+COS(y)*SIN(z); EXP(x+y+z)-1; COS(y)*SIN(z)-COS(z)*SIN(y); [ [-3,3], [-2,2], [-1,1]] """),

    ############## Following functions are aimed for execution time comparison

    # n = 7
    (
        """[][x1, x2, x3, x4, x5, x6, x7]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7]
            x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),
    (
        """[-f cached -t 4][x1, x2, x3, x4, x5, x6, x7]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7]
            x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),

    # n = 8
    (
        """[][x1, x2, x3, x4, x5, x6, x7, x8]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8]
            x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),
    (
        """[-f cached -t 4][x1, x2, x3, x4, x5, x6, x7, x8]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8]
            x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),

    # n = 9
    (
        """[][x1, x2, x3, x4, x5, x6, x7, x8, x9]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8, x9]
            x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),
    (
        """[-f cached -t 4][x1, x2, x3, x4, x5, x6, x7, x8, x9]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8, x9]
            x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),

    # n = 10
    (
        """[][x1, x2, x3, x4, x5, x6, x7, x8, x9, x10]
            [x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2 - x10^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;
            2*x1*x10;]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8, x9, x10]
            x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2 - x10^2;
            2*x1*x2;
            2*x1*x3;
            2*x1*x4;
            2*x1*x5;
            2*x1*x6;
            2*x1*x7;
            2*x1*x8;
            2*x1*x9;
            2*x1*x10;
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),

    # As the three previous, but with atan

    # n = 7
    (
        """[][x1, x2, x3, x4, x5, x6, x7]
            [atan(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2);
            atan(2*x1*x2);
            atan(2*x1*x3);
            atan(2*x1*x4);
            atan(2*x1*x5);
            atan(2*x1*x6);
            atan(2*x1*x7);]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7]
            ATAN(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2);
            ATAN(2*x1*x2);
            ATAN(2*x1*x3);
            ATAN(2*x1*x4);
            ATAN(2*x1*x5);
            ATAN(2*x1*x6);
            ATAN(2*x1*x7);
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),
    (
        """[-f cached -t 4][x1, x2, x3, x4, x5, x6, x7]
            [atan(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2);
            atan(2*x1*x2);
            atan(2*x1*x3);
            atan(2*x1*x4);
            atan(2*x1*x5);
            atan(2*x1*x6);
            atan(2*x1*x7);]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7]
            ATAN(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2);
            ATAN(2*x1*x2);
            ATAN(2*x1*x3);
            ATAN(2*x1*x4);
            ATAN(2*x1*x5);
            ATAN(2*x1*x6);
            ATAN(2*x1*x7);
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),

    # n = 8
    (
        """[][x1, x2, x3, x4, x5, x6, x7, x8]
            [atan(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2);
            atan(2*x1*x2);
            atan(2*x1*x3);
            atan(2*x1*x4);
            atan(2*x1*x5);
            atan(2*x1*x6);
            atan(2*x1*x7);
            atan(2*x1*x8);]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8]
            ATAN(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2);
            ATAN(2*x1*x2);
            ATAN(2*x1*x3);
            ATAN(2*x1*x4);
            ATAN(2*x1*x5);
            ATAN(2*x1*x6);
            ATAN(2*x1*x7);
            ATAN(2*x1*x8);
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),
    (
        """[-f cached -t 4][x1, x2, x3, x4, x5, x6, x7, x8]
            [atan(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2);
            atan(2*x1*x2);
            atan(2*x1*x3);
            atan(2*x1*x4);
            atan(2*x1*x5);
            atan(2*x1*x6);
            atan(2*x1*x7);
            atan(2*x1*x8);]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8]
            ATAN(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2);
            ATAN(2*x1*x2);
            ATAN(2*x1*x3);
            ATAN(2*x1*x4);
            ATAN(2*x1*x5);
            ATAN(2*x1*x6);
            ATAN(2*x1*x7);
            ATAN(2*x1*x8);
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),

    # n = 9
    (
        """[][x1, x2, x3, x4, x5, x6, x7, x8, x9]
            [atan(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2);
            atan(2*x1*x2);
            atan(2*x1*x3);
            atan(2*x1*x4);
            atan(2*x1*x5);
            atan(2*x1*x6);
            atan(2*x1*x7);
            atan(2*x1*x8);
            atan(2*x1*x9);]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8, x9]
            ATAN(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2);
            ATAN(2*x1*x2);
            ATAN(2*x1*x3);
            ATAN(2*x1*x4);
            ATAN(2*x1*x5);
            ATAN(2*x1*x6);
            ATAN(2*x1*x7);
            ATAN(2*x1*x8);
            ATAN(2*x1*x9);
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),
    (
        """[-f cached -t 4][x1, x2, x3, x4, x5, x6, x7, x8, x9]
            [atan(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2);
            atan(2*x1*x2);
            atan(2*x1*x3);
            atan(2*x1*x4);
            atan(2*x1*x5);
            atan(2*x1*x6);
            atan(2*x1*x7);
            atan(2*x1*x8);
            atan(2*x1*x9);]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8, x9]
            ATAN(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2);
            ATAN(2*x1*x2);
            ATAN(2*x1*x3);
            ATAN(2*x1*x4);
            ATAN(2*x1*x5);
            ATAN(2*x1*x6);
            ATAN(2*x1*x7);
            ATAN(2*x1*x8);
            ATAN(2*x1*x9);
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),

    # n = 10 
    (
        """[][x1, x2, x3, x4, x5, x6, x7, x8, x9, x10]
            [atan(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 - x9^2 - x10^2);
            atan(2*x1*x2);
            atan(2*x1*x3);
            atan(2*x1*x4);
            atan(2*x1*x5);
            atan(2*x1*x6);
            atan(2*x1*x7);
            atan(2*x1*x8);
            atan(2*x1*x9);
            atan(2*x1*x10);]
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
       """,
       """[x1, x2, x3, x4, x5, x6, x7, x8, x9, x10]
            ATAN(x1^2 - x2^2 - x3^2 - x4^2 - x5^2 - x6^2 - x7^2 - x8^2 -x9^2 - x10^2);
            ATAN(2*x1*x2);
            ATAN(2*x1*x3);
            ATAN(2*x1*x4);
            ATAN(2*x1*x5);
            ATAN(2*x1*x6);
            ATAN(2*x1*x7);
            ATAN(2*x1*x8);
            ATAN(2*x1*x9);
            ATAN(2*x1*x10);
        [ [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1], [-1, 1] ]"
        """
    ),
]

# Format string to pass to GNU time as argument.
gnuTimeFormat = "utime %U\nstime %S\nwall %e\ncpu %P\nmemory %M"

# Command to execute GNU Time
timecmd = "sudo chrt -f 99 /usr/bin/time -f \"{}\"".format(gnuTimeFormat)

# Command to execute our implementation, related to current working directory
ourCmdImplementation = "./degree"

# Command to execute reference implementation, related to current working directory
refCmdImplementation = "./refdegree"

# Patterns capturing groups in GNU time output with the values of interest.
utimePattern = re.compile(r'utime (\d+((\.)?\d*))')
stimePattern = re.compile(r'stime (\d+(\.?\d*))')
wallTimePattern = re.compile(r'wall (\d+(\.?\d*))')
memoryPattern = re.compile(r'memory (\d+(\.?\d*))')

# Pattern capturing group with the value of computed topological degree for our implementation.
ourResultMatchPattern = re.compile(r'degree\s*=\s*(\-?\d+)')

# Pattern capturing group with the value of computed topological degree for reference implementation.
refResultMatchPatter = r'degree\s*=\s*(\-?\d+)'

# Timeout time in fractions of seconds to terminate the execution of degree implementations.
# This is used to prevent hanging in infinite an loop in the case that the boundary  of the input
# box contains a solution.
timeout = 240.0

# Number of measurement repeats for each input
repeats = 3

#####

class MeasuredData:
    def __init__(self, utime, stime, walltime, memory, output, timeouted):
        self.utime = utime
        self.stime = stime
        self.walltime = walltime
        self.memory = memory
        self.output = output
        self.timeouted = timeouted

def execCommand(cmd, input, timeout):
    ''' Starts the actual execution of the underlying thread, that calls the external command
    and measures execution time.
    Returns [false, execution_time] if time exceeds and [true, execution_time] on termination within timeout.'''

    process = subprocess.Popen(cmd, shell = True, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr = subprocess.STDOUT)



class Runner(threading.Thread):
    '''Encapsulates a code to execute external command, measure execution time
    and terminate it after timeout expires (if it is still running)'''
    def __init__(self, cmd, cmdInput, timeout):
        threading.Thread.__init__(self)
        self.cmd = cmd
        self.input = cmdInput
        self.timeout = timeout

    def run(self):
        self.process = subprocess.Popen(self.cmd, shell = True, 
            stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr = subprocess.STDOUT)
        self.out = self.process.communicate(input = bytes(self.input, encoding = 'ascii'))[0]
        

    def runSubprocess(self):
        ''' Starts the actual execution of the underlying thread, that calls the external command
        and measures execution time.
        Returns [false, execution_time] if time exceeds and [true, execution_time] on termination within timeout.'''
        #startUsage = resource.getrusage(resource.RUSAGE_CHILDREN)
        self.start()
        self.join(self.timeout)


        if self.is_alive(): # We joined because of the timeout
            self.process.terminate()
            self.join()
            timeouted = True;
        else:
            timeouted = False;

        try:
            utime = float(re.search(utimePattern, self.out.decode()).group(1))
            stime = float(re.search(stimePattern, self.out.decode()).group(1))
            walltime = float(re.search(wallTimePattern, self.out.decode()).group(1))
            memory = int(re.search(memoryPattern, self.out.decode()).group(1))
        except AttributeError as e:
            print("ERROR: String values successfully parsed, but error during their parsing to numerical form occured.\n")
            raise   
        except IndexError:
            print("ERROR: Could not parse the output. Check the parameters at the top of the script.", sep = '')
            raise

        return MeasuredData(utime, stime, walltime, memory, self.out.decode(), timeouted)



# Check minimal required version. Version 3.5 is needed to use the subprocess module.
if sys.version_info < (3,5):
    raise Exception("You need to run this script with Python version 3.5 or higher.")

degSummary = []
for ourInput, refInput in degreeInput:
    print("##########################")
    print("Input function:", ourInput)
    print("Repeats: ", repeats, sep = '')    

    ourutime = 0.0
    ourstime = 0.0
    ourwalltime = 0.0
    ourmemory = 0
    ourdegStr = ""

    refutime = 0.0
    refstime = 0.0
    refwalltime = 0.0 
    refmemory = 0  
    refdegStr = ""
    for i in range(repeats):        
        measuredData = Runner(timecmd + " " + ourCmdImplementation, ourInput, timeout).runSubprocess()
        ourutime += measuredData.utime
        ourstime += measuredData.stime
        ourwalltime += measuredData.walltime
        ourmemory += measuredData.memory

        if ourdegStr == "" and not measuredData.timeouted:
            try:
                ourdegStr = re.search(ourResultMatchPattern, measuredData.output).group(1)
            except AttributeError:
                print("ERROR: Could not parse the degree from the output. Check the parameters at the top of the script.", sep = '')
                raise
            except IndexError:
                print("ERROR: Could not parse the degree from the output. Check the parameters at the top of the script.", sep = '')
                raise

        measuredData = Runner(timecmd + " " + refCmdImplementation, refInput, timeout).runSubprocess()
        refutime += measuredData.utime
        refstime += measuredData.stime
        refwalltime += measuredData.walltime
        refmemory += measuredData.memory

        if refdegStr == "" and not measuredData.timeouted:
            try:
                refdegStr = str(re.search(refResultMatchPatter, measuredData.output).group(1))
            except AttributeError:
                print("ERROR: Could not parse the degree from the output. Check the parameters at the top of the script.", sep = '')
                raise
            except IndexError:
                print("ERROR: Could not parse the degree from the output. Check the parameters at the top of the script.", sep = '')
                raise

    ourutime /= repeats
    ourstime /= repeats
    ourwalltime /= repeats
    ourmemory /= repeats
    refutime /= repeats
    refstime /= repeats
    refwalltime /= repeats
    refmemory /= repeats

    print("--------------------------")
    print("Our implementation: ")
    print("  User time: ", ourutime)
    print("  System time: ", ourstime)
    print("  User + sytem time: ", ourutime + ourstime)
    print("  Wall time: ", ourwalltime)
    print("  Max. resident set size: ", ourmemory)
    print("  Computed degree: ", ourdegStr)
    print("Reference implementation: ")
    print("  User time: ", refutime)
    print("  System time: ", refstime)
    print("  User + sytem time: ", refutime + refstime)
    print("  Wall time: ", refwalltime)
    print("  Max. resident set size: ", refmemory)
    print("  Computed degree: ", refdegStr)

    if (ourdegStr == refdegStr):
        degSummary.append(1)
        if (ourdegStr == ""):
            print("BOTH IMPLEMENTATIONS TIMEOUTED !!!")
        else:
            print("DEGREES MATCH")
    else:
        degSummary.append(0)
        print("DEGREES DO NOT MATCH !!!")
    print("--------------------------")
    print("##########################")
    print()
    
print("### ", "SUMMARY", " ###", sep='')
print("----------------------------")
print("{0:^10} | {1:10}".format("Input no.", "Result"))
print("----------------------------")
for i in range(len(degSummary)):
    if degSummary[i] == 1:
        res = "MATCH"
    else:
        res = "MISMATCH"
    print("{0:^10} | {1:10}".format(i, res))
print("----------------------------")
print("Matches: ", sum(1 for r in degSummary if r == 1))
print("Mismatches: ", sum(1 for r in degSummary if r == 0))

