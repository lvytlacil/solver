import subprocess
import resource
import threading
import io
import sys
import re
from time import process_time


##### Script parameters

# Stores the actual inputs to measure. #THR, #FNC #SOL #CAP are placeholders, that the script
# will replace with actual number of threads and function evaluator type, respectively.
solverInput = [
"""
    [-ref 1.0 -d 0.0125 -s #SOL -f #FNC -c #CAP]
    [x y p q]
    [p*x+3*y - 1;
    q*x + 2*y - 3;]
    2
    [ [0.75, 1], [0.75, 1],  [-3, -1], [1, 2] ]
""",
"""
    [-ref 1.0 -d 0.0125 -s #SOL -f #FNC -c #CAP]
    [x p q]
    [
    (x-p)*q - (x-q)*p - sqr(p)*q + sqr(q)*p;
    ]
    2
    [ [-1, 1], [-2, 2], [-2, 2] ]
""",
"""
    [-ref 1.0 -d 0.0125 -s #SOL -f #FNC -c #CAP]
    [x y p q]
    [
    acos(p*x+q) - 0.5, sin(p*y+q) - 0.5
    ]
    2
    [ [-2, 2], [-2, 2], [-1, 1], [-1, 1] ]
""",
"""
    [-ref 1.0 -d 0.0125 -s #SOL -f #FNC -c #CAP]
    [x y a b]
    [
    a*(x-1)^2 + b*y^2 - 1;
    b*x^2 + a*(y-1)^2 - 1;
    ]
    2
    [ [-2, 2], [-2, 2], [-2, 4], [-2, 4] ] 
""",
"""
    [-ref 1.0 -d 0.0125 -s #SOL -f #FNC -c #CAP]
    [x y z a b]
    [
    a*(x-1)^2 + b*y^2 + sqrt(a^2+b^2)*z^2 - 1;
    b*x^2 + sqrt(a^2+b^2)*(y-1)^2 + a*z^2 - 1;
    sqrt(a^2+b^2)*x^2 + b*y^2 + a*(z-1)^2 -1;
    ]
    2
    [ [-2, 2], [-2, 2], [-2, 2], [-2, 2], [-2, 2] ]
"""


]



# Format string to pass to GNU time as argument.
gnuTimeFormat = "utime %U\nstime %S\nwall %e\ncpu %P\nmemory %M"
# Full command to execute as a single string.
cmd = "sudo chrt -f 99 /usr/bin/time -f \"{}\" ./solver".format(gnuTimeFormat)

# Patterns capturing groups in GNU time output with the values of interest.
utimePattern = re.compile(r'utime (\d+((\.)?\d*))')
stimePattern = re.compile(r'stime (\d+(\.?\d*))')
wallTimePattern = re.compile(r'wall (\d+(\.?\d*))')
memoryPattern = re.compile(r'memory (\d+(\.?\d*))')

# No. of repeats of each measurement (i.e. for every pair of input and number of work threads)
repeats = 1



#####

# Check minimal required version. Version 3.5 is needed to use the subprocess module.
if sys.version_info < (3,5):
    raise Exception("You need to run this script with Python version 3.5 or higher.")


solverLst = ["static", "bisectonly", "bisectandkeep", "tree", "graph"]
functionLst = ["ast", "linear", "lincached"]
capacityLst = [8, 16, 32, 64, 128]

for item in solverInput:
    print("Measured input: ", item, sep = '')
    print("-" * 20)
    for solver, func, cap in [(x,y,z) for x in solverLst for y in functionLst for z in capacityLst]:

 
        # Reset measured values
        utime = []
        stime = []
        wall = []
        memory = []
        out = ""
        print("-" * 20)
        print("Frame type: ", solver, sep = '')
        print("Function evaluator type: ", func, sep = '')
        print("Capacity: ", cap, sep = '')

        for i in range(repeats):  

            p = subprocess.Popen(cmd, shell = True, stderr = subprocess.PIPE, stdout = subprocess.PIPE, stdin = subprocess.PIPE)
            concreteItem = item.replace("#FNC", func).replace("#SOL", solver).replace("#CAP", str(cap))
            output, err = p.communicate(input = concreteItem.encode())
            output = output.decode()
            err = err.decode()

            # Parse the output, save it
            try:
                utime.append(float(re.search(utimePattern, err).group(1)))
                stime.append(float(re.search(stimePattern, err).group(1)))
                wall.append(float(re.search(wallTimePattern, err).group(1)))
                memory.append(int(re.search(memoryPattern, err).group(1)))
                out = output # Same every time, just overwrite it
            except AttributeError as e:
                print("ERROR: String values successfully parsed, but error during their parsing to numerical form occured.\n")
                raise   
            except IndexError:
                print("ERROR: Could not parse the output of ", cmd, " ; Check the parameters at the top of the scripts.", sep = '')
                raise

        # Process and print the gathered information
        def avg(lst):
            try:
                return sum(lst) / (float(len(lst)))
            except ZeroDivisionError:
                print("ERROR: List cannot be empty.")
                raise

        print("  No. of repeats: ", repeats, sep = '')
        print("  User time: ", avg(utime), sep = '')
        print("  System time: ", avg(stime), sep = '')
        print("  User + system time: ", avg(utime) + avg(stime), sep = '')
        print("  Total running time (wall clock time): ", avg(wall), sep = '')
        print("  Max. resident set size(kB): ", avg(memory), sep = '')
        print("  Output: ")
        print(out)
        print("-" * 20)
        print()

    print("#" * 20);
    print("#" * 20);
    print("#" * 20);
    print("#" * 20);
    print("#" * 20);
    print();

    # TODO: Add no. cores as an option
