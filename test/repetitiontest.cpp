#define CATCH_CONFIG_RUNNER  // This tells Catch to provide a main() - only do this in one cpp file
#include "unit/catch.hpp"
#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "../src/box/box.h"
#include "../src/function/function.h"
#include "../src/bisector/bisector.h"
#include "../src/degree/idegreecomputation.h"
#include "../src/solver/algorithm.h"

using namespace vytlalib::boxes;
using namespace vytlalib::bisector;
using namespace vytlalib::degree;
using namespace vytlalib::function;
using namespace vytlalib::solver;
using std::cout;
using std::endl;
using std::flush;

void runStressTest(double time);

int main( int argc, char* argv[] ) {
    bool stressTest = false;
    double timeLimit = 0;

    for (int i = 0; i < argc; ++i) {
        if (strcmp(argv[i], "--reptest") == 0) {
            ++i;
            if (i < argc) {
                timeLimit = std::stod(argv[i]);
                stressTest = true;
                break;
            }
        }
    }

    if (stressTest) {
        std::cout << "Running stress test\n------------------------------\n";
        srand (time(NULL));
        Component::init();
        runStressTest(timeLimit);
        Component::cleanup();
    } else {
        int result = Catch::Session().run( argc, argv );
    }
}



class TestCase {
public:
    // Input function to parse
    std::string func;
    // Variable domain
    UPtrBox varDomain;
    // Parameter domain
    UPtrBox parDomain;
    // Bisection threshold
    UPtrIBisector bisector;
    // Sub-face refinement threshold
    double faceThreshold;
    // capacity
    int capacity;

    virtual bool verify(std::vector<Component> &point, bool testBelongingToYes) const = 0;


};

class TestCase1 : public TestCase {
private:
    bool correctDim(std::vector<Component> &point) const {
        return point.size() == 2;
    }

public:
    TestCase1() {
        func = "[x y p q][p*x+3*y - 1, q*x + 2*y - 3]";
        varDomain.reset(new Box(2, Component("[0.75, 1]")));
        parDomain.reset(new Box(std::vector<Component> {Component("[-3, -1]"), Component("[1, 2]")}));
        bisector.reset(new Bisector(0.05));
        faceThreshold = 32;
        capacity = 64;
    }

    bool verify(std::vector<Component> &point, bool testBelongingToYes) const override {
        if (!correctDim(point))
            return false;

        const Component &p = point[0];
        const Component &q = point[1];

        if (2*p == 3*q)
            return !testBelongingToYes;

        const Component &x = varDomain->getComp(0);
        const Component &y = varDomain->getComp(1);

        Component xSol = Component("7") / (3*q - 2*p);
        Component ySol = (q - 3*p) / (3*q - 2*p);


        if (testBelongingToYes) { // test belonging to yesList
            // False, if [xSol, ySol] is guaranteed to be outside of the domain box
            bool xInside = false, yInside = false;
            xInside = !(xSol.upper() < x.lower() || xSol.lower() > x.upper());
            yInside = !(ySol.upper() < y.lower() || ySol.lower() > y.upper());
            return xInside && yInside;
        } else { // test belonging to noList
            // False, if [xSol, ySol] is guaranteed to be inside of the domain box
            bool xOutside = false, yOutside = false;
            xOutside = !(x.lower() <= xSol.lower() && xSol.upper() <= x.upper());
            yOutside = !(y.lower() <= ySol.lower() && ySol.upper() <= y.upper());
            return xOutside || yOutside;
        }
    }
};

bool isTimeUp(clock_t start, clock_t end, double limitInSeconds) {
    return (double) (end - start) / CLOCKS_PER_SEC > limitInSeconds;
}

std::vector<Component> pointFromBox(const Box& box) {
    std::vector<Component> result;
    for (int i = 0; i < box.getSize(); ++i) {
        double step = (double) rand() / RAND_MAX;
        const Component &c = box.getComp(i);
        result.push_back(Component(c.lower() + step * (c.upper() - c.lower())));
    }
    return result;
}

bool stress(TestCase &testCase, BoxList &yesBoxes, BoxList &noBoxes, double time) {
    clock_t start = clock();

    std::vector<Box> yes;
    for (Box& box : yesBoxes)
        yes.push_back(box);

    std::vector<Box> no;
    for (Box& box : noBoxes)
        no.push_back(box);

    long yesMismatch = 0, yesMatch = 0;
    long noMismatch = 0, noMatch = 0;
    while (!isTimeUp(start, clock(), time)) {
        for (int i = 0; i < 10; ++i) {
            int boxType = rand() % 2;
            if (boxType == 0) {
                // yesBox
                int boxIndex = rand() % yes.size();
                auto point = pointFromBox(yes[boxIndex]);
                if (!testCase.verify(point, true)) {
                    yesMismatch++;
                } else {
                    yesMatch++;
                }
            } else {
                // noBox
                int boxIndex = rand() % no.size();
                auto point = pointFromBox(no[boxIndex]);
                if (!testCase.verify(point, false)) {
                    noMismatch++;
                } else {
                    noMatch++;
                }
            }
        }
    }
    cout << "Yesboxes matches: " << yesMatch << endl;
    cout << "Yesboxes mismatches: " << yesMismatch << endl;
    cout << "Noboxes matches: " << noMatch << endl;
    cout << "Noboxes mismatches: " << noMismatch << endl;
    cout << "Result: " << (yesMismatch + noMismatch == 0 ? "SUCCESS" : "FAIL")
         << endl << endl << flush;
    return yesMismatch + noMismatch == 0;
}

void runStressTest(double time) {
    typedef std::unique_ptr<TestCase> UPtrTestCase;
    typedef std::unique_ptr<Solver> UPtrSolver;
    std::vector<UPtrTestCase> suite;
    std::vector<UPtrSolver> solvers;

    // Fill inputs
    suite.push_back(UPtrTestCase(new TestCase1()));
    solvers.push_back(UPtrSolver(new SplitAndKeepFrameSolver()));
    solvers.push_back(UPtrSolver(new TreeFrameSolver()));
    solvers.push_back(UPtrSolver(new GridFrameSolver()));

    Degree dc;
    int success = 0, fail = 0;
    for (UPtrTestCase& testCase : suite) {
        cout << "##################################" << endl;
        cout << "Input function: " << testCase->func << endl;
        cout << "Domain box: " << *testCase->varDomain << endl;
        cout << "Param box: " << *testCase->parDomain << endl;
        for (UPtrSolver& solver : solvers) {
            // Solve the problem first
            auto func = IFunction::parseRecursive(testCase->func);
            BoxList yesBoxes, noBoxes;
            solver->solve(*testCase->varDomain, *testCase->parDomain, *testCase->bisector,
                      testCase->faceThreshold, testCase->capacity, *func, dc, yesBoxes, noBoxes);
            cout << "Got solution with solver of type: " << (typeid(*solver).name()) << endl;
            cout << "Stress testing now ..." << endl << flush;
            if (stress(*testCase, yesBoxes, noBoxes, time)) {
                success++;
            } else {
                fail++;
            }
        }
    }
    cout << "-------------------------------------\n";
    cout << "Summary:\nSucess: " << success << "\nFail: " << fail << endl;
    if (fail > 0) {
        cout << "FAIL" << endl;
    } else {
        cout << "SUCCESS" << endl;
    }
}


