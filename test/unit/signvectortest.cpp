#include "catch.hpp"
#include "../../src/component.h"
#include "../../src/box/box.h"

/* Tests the creation, ma-
nipulation and property setting/accessing of the sign vector implement-
ation.
 */

namespace vytlalib {
using namespace boxes;


TEST_CASE("Sign operations", "[signvec]") {
    SECTION("Integer power") {
        CHECK(pow(Sign::PLUS, 2) == Sign::PLUS);
        CHECK(pow(Sign::PLUS, 1) == Sign::PLUS);
        CHECK(pow(Sign::PLUS, 0) == Sign::PLUS);
        CHECK(pow(Sign::PLUS, -1) == Sign::PLUS);
        CHECK(pow(Sign::PLUS, -2) == Sign::PLUS);

        CHECK(pow(Sign::ZERO, 2) == Sign::ZERO);
        CHECK(pow(Sign::ZERO, 1) == Sign::ZERO);
        CHECK(pow(Sign::ZERO, 0) == Sign::ZERO);
        CHECK(pow(Sign::ZERO, -1) == Sign::ZERO);
        CHECK(pow(Sign::ZERO, -2) == Sign::ZERO);

        CHECK(pow(Sign::MINUS, 2) == Sign::PLUS);
        CHECK(pow(Sign::MINUS, 1) == Sign::MINUS);
        CHECK(pow(Sign::MINUS, 0) == Sign::PLUS);
        CHECK(pow(Sign::MINUS, -1) == Sign::MINUS);
        CHECK(pow(Sign::MINUS, -2) == Sign::PLUS);
    }
}

TEST_CASE("Construction", "[signvec]") {
    Sign n = boxes::Sign::MINUS;
    Sign p = boxes::Sign::PLUS;
    Sign z = boxes::Sign::ZERO;
    CHECK(SignVector(4).getSize() == 4);
    CHECK(SignVector(4)[0] == z);
    CHECK(SignVector(4)[2] == z);
    CHECK(SignVector(3, n)[0] == n);
    CHECK(SignVector(3, n)[2] == n);
    CHECK(SignVector({p, z, n})[0] == p);
    CHECK(SignVector({p, z, n})[1] == z);
    CHECK(SignVector({p, z, n})[2] == n);
    CHECK_THROWS(SignVector(4)[5]);
}

TEST_CASE("Determinations", "[signvec]") {
    Sign n = boxes::Sign::MINUS;
    Sign p = boxes::Sign::PLUS;
    Sign z = boxes::Sign::ZERO;

    CHECK_FALSE(SignVector(4).isSufficient());
    CHECK_FALSE(SignVector(4, Sign::ZERO).isSufficient());
    CHECK_FALSE(SignVector(0).isSufficient());
    CHECK_FALSE(SignVector(0, Sign::PLUS).isSufficient());
    CHECK(SignVector(4, Sign::PLUS).isSufficient());
    CHECK(SignVector(4, Sign::MINUS).isSufficient());
    SignVector sl(4, Sign::ZERO);
    sl[2] = Sign::MINUS;
    CHECK(sl.isSufficient());
    CHECK(SignVector({p, n, p, n}).isSufficient());
    CHECK_FALSE(SignVector({z, z, z, z}).isSufficient());
}

}
