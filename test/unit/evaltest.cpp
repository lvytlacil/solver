#include "catch.hpp"
#include <string>
#include <iostream>
#include <vector>
#include "../../src/box/box.h"
#include "../../src/function/function.h"
#include "../../src/function/functionevaluator.h"

/* Tests the correctness of our imple-
mentation of our function interpreters against gaol numerical expression
evaluation capability
 */

namespace vytlalib {
using std::string;
using std::stringstream;
using function::IFunction;
using function::Function;
using function::ByteCodeFunction;
using evaluator::StandardEvaluator;
using boxes::Box;
using std::cout;
using std::endl;

static auto& emptyBox = boxes::EMPTY_BOX;


// Wraps the expression "exp" into "[] [exp]"
static string wrap(std::string in) {
    std::stringstream ss;
    ss << "[] " << "[" << in << ";]";
    return ss.str();
}

// Takes the input gaol expression and wraps it to match our parser / interpreter
static void performComparison(const Box& ourResult, std::string gaolInput) {
    REQUIRE(ourResult.getSize() == 1);
    gaol::interval actual = gaol::interval(ourResult.getComp(0).lower(), ourResult.getComp(0).upper());
    CHECK(actual.set_eq(gaol::interval(gaolInput.c_str())));
}


/**
 * @brief Tests scalar expressions without variables.
 *
 * Note, that our interpreters are designed, so that n-th square root for odd n is
 * defined on R, not just on [0, +inf]. To our knowledge, gaol input parser
 * does not offer a way to perform odd square root like this, so we test such case like
 * the following example:
 * Our evaluation of sqrt3(-pi) is compared to gaol evaluation of -nth_root(pi, 3)
 */
TEST_CASE("Numerical expressions", "[eval]") {
    std::vector<std::pair<string, string> > inputs {
        std::make_pair("4", ""),
        std::make_pair("-4", ""),
        std::make_pair("-+-4", ""),
        std::make_pair("4.0", ""),
        std::make_pair("15.2 + 19.4", ""),
        std::make_pair("15.2 - 19.4", ""),
        std::make_pair("15.2 * 19.4", ""),
        std::make_pair("15.2 / 19.4", ""),
        std::make_pair("8 / 5", ""),
        std::make_pair("15.2 * 19.4", ""),
        std::make_pair("sqr(15.2)", "pow(15.2, 2)"),
        std::make_pair("0.1 ^ 5", "pow(0.1, 5)"),
        std::make_pair("0.11^-2", "pow(0.11, -2)"),
        std::make_pair("0.11^-2.11", "pow(0.11, -2.11)"),
        std::make_pair("3.1^4.7", "exp(4.7 * log(3.1))"),
        std::make_pair("[1, 2] + [-1, 1] + [-2, +1]", ""),
        std::make_pair("sin(2) + cos(-2)", ""),
        std::make_pair("asin(.2) + acos(-.2)", ""),
        std::make_pair("tan(3) + atan(-1.11)", ""),
        std::make_pair("pi", ""),
        std::make_pair("e + exp(1)", "exp(1) + exp(1)"),
        std::make_pair("log(pi) + exp(pi)", ""),
        std::make_pair("abs(-4) + abs(0) + abs(exp(1))", "4 + 0 + exp(1)"),
        std::make_pair("2 + 2^2 + 2^3", "2 + pow(2, 2) + pow(2, 3)"),
        std::make_pair("2^(pi + 2.4)", "pow(2, pi + 2.4)"),
        std::make_pair("sqrt(11) + sqrt(pi)", ""),
        std::make_pair("sqrt(0) + sqrt(-2)", ""),
                std::make_pair("sqrt3(-pi)", "-nth_root(pi, 3)"),
        std::make_pair("sqrt3(1) + sqrt3(10.77)", "nth_root(1, 3) + nth_root(10.77, 3)"),
                std::make_pair("sqrt3(4^2 - 1.23^2*2)", "nth_root( pow(4, 2) - pow(1.23, 2)*2, 3)"),
        std::make_pair("sqrt2(pi) + sqrt4(pi)", "nth_root(pi, 2) + nth_root(pi, 4)"),
        std::make_pair("sqr([4.1, 4.11]) - sqr([2.4, 2.41]) - sqr([6.998,7.001])",
                       "pow([4.1, 4.11], 2) - pow([2.4, 2.41], 2) - pow([6.998,7.001], 2)"),
    };

    SECTION("Common function") {
        for (auto exp : inputs) {
            if (exp.second.empty())
                exp.second = exp.first;
            {
                INFO(exp.first)
                Box ourResult = IFunction::parseRecursive(wrap(exp.first))->eval(boxes::EMPTY_BOX);
                performComparison(ourResult, exp.second);
            }
        }
    }

    SECTION("Bytecode function") {
        for (auto exp : inputs) {
            if (exp.second.empty())
                exp.second = exp.first;
            {
                INFO(exp.first)
                Box ourResult = IFunction::parseLinear(wrap(exp.first))->eval(boxes::EMPTY_BOX);
                performComparison(ourResult, exp.second);
            }
        }
    }

    SECTION("Bytecode cached function") {
        for (auto exp : inputs) {
            if (exp.second.empty())
                exp.second = exp.first;
            {
                INFO(exp.first)
                Box ourResult = IFunction::parseLinearCached(wrap(exp.first))->eval(boxes::EMPTY_BOX);
                performComparison(ourResult, exp.second);
            }
        }
    }
}

struct Triplet {
    std::string ourExpr;
    boxes::Box domain;
    std::string gaolExpr;

    Triplet(std::string ourExpr, boxes::Box domain, std::string gaolExpr):
        ourExpr(ourExpr), domain(domain), gaolExpr(gaolExpr) { }
};

TEST_CASE("Expressions with variables", "[eval]") {
    std::vector<Triplet> inputs {
        Triplet("[x][x + 3*x;]", boxes::Box(1, Component("[1, 2]")), "[1, 2] + [3] * [1, 2]"),
        Triplet("[x][sin(tan(x)) + exp(pi * [1, 2]);]", boxes::Box(1, Component("[-1, 1]")),
                "sin(tan([-1, 1])) + exp([pi] * [1, 2])"),
        Triplet("[x][x / 2 + log(x);]", boxes::Box(1, Component("[.25, .35]")),
                "[.25, .35] / [2] + log([.25, .35])")
    };

    SECTION("Common function") {
        for (Triplet& triplet : inputs) {
            {
                INFO(triplet.ourExpr);
                Box ourResult = IFunction::parseRecursive(triplet.ourExpr)->eval(triplet.domain);
                performComparison(ourResult, triplet.gaolExpr);
            }
        }
    }
}

}
