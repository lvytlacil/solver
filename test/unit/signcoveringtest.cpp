#include <iostream>
#include "catch.hpp"
#include "../../src/function/function.h"
#include "../../src/box/box.h"
#include "../../src/bisector/bisector.h"

/* Verifies the contract
of the implementation of IFunction::computeSignCovering for computing sign coverings, which
is the core of performing the numerical phase of the topological degree computation.
 */

namespace vytlalib {

using namespace boxes;
using namespace function;
using namespace bisector;

// Checks if the given sign covering is with respect to the given function
bool isWithRespectToFunc(IFunction &func, const Box &paramBox, const SignCoveringList &sc) {
    for (auto& boxvec : sc) {
        Box image = func.eval(boxvec.first, paramBox);
        if (image.computeSignVector() != boxvec.second)
            return false;
    }
    return true;
}

// A simple batch of checks for a sign covering
void testcovering(const SignCoveringList &sc,
                  bool expectedSufficient, IFunction &func, const Box &paramBox) {
    CHECK(boxes::isSignCoveringListDetermined(sc) == expectedSufficient);
    CHECK(isWithRespectToFunc(func, paramBox, sc));
}

TEST_CASE("Sign coverings of a given list of boxes", "[signcovering]") {
    // Identity in R3
    auto f1 = IFunction::parseLinear("[x y z][x; y; z]");
    auto f2 = IFunction::parseLinear("[x y][x^2 + y^2 - 1; 1.5*x - y]");
    Box b1(3, Component("[4, 5]"));
    Box b2(3, Component("[-1, 1]"));
    Box b3(3, Component("[-2, -1]"));
    Box b4(2, Component("[-1, 1]"));

    SECTION("If no F(B) constains zero, sign covering is sufficient and no box is bisected") {
        auto cov = f1->computeSignCovering(b1, boxes::EMPTY_BOX, Bisector (.1, .5));
        CHECK(cov.size() == 1);
        testcovering(cov, true, *f1, boxes::EMPTY_BOX);

        // Pass a list of boxes as the input
        BoxList boxList = {b3};
        cov = f1->computeSignCovering(boxList, boxes::EMPTY_BOX, Bisector (.1, .5));
        CHECK(cov.size() == 1);
        testcovering(cov, true, *f1, boxes::EMPTY_BOX);

        boxList = {b1, b3, b1};
        cov = f1->computeSignCovering(boxList, boxes::EMPTY_BOX, Bisector (.1, .5));
        CHECK(cov.size() == 3);
        testcovering(cov, true, *f1, boxes::EMPTY_BOX);
    }

    SECTION("If f(B) contains zero for some B, sign covering is never sufficient") {
        auto cov = f1->computeSignCovering(b2, boxes::EMPTY_BOX, Bisector (.1, .5));
        testcovering(cov, false, *f1, boxes::EMPTY_BOX);

        // Smaller bisection threshold
        cov = f1->computeSignCovering(b2, boxes::EMPTY_BOX, Bisector(.001, .5));
        testcovering(cov, false, *f1, boxes::EMPTY_BOX);

        // Pass a list of boxes as the input
        BoxList boxList = {b2};
        cov = f1->computeSignCovering(boxList, boxes::EMPTY_BOX, Bisector (.1, .5));
        testcovering(cov, false, *f1, boxes::EMPTY_BOX);

        boxList = {b1, b3, b2, b3};
        cov = f1->computeSignCovering(boxList, boxes::EMPTY_BOX, Bisector (.1, .5));
        testcovering(cov, false, *f1, boxes::EMPTY_BOX);
    }

    SECTION ("If no f(B) contains zero, but some F(B) does because of overestimation."
             " Result depends on the bisection width threshold.") {
        BoxList bnd = b4.computeInducedBoundary();

        SECTION("Insufficient bisection width threshold") {
            auto cov = f2->computeSignCovering(bnd, boxes::EMPTY_BOX, Bisector (10, .5));
            testcovering(cov, false, *f2, boxes::EMPTY_BOX);
        }

        SECTION("Sufficient bisection width threshold") {
            auto cov = f2->computeSignCovering(bnd, boxes::EMPTY_BOX, Bisector (0.01, .5));
            testcovering(cov, true, *f2, boxes::EMPTY_BOX);
        }
    }
}

}
