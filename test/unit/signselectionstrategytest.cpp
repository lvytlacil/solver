#include <utility>
#include <iostream>
#include "catch.hpp"
#include "../../src/box/box.h"
#include "../../src/degree/idegreecomputation.h"

/* Tests the im-
plemented strategies for selecting the sign and index during the com-
binatorial phase of the topological degree computation.
*/

namespace vytlalib {

using degree::DefaultSelectionStrategy;
using degree::LeastFrequentSelectionStrategy;
using namespace boxes;
using namespace std;

TEST_CASE("Invalid inputs", "[signstrategy]") {
    DefaultSelectionStrategy s;
    LeastFrequentSelectionStrategy ls;
    SignCoveringList emptyCovering;
    CHECK_THROWS(s.selectSign(emptyCovering));
    CHECK_THROWS(ls.selectSign(emptyCovering));
}

TEST_CASE("Valid inputs", "[signstrategy]") {
    // dummy box
    Box b(2, Component(-1, 1));
    Sign p = Sign::PLUS;
    Sign m = Sign::MINUS;
    Sign z = Sign::ZERO;
    DefaultSelectionStrategy def;
    LeastFrequentSelectionStrategy least;

    SignCoveringList sl1 {
        std::make_pair(b, SignVector({p, m, z})),
        std::make_pair(b, SignVector({p, m, p})),
        std::make_pair(b, SignVector({m, p, z})),
        std::make_pair(b, SignVector({m, p, m})),
    };

    SignCoveringList sl2 {
        std::make_pair(b, SignVector({p, m, z})),
        std::make_pair(b, SignVector({p, z, p})),
        std::make_pair(b, SignVector({m, p, z})),
        std::make_pair(b, SignVector({m, z, m})),
    };

    SignCoveringList sl3 {
        std::make_pair(b, SignVector({p, m, z})),
        std::make_pair(b, SignVector({p, m, p})),
        std::make_pair(b, SignVector({m, p, p})),
        std::make_pair(b, SignVector({m, z, p})),
    };

    SECTION("Least frequent strategy") {
        INFO("One PLUS at index 2 (PLUS is prefered over MINUS)");
        CHECK(least.selectSign(sl1) == std::make_pair(2, p));
        INFO("One PLUS at index 1 (lower indices are prefered)");
        CHECK(least.selectSign(sl2) == std::make_pair(1, p));
        INFO("No MINUS at index 2");
        CHECK(least.selectSign(sl3) == std::make_pair(2, m));
    }

    SECTION("Default strategy") {
        INFO("Simply returns (0, PLUS) by default");
        CHECK(least.selectSign(sl1) == std::make_pair(2, p));
        INFO("Simply returns (0, PLUS) by default");
        CHECK(least.selectSign(sl2) == std::make_pair(1, p));
        INFO("Simply returns (0, PLUS) by default");
        CHECK(least.selectSign(sl3) == std::make_pair(2, m));
    }
}


}
