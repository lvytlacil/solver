#include "catch.hpp"
#include "../../src/box/box.h"
#include <tuple>
#include <vector>
#include <algorithm>

/* Tests for verifying the
functionality of basic properties of our implementation of boxes and ori-
ented boxes and related operations like computing faces, oriented bound-
ary etc.
*/

using namespace vytlalib::boxes;

/*------- Helper methods -------*/
std::ostream& operator<<(std::ostream &out, const std::list<Box> &orientedBoundary) {
    for (auto box : orientedBoundary)
        out << box;
    return out;
}

/*------- Custom matchers -------*/

/**
 * @brief Matcher used to test oriented boundary calculation.
 */
class OrientedBoundaryMatcher : public Catch::MatcherBase<Box> {
private:
    std::vector<Box> expected_;
public:
    OrientedBoundaryMatcher(const std::vector<Box> &expected):
        expected_(expected) { }

    virtual bool match(const Box &box) const override {
        BoxList actual = box.computeInducedBoundary();

        std::list<Box> actualValues;
        for (const auto& it : actual) {
            actualValues.push_back(Box(it));
        }


        bool sizeMatch = actualValues.size() == expected_.size();
        return sizeMatch && std::is_permutation(actualValues.begin(), actualValues.end(),
                expected_.begin());
    }

    virtual std::string describe() const {
        std::ostringstream ss;
        ss << "has oriented boundary:\n";
        for (auto face : expected_) {
            ss << face << std::endl;
        }
        return ss.str();
    }
};

inline OrientedBoundaryMatcher hasGivenOrientedBoundary(const std::vector<Box> &expected) {
    return OrientedBoundaryMatcher(expected);
}

/*------- Test cases -----------*/

TEST_CASE("Box -- Construction", "[box]") {
    double lo1 = -1.0, hi1 = 1.5;
    double lo2 = -2.0, hi2 = -1.0;

    SECTION("Constructing with a single component to all dimensions.") {
        Box box(3, Component(lo1, hi1));
        CHECK(box.getComp(0).lower() == lo1);
        CHECK(box.getComp(1).upper() == hi1);
        CHECK(box.getSize() == 3);
        CHECK(box.getOrientation() == BoxOrientation::POSITIVE);
    }

    SECTION("Copy of a box") {
        Box box(3, Component(lo1, hi1));
        box = Box(box);
        CHECK(box.getComp(0).lower() == lo1);
        CHECK(box.getComp(1).upper() == hi1);
        CHECK(box.getSize() == 3);
        CHECK(box.getOrientation() == BoxOrientation::POSITIVE);
    }

    SECTION("Explicitly given orientation") {
        CHECK(Box(3, Component(lo1, hi1), BoxOrientation::NEGATIVE).getOrientation()
              == BoxOrientation::NEGATIVE);
        CHECK(Box(3, Component(lo1, hi1), BoxOrientation::POSITIVE).getOrientation()
              == BoxOrientation::POSITIVE);
    }

    SECTION("Empty box") {
        CHECK(Box().getSize() == 0);
    }

    SECTION("Illegal size during construction") {
        CHECK_THROWS(Box(-1, Component(lo1, hi1)));
    }
}

TEST_CASE("Box mutations", "[box]") {
    Component c1 = Component(-3.2, 5.4);
    Component c2 = Component(-3.1, 32.2);
    Box box(3, c1);

    SECTION("Getting after construction") {
        CHECK(box.getComp(0) == c1);
        CHECK(box.getComp(2) == c1);
        CHECK(box.getOrientation() == BoxOrientation::POSITIVE);
    }

    SECTION("Setting and getting orientation") {
        CHECK(box.getOrientation() == BoxOrientation::POSITIVE);
        box.setOrientation(BoxOrientation::POSITIVE);
        CHECK(box.getOrientation() == BoxOrientation::POSITIVE);
        box.setOrientation(BoxOrientation::NEGATIVE);
        CHECK(box.getOrientation() == BoxOrientation::NEGATIVE);
        box.setOrientation(BoxOrientation::NEGATIVE);
        CHECK(box.getOrientation() == BoxOrientation::NEGATIVE);
    }

    SECTION("Setting and getting components") {
        box.setComp(0, c1);
        CHECK(box.getComp(0) == c1);
        box.setComp(0, c2);
        CHECK(box.getComp(0) == c2);
        box.setComp(0, c2);
        CHECK(box.getComp(0) == c2);
        box.setComp(1, c1);
        box.setComp(1, c2);
        CHECK(box.getComp(1) == c2);
    }

    SECTION("Accessing components outside of bounds") {
        CHECK_THROWS(box.getComp(-1));
        CHECK_THROWS(box.getComp(4));
        CHECK_THROWS(box.getComp(125));
        CHECK_THROWS(box.setComp(-1, c1));
        CHECK_THROWS(box.setComp(4, c1));
        CHECK_THROWS(box.setComp(125, c1));
    }
}

TEST_CASE("BoxOrientation -- operations", "[box]") {
    BoxOrientation positive(BoxOrientation::POSITIVE), negative(BoxOrientation::NEGATIVE);
    CHECK(positive == BoxOrientation::POSITIVE);
    CHECK(negative == BoxOrientation::NEGATIVE);

    SECTION("Multiplication") {
        CHECK(positive * positive == BoxOrientation::POSITIVE);
        CHECK(negative * negative == BoxOrientation::POSITIVE);
        CHECK(positive * negative == BoxOrientation::NEGATIVE);
        CHECK(negative * positive == BoxOrientation::NEGATIVE);
        CHECK(negative * negative * positive * negative * positive == BoxOrientation::NEGATIVE);
        CHECK(negative * negative * negative * negative * positive == BoxOrientation::POSITIVE);
    }

    SECTION("Negation") {
        CHECK((!positive) == BoxOrientation::NEGATIVE);
        CHECK((!negative) == BoxOrientation::POSITIVE);
        CHECK((!!positive) == BoxOrientation::POSITIVE);
        CHECK((!!negative) == BoxOrientation::NEGATIVE);
    }
}

TEST_CASE("Computed properties", "[box]") {

    SECTION("Longest dimension") {
        std::vector<Component> components {Component(1, 3), Component(-4, 5), Component(232, 233)};
        Box box(components);
        CHECK(box.getLongestDim() == 1);

        // All degenerated
        box = Box(4, Component(4.2, 4.2));
        CHECK(box.getLongestDim() == -1);
    }

    SECTION("Topological dimension") {
        Box box(7, Component(10, 15));
        CHECK(Box(7, Component(10, 12)).getSize() ==
              Box(7, Component(10, 12)).getTopologicalDimension());
        CHECK(Box(7, Component(10, 10)).getTopologicalDimension() == 0);
    }

    SECTION("Zero containtment") {
        CHECK(Box(2, Component("[-1, 1]")).containsZero());
        CHECK(Box(2, Component("[0, 1]")).containsZero());
        CHECK(Box(2, Component("[0, 0]")).containsZero());

        Component c1("[-1, -0.5]");
        Component c2("[-1, 1]");
        CHECK_FALSE(Box(2, c1).containsZero());
        CHECK_FALSE(Box({c1, c2}).containsZero());
    }
}

TEST_CASE("Subset and sub-box relations", "[box]") {
    Box box(4, Component(-3.2, 11.0));
    Box deg1(box);
    deg1.collapseToLower(0);
    deg1.collapseToLower(1);
    deg1.collapseToLower(2);
    deg1.collapseToLower(3);
    Box deg2(4, Component(5.0, 5.0));
    Box other(box);

    SECTION("Subset relation") {
        // Positive cases
        CHECK(Box(4, Component(-1, 1)).isSubsetOf(box));
        CHECK(box.isSubsetOf(box));

        // Degenerated boxes
        CHECK(deg1.isSubsetOf(box));
        CHECK(deg2.isSubsetOf(box));
        CHECK(deg1.isSubsetOf(deg1));
        CHECK_FALSE(box.isSubsetOf(deg1));

        // Negative cases
        other.setComp(2, Component(-5, 12));
        CHECK_FALSE(other.isSubsetOf(box));
        other.setComp(2, Component(-5, 11.0));
        CHECK_FALSE(other.isSubsetOf(box));
        other.setComp(2, Component(-3.2, 12));
        CHECK_FALSE(other.isSubsetOf(box));
        CHECK_FALSE(Box(3, Component(-1, 1)).isSubsetOf(box));
    }

    SECTION("Subbox relation") {
        // Positive cases
        CHECK(Box(4, Component(-1, 1)).isSubboxOf(box));
        CHECK(box.isSubboxOf(box));

        // Degenerated boxes
        CHECK_FALSE(deg1.isSubboxOf(box));
        CHECK_FALSE(deg2.isSubboxOf(box));
        CHECK(deg1.isSubboxOf(deg1));
        CHECK_FALSE(box.isSubboxOf(deg1));

        // Size > topological dimension
        Box b1 = Box(4, Component(0.25, 0.75));
        Box b2(b1);
        b2.collapseToLower(1);
        Box b3(b1);
        b3.collapseToLower(2);
        Box b4(b1);
        b4.collapseToUpper(2);

        // Degenerated component mismatch on index
        CHECK_FALSE(b2.isSubboxOf(b3));
        // Degenerated component match on index, but different value
        CHECK_FALSE(b3.isSubboxOf(b4));
        // Match of degenerated components
        b1.collapseToLower(1);
        CHECK(b1.isSubboxOf(b2));
    }
}

TEST_CASE("Semantic comparison", "[box]") {
    typedef std::vector<Component> cv;
    cv c1 {Component(-1, 1), Component(2, 4)};
    cv c2 {Component(-1, 1), Component(2, 4), Component(11, 12)};
    cv c3 {Component(11, 12), Component(-1, 1), Component(2, 4)};

    CHECK(Box() == Box());
    CHECK(Box(c1) == Box(c1));
    CHECK(Box(c1, BoxOrientation::POSITIVE) == Box(c1, BoxOrientation::POSITIVE));
    CHECK_FALSE(Box(c1, BoxOrientation::POSITIVE) == Box(c1, BoxOrientation::NEGATIVE));
    CHECK_FALSE(Box(c1, BoxOrientation::POSITIVE) == Box(c2, BoxOrientation::POSITIVE));
    CHECK_FALSE(Box(c1, BoxOrientation::NEGATIVE) == Box(c3, BoxOrientation::NEGATIVE));

    CHECK_FALSE(Box() != Box());
    CHECK_FALSE(Box(c1) != Box(c1));
    CHECK_FALSE(Box(c1, BoxOrientation::POSITIVE) != Box(c1, BoxOrientation::POSITIVE));
    CHECK(Box(c1, BoxOrientation::POSITIVE) != Box(c1, BoxOrientation::NEGATIVE));
    CHECK(Box(c1, BoxOrientation::POSITIVE) != Box(c2, BoxOrientation::POSITIVE));
    CHECK(Box(c1, BoxOrientation::NEGATIVE) != Box(c3, BoxOrientation::NEGATIVE));
}

TEST_CASE("Calculating oriented boundary", "[box]") {
    Component b1(-1.0, 2), b2(2.0, 4.0), b3(-1, 10);
    std::vector<Component> parentComponents {b1, b2, b3};
    Component b1D(b1.lower(), b1.lower());
    Component b1U(b1.upper(), b1.upper());
    Component b2D(b2.lower(), b2.lower());
    Component b2U(b2.upper(), b2.upper());
    Component b3D(b3.lower(), b3.lower());
    Component b3U(b3.upper(), b3.upper());


    SECTION("Parent box with positive orientation, boundaries non trivial.") {
        std::vector<Box> expected;
        expected.push_back(Box(std::vector<Component> {b1D, b2, b3}, BoxOrientation::NEGATIVE));
        expected.push_back(Box(std::vector<Component> {b1U, b2, b3}, BoxOrientation::POSITIVE));

        expected.push_back(Box(std::vector<Component> {b1, b2D, b3}, BoxOrientation::POSITIVE));
        expected.push_back(Box(std::vector<Component> {b1, b2U, b3}, BoxOrientation::NEGATIVE));

        expected.push_back(Box(std::vector<Component> {b1, b2, b3D}, BoxOrientation::NEGATIVE));
        expected.push_back(Box(std::vector<Component> {b1, b2, b3U}, BoxOrientation::POSITIVE));

        CHECK_THAT(Box(parentComponents, BoxOrientation::POSITIVE), hasGivenOrientedBoundary(expected));
    }

    SECTION("Parent box with negative orientation, boundaries non trivial.") {
        std::vector<Box> expected;

        expected.push_back(Box(std::vector<Component> {b1D, b2, b3}, BoxOrientation::POSITIVE));
        expected.push_back(Box(std::vector<Component> {b1U, b2, b3}, BoxOrientation::NEGATIVE));

        expected.push_back(Box(std::vector<Component> {b1, b2D, b3}, BoxOrientation::NEGATIVE));
        expected.push_back(Box(std::vector<Component> {b1, b2U, b3}, BoxOrientation::POSITIVE));

        expected.push_back(Box(std::vector<Component> {b1, b2, b3D}, BoxOrientation::POSITIVE));
        expected.push_back(Box(std::vector<Component> {b1, b2, b3U}, BoxOrientation::NEGATIVE));

        CHECK_THAT(Box(parentComponents, BoxOrientation::NEGATIVE), hasGivenOrientedBoundary(expected));
    }

    SECTION("Parent box with positive orientation, Component with index 1 trivial.") {
        parentComponents[1] = b2D;
        std::vector<Box> expected;

        expected.push_back(Box(std::vector<Component> {b1D, b2D, b3}, BoxOrientation::NEGATIVE));
        expected.push_back(Box(std::vector<Component> {b1U, b2D, b3}, BoxOrientation::POSITIVE));

        expected.push_back(Box(std::vector<Component> {b1, b2D, b3D}, BoxOrientation::POSITIVE));
        expected.push_back(Box(std::vector<Component> {b1, b2D, b3U}, BoxOrientation::NEGATIVE));

        CHECK_THAT(Box(parentComponents, BoxOrientation::POSITIVE), hasGivenOrientedBoundary(expected));
    }
}
