#include "catch.hpp"
#include <string>
#include <iostream>
#include <vector>
#include "../../src/parser/scanner.h"

/* Verifies, that inputs (expres-
sions, function definitions) are scanned into correct sequences of tokens.
 */

namespace vytlalib {
using std::string;
using std::stringstream;
using input::Input;
using scanner::Scanner;
using scanner::Token;
using scanner::TokenType;
using std::cout;
using std::endl;

/**
 * @brief Matcher that stores a sequence of tokens and accepts string,
 * which it scans with Scanner until first eof token is encountered, and then
 * compares with the stored sequence. NOTE: The encountered eof is dropped.
 */
class TokenSequence : public Catch::MatcherBase<string> {
private:
    std::vector<Token> tokens_;

public:
    TokenSequence(const std::vector<Token> &tokens):
        tokens_(tokens) { }

    virtual bool match(const string &inputString) const override {
        std::stringstream ss(inputString);
        std::unique_ptr<Input> in(new Input(ss));
        Scanner scanner(std::move(in));

        std::vector<Token> actualTokens;

        Token actual = Token(TokenType::eof);
        while ( (actual = scanner.scanNext()).getType() != TokenType::eof) {
            actualTokens.push_back(actual);
            // Avoid stucking in a loop if some error prevents from reaching eof
            if (actualTokens.size() > tokens_.size())
                break;
        }

        if (tokens_.size() != actualTokens.size())
            return false;

        bool result = true;
        for (size_t i = 0; i < actualTokens.size(); ++i) {

            if (actualTokens[i] != tokens_[i])
                result = false;

        }

        return result;
    }

    virtual std::string describe() const {
        std::ostringstream ss;
        ss << "is tokenized as expected into: ";
        for (Token token : tokens_) {
            ss << "{" << token << "}";
        }
        return ss.str();
    }
};

inline TokenSequence isTokenizedInto(const std::vector<Token> &tokens) {
    return TokenSequence(tokens);
}

TEST_CASE("Simple expressions", "[scanner]") {
    string input = "x + y - 12";
    SECTION(input.c_str()) {
        std::vector<Token> expected {Token(TokenType::identifier, "x"),
                    Token(TokenType::plus),
                    Token(TokenType::identifier, "y"),
                    Token(TokenType::minus),
                    Token(TokenType::number, "12")};
        CHECK_THAT(input, isTokenizedInto(expected));
    }

    input = "sin(x) + cos(y) + 12.4; tan(x*y)";
    SECTION(input.c_str()) {
        std::vector<Token> expected {Token(TokenType::sin),
                    Token(TokenType::lparen),
                    Token(TokenType::identifier, "x"),
                    Token(TokenType::rparen),
                    Token(TokenType::plus),
                    Token(TokenType::cos),
                    Token(TokenType::lparen),
                    Token(TokenType::identifier, "y"),
                    Token(TokenType::rparen),
                    Token(TokenType::plus),
                    Token(TokenType::number, "12.4"),
                    Token(TokenType::semicolon),
                    Token(TokenType::tan),
                    Token(TokenType::lparen),
                    Token(TokenType::identifier, "x"),
                    Token(TokenType::mul),
                    Token(TokenType::identifier, "y"),
                    Token(TokenType::rparen),
                    };
        CHECK_THAT(input, isTokenizedInto(expected));
    }

    input = "sqrt4 sqrt10 sqrt 10   sqrt10.4   sqrt10ahoj";
    SECTION(input.c_str()) {
        std::vector<Token> expected {Token(TokenType::sqrtn, "4"),
                    Token(TokenType::sqrtn, "10"),
                    Token(TokenType::sqrt),
                    Token(TokenType::number, "10"),
                    Token(TokenType::sqrtn, "10"),
                    Token(TokenType::number, ".4"),
                    Token(TokenType::identifier, "sqrt10ahoj")
                    };
        CHECK_THAT(input, isTokenizedInto(expected));
    }

    input = "cos(12.2)";
    SECTION(input.c_str()) {
        std::vector<Token> expected {Token(TokenType::cos),
                    Token(TokenType::lparen),
                    Token(TokenType::number, "12.2"),
                    Token(TokenType::rparen),
                    };
        CHECK_THAT(input, isTokenizedInto(expected));
    }

    input = "[-2, -1]";
    SECTION(input.c_str()) {
        std::vector<Token> expected {Token(TokenType::lbracket),
                    Token(TokenType::minus),
                    Token(TokenType::number, "2"),
                    Token(TokenType::comma),
                    Token(TokenType::minus),
                    Token(TokenType::number, "1"),
                    Token(TokenType::rbracket),
                    };
        CHECK_THAT(input, isTokenizedInto(expected));
    }


}

}
