#include "catch.hpp"
#include <iostream>
#include <sstream>
#include "../../src/box/box.h"
#include "../../src/bisector/bisector.h"

/* Tests the bisection related
operations provided by Bisector and Box classes
*/

using namespace vytlalib::boxes;
using namespace vytlalib::bisector;
using namespace std;

/**
 * @brief Class for matching BisectionResult expected instances to
 * actual instances obtained by calling particular method of given bisector.
 */
class BisectorMatcher : public Catch::MatcherBase<BisectionResult> {
private:
    const Box &box_;
    const IBisector &bisector_;

public:
    BisectorMatcher(const Box &box, const IBisector &bisector):
        box_(box), bisector_(bisector) { }

    virtual bool match(const BisectionResult &expected) const override {
        BisectionResult actual = bisector_.bisect(box_);
        return actual == expected;
    }

    virtual std::string describe() const {
        std::ostringstream ss;
        ss << "is the result of examining " << box_ << " with " << bisector_ << endl;
        return ss.str();
    }
};

inline BisectorMatcher resultOfExamining(const Box &box, const IBisector &bisector) {
    return BisectorMatcher(box, bisector);
}

TEST_CASE("Examining boxes for bisection", "[bisector]") {
    const double threshold = 1e-3;
    const double ratio = 0.5;

    auto degeneratedBounds = std::vector<Component> {Component(4, 4), Component(6, 6), Component(-4, -4)};

    SECTION("Box with three components of sufficient width") {
        auto components = std::vector<Component> {Component(1, 3), Component(-11, 11), Component(19, 26)};
        Box box(components);

        SECTION("ratio = 0.5") {
            BisectionResult br(1, components[1].step(0.5));
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.5)));
        }

        SECTION("ratio = 0.25") {
            BisectionResult br(1, components[1].step(0.25));
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.25)));
        }

        SECTION("ratio = 0.75") {
            BisectionResult br(1, components[1].step(0.75));
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.75)));
        }
    }

    SECTION("Box with second component of sufficient width") {
        auto components = std::vector<Component> {Component(-1, 1), Component(3, 3), Component(-13, 16)};
        Box box(components);

        SECTION("ratio = 0.5") {
            BisectionResult br(2, components[2].step(0.5));
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.5)));
        }

        SECTION("ratio = 0.25") {
            BisectionResult br(2, components[2].step(0.25));
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.25)));
        }

        SECTION("ratio = 0.75") {
            BisectionResult br(2, components[2].step(0.75));
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.75)));
        }
    }

    SECTION("Box with all components of insufficient width") {
        auto components = std::vector<Component> {Component(4, 4), Component(6, 6), Component(-4, -4)};
        Box box(components);

        SECTION("ratio = 0.5") {
            BisectionResult br(-1, 0);
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.5)));
        }

        SECTION("ratio = 0.25") {
            BisectionResult br(-1, 0);
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.25)));
        }

        SECTION("ratio = 0.75") {
            BisectionResult br(-1, 0);
            CHECK_THAT(br, resultOfExamining(box, Bisector(threshold, 0.75)));
        }
    }
}

TEST_CASE("Examining boxes for bisection - exceptions", "[bisector]") {
    SECTION("Incorrect ratio") {
        CHECK_THROWS(Bisector(1, -1.0));
        CHECK_THROWS(Bisector(1, 0.0));
        CHECK_THROWS(Bisector(1, 1.0));
        CHECK_THROWS(Bisector(1, 2.0));
    }
}


