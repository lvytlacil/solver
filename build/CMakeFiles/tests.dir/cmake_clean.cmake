file(REMOVE_RECURSE
  "CMakeFiles/tests.dir/test/repetitiontest.cpp.o"
  "CMakeFiles/tests.dir/test/unit/signcoveringtest.cpp.o"
  "CMakeFiles/tests.dir/test/unit/bisectortest.cpp.o"
  "CMakeFiles/tests.dir/test/unit/signselectionstrategytest.cpp.o"
  "CMakeFiles/tests.dir/test/unit/evaltest.cpp.o"
  "CMakeFiles/tests.dir/test/unit/signvectortest.cpp.o"
  "CMakeFiles/tests.dir/test/unit/scannertest.cpp.o"
  "CMakeFiles/tests.dir/test/unit/boxtest.cpp.o"
  "CMakeFiles/tests.dir/src/box/box.cpp.o"
  "CMakeFiles/tests.dir/src/box/signvector.cpp.o"
  "CMakeFiles/tests.dir/src/bisector/bisector.cpp.o"
  "CMakeFiles/tests.dir/src/component.cpp.o"
  "CMakeFiles/tests.dir/src/degree/paralleldegreecomputation.cpp.o"
  "CMakeFiles/tests.dir/src/degree/idegreecomputation.cpp.o"
  "CMakeFiles/tests.dir/src/parser/scanner.cpp.o"
  "CMakeFiles/tests.dir/src/parser/parser.cpp.o"
  "CMakeFiles/tests.dir/src/parser/ast.cpp.o"
  "CMakeFiles/tests.dir/src/parser/input.cpp.o"
  "CMakeFiles/tests.dir/src/message/messageprocessor.cpp.o"
  "CMakeFiles/tests.dir/src/function/function.cpp.o"
  "CMakeFiles/tests.dir/src/function/functionevaluator.cpp.o"
  "CMakeFiles/tests.dir/src/solver/gridframe.cpp.o"
  "CMakeFiles/tests.dir/src/solver/frame.cpp.o"
  "CMakeFiles/tests.dir/src/solver/algorithm.cpp.o"
  "CMakeFiles/tests.dir/src/utility/utility.cpp.o"
  "tests/tests.pdb"
  "tests/tests"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/tests.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
